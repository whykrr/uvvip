<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">General</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form General</h6>
	</div>
	<div class="card-body">	
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<form method="post" action="<?= base_url('administrator/general/act_save'); ?>" enctype="multipart/form-data">
			<div class="form-group row pl-2 mb-1">
				<label for="banner_general" class="col-sm-2 col-form-label">Banner Utama</label>
				<div class="col-sm-10">
					<input type="hidden" name="old_banner" value="<?= $data['banner_general']; ?>">
					<?= getInput('banner_general', 'file', $validation); ?>
					<img src="<?= base_url('assets/upload/image/general/'.$data['banner_general']); ?>" class="mt-2 w-25 img-thumbnail img-preview">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="title" class="col-sm-2 col-form-label">Judul</label>
				<div class="col-sm-10">
					<?= getTextArea('title', $validation, $data['title']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="title_sub1" class="col-sm-2 col-form-label">Sub Judul 1</label>
				<div class="col-sm-10">
					<?= getTextArea('title_sub1', $validation, $data['title_sub1']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="title_sub2" class="col-sm-2 col-form-label">Sub Judul 2</label>
				<div class="col-sm-10">
					<?= getTextArea('title_sub2', $validation, $data['title_sub2']); ?>
				</div>
			</div>
			<button type="submit" class="btn btn-primary float-right">Simpan</button>
		</form>		
	</div>
</div>
<?= $this->endSection('content'); ?>