<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/offer'); ?>">Partner</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Partner</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><?= $data['name']; ?></h6>
	</div>
	<div class="card-body">
		<div class="row position-relative">
			<div class="col-md-12 p-3">
				<img src="<?= base_url('assets/upload/image/partner/' . $data['photo']); ?>" class="w-50 mx-auto d-block">
			</div>
			<div class="col-md-12 p-3">
				<p><?= $data['content']; ?></p>
			</div>
			<div class="col-md-12 p-3">
				<a href="<?= base_url('administrator/partner'); ?>" class="btn btn-danger btn-lg mt-2 mr-2 float-right">KEMBALI</a>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>