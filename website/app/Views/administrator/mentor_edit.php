<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Mentor</li>
		<li class="breadcrumb-item active" aria-current="page">Ubah Mentor</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Ubah Mentor</h6>
	</div>
	<div class="card-body">
		<form method="post" action="<?= base_url('administrator/mentor/act_edit'); ?>" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?= $data['id']; ?>">
			<div class="col-md-12 mb-3">
				<label for="name">Nama Mentor</label>
				<?= getInput('name', 'text', $validation, $data['name']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="photo">Foto Mentor </label>
				<?= getInput('photo', 'file', $validation); ?>
				<img src="<?= base_url('assets/upload/image/mentor/'.$data['photo']); ?>" class="mt-2 w-25 img-thumbnail img-preview">
			</div>
			<div class="col-md-12 mb-3">
				<label for="audio">Rekaman Mentor </label>
				<?= getInput('audio', 'file', $validation); ?>
				<audio controls="" class="mt-2">
					<source src="<?= base_url('assets/upload/audio/mentor/'.$data['audio']); ?>" type="audio/mp3"></source>
					<source src="<?= base_url('assets/upload/audio/mentor/'.$data['audio']); ?>" type="audio/wav"></source>
				</audio>
			</div>
			<div class="col-md-12 mb-3">
				<label for="message">Pesan</label>
				<?= getTextarea('message', $validation, $data['message']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary btn-lg mt-2 float-right">SIMPAN</button>
				<a href="<?= base_url('administrator/mentor'); ?>" class="btn btn-danger btn-lg mt-2 mr-2 float-right">BATAL</a>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>