<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Testimoni</li>
		<li class="breadcrumb-item active" aria-current="page">Tambah Testimoni</li>
	</ol>
</nav>
<?php
$rating = [
	['id' =>'1', 'name' => '1'],
	['id' =>'2', 'name' => '2'],
	['id' =>'3', 'name' => '3'],
	['id' =>'4', 'name' => '4'],
	['id' =>'5', 'name' => '5'],
]
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Tambah Testimoni</h6>
	</div>
	<div class="card-body">
		<form method="post" action="<?= base_url('administrator/testimoni/act_add'); ?>" enctype="multipart/form-data">
			
			<div class="col-md-12 mb-3">
				<label for="name">Nama</label>
				<?= getInput('name', 'text', $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="ratting">Penilaian</label>
				<?= getSelect('ratting', $rating, $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="testimonial">Testimoni</label>
				<?= getTextarea('testimonial', $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="video">Video </label>
				<?= getInput('video', 'file', $validation); ?>
				<video controls class="mt-2">
					<source src=""></source>
				</video>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary btn-lg mt-2 float-right">SIMPAN</button>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>