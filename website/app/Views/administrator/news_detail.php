<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/news'); ?>">Berita</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Berita</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><?= $data['title']; ?></h6>
	</div>
	<div class="card-body">
		<div class="row position-relative">
			<div class="col-md-12 pt-3">
				<?php
				$date = date('Y-m-d', strtotime($data['created_at']));
				$time = date('H:i:s', strtotime($data['created_at']));
				?>
				<b>Tanggal Terbit : </b><?= tgl_indo($date); ?> <?= $time; ?>
			</div>
			<div class="col-md-12 p-3">
				<img src="<?= base_url('assets/upload/image/news/' . $data['banner']); ?>" class="w-50 mx-auto d-block">
			</div>
			<div class="col-md-12 p-3">
				<p><?= $data['content_wysiwig']; ?></p>
			</div>
			<div class="col-md-12 p-3">
				<a href="<?= base_url('administrator/news'); ?>" class="btn btn-danger btn-lg mt-2 mr-2 float-right">KEMBALI</a>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>