<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">User Administrator</li>
		<li class="breadcrumb-item active" aria-current="page">Tambah User Administrator</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form User Administrator</h6>
	</div>
	<div class="card-body">
		<form method="post" action="<?= base_url('administrator/useradmin/act_add'); ?>">
			
			<div class="form-group row pl-2 mb-1">
				<label for="name" class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-10">
					<?= getInput('name', 'text', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="address" class="col-sm-2 col-form-label">Alamat</label>
				<div class="col-sm-10">
					<?= getTextArea('address', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="phone" class="col-sm-2 col-form-label">Nomor Telepon</label>
				<div class="col-sm-10">
					<?= getInput('phone', 'text', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="email" class="col-sm-2 col-form-label">E-mail</label>
				<div class="col-sm-10">
					<?= getInput('email', 'email', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="password" class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<?= getInput('password', 'password', $validation); ?>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-lg mt-2 float-right">SIMPAN</button>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>