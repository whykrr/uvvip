<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>UVVIP | Website Administrator</title>
	<link rel="icon" href="<?= base_url('assets/img/logo.png'); ?>">

	<!-- Custom fonts for this template-->
	<link href="<?= base_url('assets/template/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('assets/template/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('assets/template/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">

	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
</head>

<body id="page-top">
	<div id="wrapper">
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
		<!-- <div class="sidebar-brand-icon">
			<i class="fas fa-laugh-wink"></i>
		</div> -->
		<div class="sidebar-brand-text mx-3">ADMINISTRATOR</div>
	</a>

	<hr class="sidebar-divider my-0">

	<li class="nav-item <?= ($page == 'Dashboard') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/dashboard'); ?>">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span>
		</a>
	</li>

	<li class="nav-item <?= ($page == 'User Administrator') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/useradmin'); ?>">
			<i class="fas fa-fw fa-users"></i>
			<span>User Administrator</span>
		</a>
	</li>

	<li class="nav-item <?= ($page == 'General') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/general'); ?>">
			<i class="fas fa-fw fa-home"></i>
			<span>General</span>
		</a>
	</li>
	<li class="nav-item <?= ($page == 'Cara Kerja') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/procedure'); ?>">
			<i class="fas fa-fw fa-briefcase"></i>
			<span>Cara Kerja</span>
		</a>
	</li>

	<li class="nav-item <?= ($page == 'Tentang Kami') ? 'active' : ''; ?>">
		<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu-transaction" aria-expanded="true" aria-controls="menu-transaction">
			<i class="fas fa-fw fa-info"></i>
			<span>Tentang Kami</span>
		</a>
		<div id="menu-transaction" class="collapse <?= ($page == 'Tentang Kami') ? 'show' : ''; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<h6 class="collapse-header">Tentang Kami</h6>
				<a class="collapse-item <?= ($page == 'Tentang Kami') ? ($sub_page == 'sambutan') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/about_us/greeting'); ?>">Sambutan</a>
				<a class="collapse-item <?= ($page == 'Tentang Kami') ? ($sub_page == 'visi_misi') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/about_us/vision'); ?>">Visi Misi</a>
				<a class="collapse-item <?= ($page == 'Tentang Kami') ? ($sub_page == 'struktur') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/about_us/structure'); ?>">Struktur</a>
			</div>
		</div>
	</li>

	<li class="nav-item <?= ($page == 'Berita') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/news'); ?>">
			<i class="fas fa-fw fa-newspaper"></i>
			<span>Berita</span>
		</a>
	</li>
	<li class="nav-item <?= ($page == 'Partner') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/partner'); ?>">
			<i class="fas fa-fw fa-users"></i>
			<span>Partner</span>
		</a>
	</li>
	<li class="nav-item <?= ($page == 'Mentor') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/mentor'); ?>">
			<i class="fas fa-fw fa-user"></i>
			<span>Mentor</span>
		</a>
	</li>
	<li class="nav-item <?= ($page == 'Testimoni Siswa') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/testimoni'); ?>">
			<i class="fas fa-fw fa-check-square"></i>
			<span>Testimoni Siswa</span>
		</a>
	</li>
	<li class="nav-item <?= ($page == 'Kotak Masuk') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/inbox'); ?>">
			<i class="fas fa-fw fa-envelope"></i>
			<span>Kotak Masuk</span>
		</a>
	</li>
	<li class="nav-item <?= ($page == 'Pengaturan') ? 'active' : ''; ?>">
		<a class="nav-link" href="<?= base_url('administrator/setting'); ?>">
			<i class="fas fa-fw fa-cogs"></i>
			<span>Pengaturan</span>
		</a>
	</li>
</ul>
<div id="content-wrapper" class="d-flex flex-column">
	<div id="content">
		<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
			<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
				<i class="fa fa-bars"></i>
			</button>
			<ul class="navbar-nav">
				<h1 class="h3 text-gray-800"><?= $page; ?></h1>
			</ul>
			<ul class="navbar-nav ml-auto">
				<?php $user = session()->get('administrator'); ?>
				<li class="nav-item dropdown no-arrow">
					<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user['name']; ?></span>
						<img class="img-profile rounded-circle" src="<?= base_url('assets/img/logo.png'); ?>">
					</a>
					<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
						<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
							<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
							Logout
						</a>
					</div>
				</li>

			</ul>

		</nav>
		<div class="container-fluid">
			<?= $this->renderSection('content'); ?>
		</div>

	</div>
	<footer class="sticky-footer bg-white">
		<div class="container my-auto">
			<div class="copyright text-center my-auto">
				<span>Copyright &copy; United Vision VIP</span>
			</div>
		</div>
	</footer>
	<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a class="btn btn-primary" href="<?= base_url('administrator/logout'); ?>">Logout</a>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Are you sure to delete this data?</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<form method="post">
					<input type="hidden" name="_method" value="DELETE">
					<button type="submit" class="btn btn-danger btn-block">Delete</button>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/template/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/template/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/template/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/template/js/sb-admin-2.min.js'); ?>"></script>

<!-- Page level plugins -->
<script src="<?= base_url('assets/template/vendor/chart.js/Chart.min.js'); ?>"></script>
<script src="<?= base_url('assets/template/vendor/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('assets/template/vendor/datatables/dataTables.bootstrap4.min.js'); ?>"></script>

<!-- Page level custom scripts -->
<script src="<?= base_url('assets/template/js/demo/datatables-demo.js'); ?>"></script>

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script type="text/javascript">
	setTimeout(function(){
		$('.alert').fadeOut();
	}, 3500);
	$(document).ready(function() {
		$('#content_wysiwig').summernote({
			toolbar: [
			['style', ['bold', 'italic', 'underline', 'clear']],
			['font', ['strikethrough', 'superscript', 'subscript']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']]
			]
		});
		$('input[type=file]').change(function (){
			if (this.files && this.files[0]) {
				var reader = new FileReader();
				input = this;

				reader.onload = function(e) {
					console.log(e);
					$(input).parent().find('.img-preview').attr('src', e.target.result);
					$(input).parent().find('video').html('<source src="'+e.target.result+'"></source>');
					$(input).parent().find('audio').html('<source src="'+e.target.result+'"></source>');
				}

				reader.readAsDataURL(this.files[0]);
			}
		})
		$('#deleteModal').on('show.bs.modal', function(e) {
			$(this).find('form').attr('action', $(e.relatedTarget).attr('href'));
		});
	});
</script>
</body>

</html>
