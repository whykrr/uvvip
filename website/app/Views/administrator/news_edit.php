<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Berita</li>
		<li class="breadcrumb-item active" aria-current="page">Ubah Berita</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Ubah Berita</h6>
	</div>
	<div class="card-body">
		<form method="post" action="<?= base_url('administrator/news/act_edit'); ?>" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?= $data['id']; ?>">
			<input type="hidden" name="slug" value="<?= $data['slug']; ?>">
			<div class="col-md-12 mb-3">
				<label for="title">Judul</label>
				<?= getInput('title', 'text', $validation, $data['title']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="banner">Banner</label>
				<?= getInput('banner', 'file', $validation, $data['banner']); ?>
				<img src="<?= base_url('assets/upload/image/news/'.$data['banner']); ?>" class="mt-2 w-25 img-thumbnail img-preview">
			</div>
			<div class="col-md-12 mb-3">
				<label for="content_wysiwig">Konten</label>
				<?= getTextarea('content_wysiwig', $validation, @$data['content_wysiwig']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary btn-lg mt-2 float-right">SIMPAN</button>
				<a href="<?= base_url('administrator/news'); ?>" class="btn btn-danger btn-lg mt-2 mr-2 float-right">BATAL</a>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>