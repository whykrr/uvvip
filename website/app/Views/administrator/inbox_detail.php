<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Kotak Masuk</li>
		<li class="breadcrumb-item active" aria-current="page">Detail Kotak Masuk</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Kotak Masuk</h6>
	</div>
	<div class="card-body">			
		<div class="form-group row pl-2 mb-1">
			<label for="name" class="col-sm-2 col-form-label">Nama</label>
			<div class="col-sm-10">
				<?= getInput('name', 'text', $validation, $data['name'], 'disabled'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="phone" class="col-sm-2 col-form-label">Nomor Telepon</label>
			<div class="col-sm-10">
				<?= getInput('phone', 'text', $validation, $data['phone'], 'disabled'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="email" class="col-sm-2 col-form-label">Email</label>
			<div class="col-sm-10">
				<?= getInput('email', 'text', $validation, $data['email'], 'disabled'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="content" class="col-sm-2 col-form-label">Konten</label>
			<div class="col-sm-10">
				<?= getTextarea('content', $validation, $data['content'], 'disabled'); ?>
			</div>
		</div>
		<a href="<?= base_url('administrator/inbox'); ?>" class="btn btn-danger float-right">Kembali</a>
	</div>
</div>
<?= $this->endSection('content'); ?>