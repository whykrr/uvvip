<?= $this->extend('admin/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page">Dashboard</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Ballroom</h6>
	</div>
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<form method="post" action="<?= base_url('admin/act_ballroom'); ?>">
			<div class="form-group row pl-2 mb-1">
				<label for="smiley" class="col-sm-2 col-form-label">Smiley</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="smiley" name="smiley" value="<?= (old('smiley')) ? old('smiley') : $data['smiley']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="friendly" class="col-sm-2 col-form-label">Friendly</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="friendly" name="friendly" value="<?= (old('friendly')) ? old('friendly') : $data['friendly']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="happy" class="col-sm-2 col-form-label">Happy</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="happy" name="happy" value="<?= (old('happy')) ? old('happy') : $data['happy']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="unique1" class="col-sm-2 col-form-label">Unique 1</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="unique1" name="unique1" value="<?= (old('unique1')) ? old('unique1') : $data['unique1']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="unique2" class="col-sm-2 col-form-label">Unique 2</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="unique2" name="unique2" value="<?= (old('unique2')) ? old('unique2') : $data['unique2']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="unique3" class="col-sm-2 col-form-label">Unique 3</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="unique3" name="unique3" value="<?= (old('unique3')) ? old('unique3') : $data['unique3']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="trendy" class="col-sm-2 col-form-label">Trendy</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="trendy" name="trendy" value="<?= (old('trendy')) ? old('trendy') : $data['trendy']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="funky" class="col-sm-2 col-form-label">Funky</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="funky" name="funky" value="<?= (old('funky')) ? old('funky') : $data['funky']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="bright1" class="col-sm-2 col-form-label">Bright 1</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="bright1" name="bright1" value="<?= (old('bright1')) ? old('bright1') : $data['bright1']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="bright2" class="col-sm-2 col-form-label">Bright 2</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="bright2" name="bright2" value="<?= (old('bright2')) ? old('bright2') : $data['bright2']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="bright3" class="col-sm-2 col-form-label">Bright 3</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="bright3" name="bright3" value="<?= (old('bright3')) ? old('bright3') : $data['bright3']; ?>">
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-lg float-right mt-2">SIMPAN</button>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>