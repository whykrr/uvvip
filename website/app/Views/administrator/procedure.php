<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Cara Kerja</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Cara Kerja</h6>
	</div>
	<div class="card-body">	
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<form method="post" action="<?= base_url('administrator/procedure/act_save'); ?>" enctype="multipart/form-data">
			<div class="form-group row pl-2 mb-1">
				<label for="step1" class="col-sm-2 col-form-label">Step 1</label>
				<div class="col-sm-10">
					<?= getTextArea('step1', $validation, $data['step1']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="step2" class="col-sm-2 col-form-label">Step 2</label>
				<div class="col-sm-10">
					<?= getTextArea('step2', $validation, $data['step2']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="step3" class="col-sm-2 col-form-label">Step 3</label>
				<div class="col-sm-10">
					<?= getTextArea('step3', $validation, $data['step3']); ?>
				</div>
			</div>
			<button type="submit" class="btn btn-primary float-right">Simpan</button>
		</form>		
	</div>
</div>
<?= $this->endSection('content'); ?>