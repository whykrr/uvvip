<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">User Administrator</li>
		<li class="breadcrumb-item active" aria-current="page">Ubah User Administrator</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Ubah User Administrator</h6>
	</div>
	<div class="card-body">
		<form method="post" action="<?= base_url('administrator/useradmin/act_edit'); ?>">
			
			<div class="form-group row pl-2 mb-1">
				<label for="nama" class="col-sm-2 col-form-label">Nama</label>
				<input type="hidden" name="id" value="<?= $data['id']; ?>">
				<input type="hidden" name="old_email" value="<?= $data['email']; ?>">
				<input type="hidden" name="old_password" value="<?= $data['password']; ?>">
				<div class="col-sm-10">
					<?= getInput('name', 'text', $validation, $data['name']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="address" class="col-sm-2 col-form-label">Alamat</label>
				<div class="col-sm-10">
					<?= getTextArea('address', $validation, $data['address']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="phone" class="col-sm-2 col-form-label">Nomor Telepon</label>
				<div class="col-sm-10">
					<?= getInput('phone', 'text', $validation, $data['phone']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="email" class="col-sm-2 col-form-label">E-mail</label>
				<div class="col-sm-10">
					<?= getInput('email', 'email', $validation, $data['email']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="password" class="col-sm-2 col-form-label">Password</label>
				<div class="col-sm-10">
					<?= getInput('password', 'password', $validation, $data['password']); ?>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-lg mt-2 float-right">SIMPAN</button>
			<a href="<?= base_url('administrator/useradmin'); ?>" class="btn btn-danger btn-lg mt-2 mr-2 float-right">BATAL</a>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>