<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Partner</li>
		<li class="breadcrumb-item active" aria-current="page">Tambah Partner</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Tambah Partner</h6>
	</div>
	<div class="card-body">
		<form method="post" action="<?= base_url('administrator/partner/act_add'); ?>" enctype="multipart/form-data">
			
			<div class="col-md-12 mb-3">
				<label for="name">Nama</label>
				<?= getInput('name', 'text', $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="photo">Foto Partner </label>
				<?= getInput('photo', 'file', $validation); ?>
				<img src="" class="mt-2 w-25 img-thumbnail img-preview">
			</div>
			<div class="col-md-12 mb-3">
				<label for="content">Konten</label>
				<?= getTextarea('content', $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary btn-lg mt-2 float-right">SIMPAN</button>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>