<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Tentang Kami</li>
		<li class="breadcrumb-item" aria-current="page">Sambutan</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Sambutan</h6>
	</div>
	<div class="card-body">	
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<form method="post" action="<?= base_url('administrator/about_us/act_save_greeting'); ?>" enctype="multipart/form-data">
			<div class="col-md-12 mb-3">
				<label for="greeting_banner">Gambar</label>
				<input type="hidden" name="old_banner" value="<?= $data['greeting_banner']; ?>">
				<?= getInput('greeting_banner', 'file', $validation); ?>
				<img src="<?= base_url('assets/upload/image/about_us/'.$data['greeting_banner']); ?>" class="mt-2 w-25 img-thumbnail img-preview">
			</div>
			<div class="col-md-12 mb-3">
				<label for="banner">Sambutan</label>
				<?= getTextarea('greeting', $validation, $data['greeting']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary float-right">Simpan</button>
			</div>
		</form>		
	</div>
</div>
<?= $this->endSection('content'); ?>