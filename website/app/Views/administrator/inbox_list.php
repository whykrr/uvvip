<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Kotak Masuk</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Data Kotak Masuk</h6>
	</div>
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Telp</th>
						<th>Email</th>
						<th>Tanggal Kirim</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach ($list as $data) {
						?>
						<tr>
							<td><?= $no++; ?></td>
							<td><?= $data['name']; ?></td>
							<td><?= $data['phone']; ?></td>
							<td><?= $data['email']; ?></td>
							<td><?= tgl_indo(date("Y-m-d", strtotime($data['created_at']))); ?></td>
							<td>
								<a href="<?= base_url('administrator/inbox/detail/'. $data['id']); ?>" class="btn btn-info">Detail</a>
								<form class="d-inline" method="post" action="<?= base_url('administrator/inbox/act_delete'); ?>">
									<input type="hidden" name="id" value="<?= $data['id']; ?>">
									<button type="submit" class="btn btn-danger">Hapus</button>
								</form>
							</td>
						</tr>
						<?php
					} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>