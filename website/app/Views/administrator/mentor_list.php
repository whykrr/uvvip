<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Mentor</li>
	</ol>
</nav>
<div class="row">
	<div class="col-lg-12 mb-2">
		<a href="<?= base_url('administrator/mentor/add'); ?>" class="btn btn-success btn-icon-split float-right ">
			<span class="icon text-white-50">
				<i class="fas fa-plus"></i>
			</span>
			<span class="text">Tambah Mentor</span>
		</a>
	</div>
</div>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Data Mentor</h6>
	</div>
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Foto</th>
						<th>Audio</th>
						<th>Pesan</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach ($list as $data) {
						?>
						<tr>
							<td><?= $no++; ?></td>
							<td><?= $data['name']; ?></td>
							<td class="w-25"><img src="<?= base_url('assets/upload/image/mentor/' . $data['photo']); ?>" class="img-thumbnail"></td>
							<td>
								<audio controls>
									<source src="<?= base_url('assets/upload/audio/mentor/' . $data['audio']); ?>" type="audio/mpeg"></source>
									<source src="<?= base_url('assets/upload/audio/mentor/' . $data['audio']); ?>" type="audio/wav"></source>
								</audio>
							</td>
							<td class="w-25"><?= $data['message'] ?></td>
							<td>
								<a href="<?= base_url('administrator/mentor/edit/'.$data['id']); ?>" class="btn btn-warning">Edit</a>
								<form action="<?= base_url('administrator/mentor/act_delete'); ?>" method="post" class="d-inline">
									<input type="hidden" name="id" value="<?= $data['id']; ?>">
									<input type="hidden" name="photo" value="<?= $data['photo']; ?>">
									<input type="hidden" name="audio" value="<?= $data['audio']; ?>">
									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</td>
						</tr>
						<?php
					} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>