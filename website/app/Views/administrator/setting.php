<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Penaturan</li>
	</ol>
</nav>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Pengaturan</h6>
	</div>
	<div class="card-body">	
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<form method="post" action="<?= base_url('administrator/setting/act_save'); ?>" enctype="multipart/form-data">
			<div class="form-group row pl-2 mb-1">
				<label for="phone" class="col-sm-2 col-form-label">Nomor Telp. UVVIP</label>
				<div class="col-sm-10">
					<?= getInput('phone', 'text', $validation, $data['phone']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="wa_live" class="col-sm-2 col-form-label">Nomor WhatsApp Live Chat (Harus menggunakan 62)</label>
				<div class="col-sm-10">
					<?= getInput('wa_live', 'number', $validation, $data['wa_live']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="email_support" class="col-sm-2 col-form-label">Email UVVIP</label>
				<div class="col-sm-10">
					<?= getInput('email_support', 'email', $validation, $data['email_support'],); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="facebook" class="col-sm-2 col-form-label">Link Facebook</label>
				<div class="col-sm-10">
					<?= getInput('facebook', 'text', $validation, $data['facebook']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="twitter" class="col-sm-2 col-form-label">Link Twitter</label>
				<div class="col-sm-10">
					<?= getInput('twitter', 'text', $validation, $data['twitter']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="instagram" class="col-sm-2 col-form-label">Link Instagram</label>
				<div class="col-sm-10">
					<?= getInput('instagram', 'text', $validation, $data['instagram']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="youtube" class="col-sm-2 col-form-label">Link Youtube</label>
				<div class="col-sm-10">
					<?= getInput('youtube', 'text', $validation, $data['youtube']); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="video_how_works" class="col-sm-2 col-form-label">Video Cara Kerja</label>
				<div class="col-sm-10">
					<input type="hidden" name="old_video_how_works" value="<?= $data['video_how_works']; ?>">
					<?= getInput('video_how_works', 'file', $validation); ?>
					<video controls class="mt-2">
						<source src="<?= base_url('assets/upload/video/setting/'.$data['video_how_works']); ?>"></source>
					</video>
				</div>
			</div>
			<button type="submit" class="btn btn-primary float-right">Simpan</button>
		</form>		
	</div>
</div>
<?= $this->endSection('content'); ?>