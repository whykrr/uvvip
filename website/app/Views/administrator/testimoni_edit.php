<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Testimoni</li>
		<li class="breadcrumb-item active" aria-current="page">Ubah Testimoni</li>
	</ol>
</nav>
<?php
$rating = [
	['id' =>'1', 'name' => '1'],
	['id' =>'2', 'name' => '2'],
	['id' =>'3', 'name' => '3'],
	['id' =>'4', 'name' => '4'],
	['id' =>'5', 'name' => '5'],
]
?>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Ubah Testimoni</h6>
	</div>
	<div class="card-body">
		<form method="post" action="<?= base_url('administrator/testimoni/act_edit'); ?>" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?= $data['id']; ?>">
			<div class="col-md-12 mb-3">
				<label for="name">Nama</label>
				<?= getInput('name', 'text', $validation, $data['name']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="ratting">Penilaian</label>
				<?= getSelect('ratting', $rating, $validation, $data['ratting']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="testimonial">Testimoni</label>
				<?= getTextarea('testimonial', $validation, $data['testimonial']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="video">Video </label>
				<?= getInput('video', 'file', $validation); ?>
				<video controls class="mt-2">
					<source src="<?= base_url('assets/upload/video/testimoni/'.$data['video']); ?>" type="video/mp4"></source>
					<source src="<?= base_url('assets/upload/video/testimoni/'.$data['video']); ?>" type="video/x-flv"></source>
				</video>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary btn-lg mt-2 float-right">SIMPAN</button>
				<a href="<?= base_url('administrator/testimoni'); ?>" class="btn btn-danger btn-lg mt-2 mr-2 float-right">BATAL</a>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>