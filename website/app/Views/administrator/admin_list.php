<?= $this->extend('administrator/layout') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">User Administrator</li>
	</ol>
</nav>
<div class="row">
	<div class="col-lg-12 mb-2">
		<a href="<?= base_url('administrator/useradmin/add'); ?>" class="btn btn-success btn-icon-split float-right ">
			<span class="icon text-white-50">
				<i class="fas fa-plus"></i>
			</span>
			<span class="text">Tambah User</span>
		</a>
	</div>
</div>
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Data User Administrator</h6>
	</div>
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%">
				<thead>
					<tr>
						<td>No</td>
						<td>Nama</td>
						<td>Alamat</td>
						<td>Nomor Telepon</td>
						<td>Email</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<?php 
					$no = 1;
					foreach ($list as $data) {
						?>
						<tr>
							<td><?= $no++; ?></td>
							<td><?= $data['name']; ?></td>
							<td><?= $data['address']; ?></td>
							<td><?= $data['phone']; ?></td>
							<td><?= $data['email']; ?></td>
							<td>
								<a href="<?= base_url('administrator/useradmin/edit/'.$data['id']); ?>" class="btn btn-warning">Edit</a>
								<form action="<?= base_url('administrator/useradmin/act_delete'); ?>" method="post" class="d-inline">
									<input type="hidden" name="id" value="<?= $data['id']; ?>">
									<button type="submit" class="btn btn-danger">Delete</button>
								</form>
							</td>
						</tr>
						<?php
					} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>