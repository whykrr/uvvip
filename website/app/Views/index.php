<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>United Vision VIP</title>
    <link rel="icon" href="<?= base_url('assets/img/logo.png'); ?>">


    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url('assets/dist/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/owl.carousel.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/owl.theme.default.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/all.min.css'); ?>">
    <link rel="stylesheet" href="<?= base_url('assets/styles/home.css'); ?>">
</head>
<body>
    <!-- Navbars -->
    <nav class="navbar navbar-expand-lg navbar-light mt-3">
        <div class="container">
            <a class="navbar-brand" href="index.html">
                <img src="<?= base_url('assets/img/logo.png'); ?>" alt="logo.png" class="img-fluid">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expande"false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav ml-auto">
                    <a class="nav-link" href="#carakerja">Cara Kerja</a>
                    <a class="nav-link" href="#harga">Harga</a>
                    <a class="nav-link" href="#berita">Berita</a>
                    <a class="nav-link" href="#partner">Partner</a>
                    <a class="nav-link" href="#testimoni">Testimoni</a>
                    <a class="nav-link" href="#tentang-kami">Tentang Kami</a>
                </div>
                <div class="navbar-nav">
                    <a href="http://dashboard.unitedvipvision.id/login" class="btn btn-warning mx-2 my-2 my-sm-0 btn-lg">Log In</a>
                    <a href="http://dashboard.unitedvipvision.id/register" class="btn btn-warning mx-2 my-2 my-sm-0 btn-lg">Sign Up</a>
                </div>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Jumbotron -->
    <div class="jumbotron jumbotron-fluid" style="background-image: url(<?= base_url('assets/upload/image/general/'.$setting['banner_general']); ?>)">
        <div class="container">
            <p class="lead"><?= $setting['title']; ?></p>
            <a class="btn btn-warning btn-lg" href="http://dashboard.unitedvipvision.id/login" role="button">Mulai</a>
        </div>
    </div>
    <!-- End Jumbotron -->

    <!-- Panel -->
    <div class="row justify-content-center">
        <div class="col-lg-10 panel">
            <div class="row container">
                <div class="col-lg">
                    <img src="<?= base_url('assets/img/verified.png'); ?>" alt="verified.png" class="float-left">
                    <p><?= $setting['title_sub1']; ?></p>
                </div>
                <div class="col-lg">
                    <img src="<?= base_url('assets/img/rating.png'); ?>" alt="rating.png" class="float-left">
                    <p><?= $setting['title_sub2']; ?></p>
                </div>
            </div>
        </div>
    </div>
    <!-- End Panel -->

    <hr>

    <!-- Cara Kerja -->
    <div class="container pt-2" id="carakerja">
        <h4 class="display-md my-5 text-center">Cara Kerja</h4>
        <div class="row justify-content-center mt-3">
            <div class="col-lg-7 col-sm-12 image">
                <img class="img-fluid" src="assets/img/referral-step-02-45a75b91858a8c0d6c9f21c491ed6537153fab99bb8764e428486b38745eda7c.png" alt="carakerja1.png">
            </div>
            <div class="col-lg-5 col-sm-12 info">
                <h6>First: <span>Hubungi Kami</span></h6>
                <p><?= $procedure['step1']; ?></p>
            </div>
            <hr>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-lg-7 col-sm-12 info">
                <h6>Pilih <span>Kelas Anda</span></h6>
                <p><?= $procedure['step2']; ?></p>
            </div>
            <div class="col-lg-5 col-sm-12 image">
                <img class="img-fluid" src="assets/img/OBJECTS.png" alt="carakerja2.png">
            </div>
            <hr>
        </div>
        <div class="row justify-content-center mt-3">
            <div class="col-lg-7 col-sm-12 image">
                <img class="img-fluid" src="assets/img/OBJECTS-1.png" alt="carakerja3.png">
            </div>
            <div class="col-lg-5 col-sm-12 info">
                <h6>Pilih <span>Mentor</span></h6>
                <p><?= $procedure['step3']; ?></p>
            </div>
        </div>
    </div>
    <!-- End Cara Kerja -->

    <!-- videos -->
    <div id="videos">
      <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-sm-12">
              <div class="embed">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="<?= base_url('assets/upload/video/setting/'.$setting['video_how_works']); ?>" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-lg-12 col-sm-12">
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Coti</button>
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Trading</button>
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Ripple</button>
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Bitcoin</button>
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Cryptocurrency</button>
        </div>
    </div>
    <div class="row my-3">
        <div class="col-lg-12 col-sm-12">
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Trading Competition</button>
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Litecoin</button>
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Ethereum</button>
            <button class="btn btn-outline-warning btn-lg mx-2 my-2">Live Trading</button>
        </div>
    </div>
</div>
</div>
<!-- End Videos -->

<!-- Harga -->
<div id="harga">
    <div class="container pt-2" id="harga">
        <h4 class="display-md my-5 text-center">Harga</h4>
        <div class="row">
            <div class="col-lg-4 col-sm-12 my-3">
                <div class="card price">
                    <div class="image active">
                        <img class="img-fluid card-img-top" src="<?= base_url('assets/img/pack1.png'); ?>" alt="harga1.png">
                    </div>
                    <div class="card-body">
                      <h5 class="card-title">Basic</h5>
                      <p class="card-desc"><?= $pbasic['features']; ?></p>
                      <h5 class="card-text"><?= format_kilo($pbasic['price']); ?></h5>
                      <p class="card-desc-kelas">Per Kelas</p>
                      <a href="http://dashboard.unitedvipvision.id/account/class_shop/class_list?package=<?= $pbasic['id']; ?>" target="_blank" class="btn btn-secondary btn-md"><i class="fas fa-arrow-right"></i></a>
                  </div>
              </div>
          </div>
          <div class="col-lg-4 col-sm-12 my-3">
            <div class="card price active">
                <div class="image">
                    <img class="img-fluid card-img-top" src="<?= base_url('assets/img/pack2.png'); ?>" alt="harga2.png">
                </div>
                <div class="card-body">
                  <h5 class="card-title">Premium</h5>
                  <p class="card-desc"><?= $ppremium['features']; ?></p>
                  <h5 class="card-text"><?= format_kilo($ppremium['price']); ?></h5>
                  <p class="card-desc-kelas">Per Kelas</p>
                  <a href="http://dashboard.unitedvipvision.id/account/class_shop/class_list?package=<?= $ppremium['id']; ?>" target="_blank" class="btn btn-warning btn-md"><i class="fas fa-arrow-right"></i></a>
              </div>
          </div>
      </div>
      <div class="col-lg-4 col-sm-12 my-3">
        <div class="card price">
            <div class="image">
                <img class="img-fluid card-img-top" src="<?= base_url('assets/img/pack3.png'); ?>" alt="harga3.png">
            </div>
            <div class="card-body">
              <h5 class="card-title">VIP</h5>
              <p class="card-desc"><?= $pvip['features']; ?></p>
              <h5 class="card-text"><?= format_kilo($pvip['price']); ?></h5>
              <p class="card-desc-kelas">Per Kelas</p>
              <a href="http://dashboard.unitedvipvision.id/account/class_shop/class_list?package=<?= $pvip['id']; ?>" target="_blank" class="btn btn-secondary btn-md"><i class="fas fa-arrow-right"></i></a>
          </div>
      </div>
  </div>
</div>
</div>
</div>
<!-- End Harga -->

<!-- Berita -->
<div id="berita">
    <div class="container pt-2">
        <h4 class="display-md my-5 text-center">Berita Terbaru</h4>
        <div class="row owl-carousel owl-theme">
            <?php 
            foreach ($news as $data) {
                ?>
                <div class="col-lg-12 col-sm-12 my-3">
                    <div class="card news">
                        <img src="<?= base_url('assets/upload/image/news/'.$data['banner']); ?>" class="img-fluid card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title"><?= $data['title']; ?></h5>
                            <p class="card-text"><?= readMoreHelper($data['content'], 100); ?></p>
                            <button type="button" class="btn btn-warning text-white" onclick="show_news('<?= $data['slug']; ?>')">Baca Selengkapnya</button>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>

<div id="partner">
    <div class="container pt-2">
        <h4 class="display-md my-5 text-center">Partner Kami</h4>
        <div class="row owl-carousel owl-theme">
            <?php 
            foreach ($partner as $data) {
                ?>
                <div class="col-lg-12 col-sm-12 my-3">
                    <div class="card news">
                        <img src="<?= base_url('assets/upload/image/partner/'.$data['photo']); ?>" class="img-fluid card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title"><?= $data['name']; ?></h5>
                            <p class="card-text"><?= $data['content']; ?></p>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<!-- End Berita -->

<!-- Mentor -->
<div id="mentor">
    <div class="container pt-2">
        <h4 class="display-md my-5 text-center">Kualifikasi dan Pengalaman Mentor</h4>
        <div class="row owl-carousel owl-theme owl-mentor">
            <?php 
            foreach ($mentor as $data) {
                ?>
                <div class="col-sm-12 col-lg-12 my-3 text-center align-items-center">
                    <img src="<?= base_url('assets/upload/image/mentor/'.$data['photo']); ?>" class="img-fluid img-thumbnail">
                    <p class="audio-title"><?= $data['message']; ?></p>
                    <audio controls>
                        <source src="<?= base_url('assets/upload/audio/mentor/'.$data['audio']); ?>"></source>
                    </audio>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<!-- End Mentor -->

<!-- Testimoni -->
<div id="testimoni">
    <div class="container pt-2">
        <h4 class="display-md my-5 text-center">Testimoni Siswa</h4>
        <div class="row owl-carousel owl-theme owl-testimoni">
            <?php 
            foreach ($testi as $data) {
                ?>
                <div class="col-sm-12 col-lg-12 my-3 text-center">
                    <video controls width="100%" height="200px">
                        <source src="<?= base_url('assets/upload/video/testimoni/'.$data['video']); ?>"></source>
                    </video>
                    <p class="title"><?= $data['name']; ?></p>
                    <div class="star">
                        <?php 
                        for ($i = 0; $i < $data['ratting']; $i++) {
                            ?>
                            <i class="fas fa-star"></i>
                            <?php
                        }
                        ?>
                    </div>
                    <p class="notes">"<?= $data['testimonial']; ?>"</p>
                </div>
                <?php
            } 
            ?>
        </div>
    </div>
</div>
<!-- End Testimoni -->

<!-- Tentang Kami -->
<div id="tentang-kami">
    <div class="container pt-2">
        <h4 class="display-md my-5 text-center">Tentang Kami</h4>
        <div class="btn-group btn-about" role="group" aria-label="Basic example">
            <button type="button" data-target="#greeting" class="btn btn-light active">Sambutan</button>
            <button type="button" data-target="#vision" class="btn btn-light">Visi dan Misi</button>
            <button type="button" data-target="#structure" class="btn btn-light">Struktur</button>
        </div>
        <div class="row about_us" id="greeting">
            <div class="col-lg-6 img">
                <img src="<?= base_url('assets/upload/image/about_us/'.$about['greeting_banner']); ?>" alt="" class="img-fluid card-img">
            </div>
            <div class="col-lg-6 scroll">
                <div class="card">
                    <div class="card-body">
                        <?= $about['greeting']; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row about_us" id="vision" style="display: none">
            <div class="col-lg-6 img">
                <img src="<?= base_url('assets/upload/image/about_us/'.$about['vision_banner']); ?>" alt="" class="img-fluid card-img">
            </div>
            <div class="col-lg-6 scroll">
                <div class="card">
                    <div class="card-body">
                        <?= $about['vision']; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row about_us" id="structure" style="display: none">
            <div class="col-lg-6 img">
                <img src="<?= base_url('assets/upload/image/about_us/'.$about['structure_banner']); ?>" alt="" class="img-fluid card-img">
            </div>
            <div class="col-lg-6 scroll">
                <div class="card">
                    <div class="card-body">
                        <?= $about['structure']; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Tentang Kami -->

<!-- Hubungi Kami -->
<div id="hubungi-kami">
    <div class="container pt-2">
        <h4 class="display-md my-5 text-center">Hubungi Kami</h4>
        <div class="row">
            <div class="col-lg-6 offset-lg-3 col-sm-12">
                <div class="card">
                    <div class="card-body contact">
                        <form id="form-contact-us">
                            <div class="form-group">
                                <input type="text" class="form-control my-3" name="name" id="name" placeholder="Name" required autocomplete="off">
                                <input type="number" class="form-control my-3" name="phone" id="phone" placeholder="Phone Number" required autocomplete="off">
                                <input type="email" class="form-control my-3" name="email" id="email" placeholder="E-mail" required autocomplete="off">
                                <textarea class="form-control my-3" name="content" placeholder="Konten Pesan" required></textarea>
                                <button type="submit" class="btn btn-warning btn-sm text-white float-right">Submit Now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Hubungi Kami -->

<!-- Footer Section -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <ul class="list-unstyled kolom-1">
                    <li><a href="#carakerja">Cara Kerja</a></li>
                    <li><a href="#harga">Harga</a></li>
                    <li><a href="#berita">Berita</a></li>
                    <li><a href="#partner">Partner</a></li>
                    <li><a href="#testimoni">Testimoni</a></li>
                    <li><a href="#tentang-kami">Tentang Kami</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <ul class="list-unstyled kolom-2">
                    <li><a href="http://dashboard.unitedvipvision.id/register">Daftar Menjadi Siswa</a></li>
                    <li><a href="http://dashboard.unitedvipvision.id/login">Dashboard UVVIP</a></li>
                    <li><a href="#">Komisi Refferral</a></li>
                </ul>
            </div>
            <div class="col-lg-2">
                <ul class="list-unstyled kolom-2">
                    <li><a href="<?= $setting['facebook']; ?>"><i class="fab fa-facebook fa-fw mx-2"></i> Facebook</a></li>
                    <li><a href="<?= $setting['twitter']; ?>"><i class="fab fa-twitter fa-fw mx-2"></i> Twitter</a></li>
                    <li><a href="<?= $setting['instagram']; ?>"><i class="fab fa-instagram fa-fw mx-2"></i> Instagram</a></li>
                    <li><a href="<?= $setting['youtube']; ?>"><i class="fab fa-youtube fa-fw mx-2"></i> Youtube</a></li>
                </ul>
            </div>
            <div class="col-lg-1 offset-lg-2">
                <hr class="vertical">
            </div>
            <div class="col-lg-3 kolom-3">
                <ul class="list-unstyled">
                    <li>
                        <img src="assets/img/Logo.png" alt="logo.png" class="img-fluid">
                    </li>
                    <li>Hubungi Kami</li>
                    <li><?= $setting['phone']; ?></li>
                    <li><?= $setting['email_support']; ?></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<!-- End Footer -->

<!-- Footer Bar -->
<div id="footer-bar">
    <div class="container">
        <div class="row">
            <div class="col text-center mt-3">
                <p>Copyright &copy; 2020 United Vision VIP</p>
            </div>
        </div>
    </div>
</div>
<!-- End Footer Bar -->

<!-- plugins -->
<a href="https://wa.me/<?= $setting['wa_live']; ?>" target="_blank">
    <img src="assets/img/logowa.png" class="img-fluid" id="plug-btn">
</a>
<script src="<?= base_url('assets/dist/js/jquery-3.5.1.min.js'); ?>"></script>
<script src="<?= base_url('assets/dist/js/bootstrap.bundle.min.js'); ?>"></script>
<script src="<?= base_url('assets/dist/js/owl.carousel.min.js'); ?>"></script>
<script src="<?= base_url('assets/dist/js/all.min.js'); ?>"></script>
<script src="<?= base_url('assets/dist/js/home.js'); ?>"></script>

<div class="modal" tabindex="-1" id="modal-inbox">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Kontak Kami</h5>
            </div>
            <div class="modal-body">
                <p>Pesan anda telah masuk!</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="$('#modal-inbox').hide()">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" tabindex="-1" id="modal-news">
    <div class="modal-dialog modal-xl modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="news_title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#modal-news').hide()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-md-12 pt-3">
                    <b>Tanggal Terbit : </b><p class="d-inline" id="news_date">-</p>
                </div>
                <div class="col-md-12 p-3">
                    <img src="" class="w-50 mx-auto d-block" id="news_img">
                </div>
                <div class="col-md-12 p-3" id="news_content">
                </div>
            </div>
        </div>
    </div>
</div>
</body>

<script type="text/javascript">
    $('.btn-about button').click(function(){
        target = $(this).data('target');
        $('.about_us').hide();
        $(target).show();
        $('.btn-about button').removeClass('active');
        $(this).addClass('active');
    });
    $('#form-contact-us').submit(function(){
        $(this).find('button[type=submit]').attr('disabled', true);
        $.ajax({
            type: 'post',
            url: '<?= base_url('home/act_inbox'); ?>',
            data: $(this).serialize(),
            success: function(data)
            {
                $('#form-contact-us').find('button[type=submit]').attr('disabled', false);
                $('#form-contact-us').find('input').val('');
                $('#form-contact-us').find('textarea').val('');
                $('#modal-inbox').show();
            },
            error : function() {
                $('#form-contact-us').find('button[type=submit]').attr('disabled', false);
            }
        });
        return false;
    })
    function show_news(slug){
        $.ajax({
            type: 'post',
            url: '<?= base_url('home/get_news'); ?>',
            data: {
                slug: slug
            },
            dataType: 'json',
            success: function(data)
            {
                $('#modal-news #news_title').html(data.news_title);
                $('#modal-news #news_date').html(data.news_date);
                $('#modal-news #news_img').attr('src', data.news_img);
                $('#modal-news #news_content').html(data.news_content);
                $('#modal-news').show();
            },
        });
        return false;
    }
</script>
</html>