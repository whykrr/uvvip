<?php 
namespace App\Controllers;
use App\Models\TestimonialsMod;
use App\Models\PartnerMod;
use App\Models\PackageMod;
use App\Models\NewsMod;
use App\Models\MentorMod;
use App\Models\ContactMod;

class Home extends BaseController
{
	public function __construct()
	{
		helper('general');
		$this->testiMod = new TestimonialsMod();
		$this->partnerMod = new PartnerMod();
		$this->packageMod = new PackageMod();
		$this->newsMod = new NewsMod();
		$this->mentorMod = new MentorMod();
		$this->contactMod = new ContactMod();
		$this->db = $db = \Config\Database::connect();
	}

	public function index()
	{
		$data['setting'] = $this->db->table('website_settings')->get()->getRowArray();
		$data['procedure'] = $this->db->table('website_procedure')->get()->getRowArray();
		$data['pbasic'] = $this->packageMod->where('id', '1')->first();
		$data['ppremium'] = $this->packageMod->where('id', '2')->first();
		$data['pvip'] = $this->packageMod->where('id', '3')->first();
		$data['news'] = $this->newsMod->orderBy('id', 'desc')->findAll();
		$data['partner'] = $this->partnerMod->orderBy('id', 'desc')->findAll();
		$data['mentor'] = $this->mentorMod->orderBy('id', 'desc')->findAll();
		$data['testi'] = $this->testiMod->orderBy('id', 'desc')->findAll();
		$data['about'] = $this->db->table('website_about_us')->get()->getRowArray();
		return view('index', $data);
	}

	//--------------------------------------------------------------------
	public function get_news(){
		$slug = $this->request->getPost('slug');

		$news = $this->newsMod->where('slug', $slug)->first();
		$date = date('Y-m-d', strtotime($news['created_at']));
		$time = date('H:i:s', strtotime($news['created_at']));

		$json['news_title'] = $news['title'];
		$json['news_date'] = tgl_indo($date).' '.$time;
		$json['news_img'] = base_url('assets/upload/image/news/'.$news['banner']);
		$json['news_content'] = $news['content_wysiwig'];

		echo json_encode($json);
	}

	public function act_inbox(){
		$post = $this->request->getPost();

		$this->contactMod->save($post);
	}
}
