<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\AdministratorModel;

class Useradmin extends BaseController
{
	public function __construct()
	{
		helper('general');
		$this->adminModel = new AdministratorModel();
	}
	public function index()
	{
		$data['list'] = $this->adminModel->where('email !=', 'developer@wk.com')->orderBy('id', 'desc')->findAll();

		$data['page'] = "User Administrator";
		return view('administrator/admin_list', $data);
	}
	public function add()
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "User Administrator";
		return view('administrator/admin_add', $data);
	}
	public function edit($id)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "User Administrator";
		$data['data'] = $this->adminModel->where('id', $id)->first();
		return view('administrator/admin_edit', $data);
	}

	//--------------------------------------------------------------------

	public function act_add(){

		$validation = $this->validate([
			'name'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!'
				]
			],
			'phone'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!'
				]
			],
			'email' => [
				'rules'  => 'required|is_unique[administrators.email]|valid_email',
				'errors' => [
					'required' => 'Form ini harus diisi!',
					'is_unique' => 'Email sudah terdaftar!',
					'valid_email' => 'Email tidak valid!',
				]
			],
			'password'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!'
				]
			],
		]);

		$post = $this->request->getPost();

		if(!$validation) {
			return redirect()->to(base_url('administrator/useradmin/add'))->withInput();
		} else {

			$post['password'] = password_hash($post['password'], PASSWORD_BCRYPT);
			$this->adminModel->insert($post);

			session()->setFlashdata('message', 'Data User Administrator telah ditambahkan!');
			return redirect()->to(base_url('administrator/useradmin'));
		}
	}
	public function act_edit(){

		$users = session()->get('administrator');
		$post = $this->request->getPost();

		if($post['old_email'] == $post['email']){
			$rules_email = 'required|valid_email';
		}else{
			$rules_email = 'required|is_unique[administrators.email]|valid_email';
		}

		$validation = $this->validate([
			'name'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!'
				]
			],
			'phone'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!'
				]
			],
			'email' => [
				'rules'  => $rules_email,
				'errors' => [
					'required' => 'Form ini harus diisi!',
					'is_unique' => 'Email sudah terdaftar!',
					'valid_email' => 'Email tidak valid!',
				]
			],
			'password'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!'
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/useradmin/edit/'.$post['id']))->withInput();
		} else {

			if($post['old_password'] == $post['password']){
				unset($post['password']);
			} else{
				$post['password'] = password_hash($post['password'], PASSWORD_BCRYPT);
			}

			$this->adminModel->save($post);

			if($users['id'] == $post['id']){
				unset($post['password']);
				session()->set('administrator', $post);
			}

			session()->setFlashdata('message', 'Data User Administrator telah di ubah!');
			return redirect()->to(base_url('administrator/useradmin'));
		}
	}
	public function act_delete(){
		$this->adminModel->delete($this->request->getPost('id'));

		session()->setFlashdata('message', 'Data User Administrator telah dihapus!');
		return redirect()->to(base_url('administrator/useradmin'));
	}
}
