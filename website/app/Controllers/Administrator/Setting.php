<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;

class Setting extends BaseController
{
	public function __construct()
	{
		helper('general');
	}
	public function index()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('website_settings');
		$get_setting = $builder->get();

		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Pengaturan";
		$data['data'] =  $get_setting->getRowArray();
		return view('administrator/setting', $data);
	}
	public function act_save()
	{
		$validation = $this->validate([
			'phone' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Nomor Telp harus diisi!',
				]
			],
			'wa_live' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Nomor WhatsApp Live Chat harus diisi!',
				]
			],
			'email_support' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Email harus diisi!',
				]
			],
			'video_how_works' => [
				'rules'  => 'mime_in[video_how_works,video/mp4,video/x-flv]',
				'errors' => [
					'uploaded' => 'Video harus diisi!',
					'mime_in' => 'Video harus mp4 atau flv!',
				]
			],
		]);
		if(!$validation) {
			return redirect()->to(base_url('administrator/setting'))->withInput();
		} else {
			$post = $this->request->getPost();
			$video = $this->request->getFile('video_how_works');

			if($video->getError() != 4){
				$newName = $video->getRandomName();
				$video->move('assets/upload/video/setting', $newName);
				@unlink('assets/upload/video/setting/'.$post['old_video_how_works']);
				$post['video_how_works'] = $newName;
			}
			unset($post['old_video_how_works']);

			$db = \Config\Database::connect();

			$builder = $db->table('website_settings');
			$builder->where('id', '1');
			$builder->update($post);

			session()->setFlashdata('message', 'Pengaturan berhasil disimpan!');
			return redirect()->to(base_url('administrator/setting'));
		}
	}

}
