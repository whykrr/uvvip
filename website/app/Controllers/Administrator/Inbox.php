<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\ContactMod;

class Inbox extends BaseController
{
	public function __construct()
	{
		helper('general');
		$this->contactMod = new ContactMod();
	}
	public function index()
	{
		$data['page'] = "Kotak Masuk";
		$data['list'] =  $this->contactMod->orderBy('id', 'desc')->findAll();
		return view('administrator/inbox_list', $data);
	}
	public function detail($id)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Kotak Masuk";
		$data['data'] =  $this->contactMod->where('id', $id)->first();
		return view('administrator/inbox_detail', $data);
	}

	public function act_delete(){
		$this->contactMod->delete($this->request->getPost('id'));

		session()->setFlashdata('message', 'Kotak Masuk telah dihapus!');
		return redirect()->to(base_url('administrator/inbox'));
	}
}
