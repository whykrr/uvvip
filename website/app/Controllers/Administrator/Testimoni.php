<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\TestimonialsMod;
use Cocur\Slugify\Slugify;

class Testimoni extends BaseController
{
	public function __construct()
	{
		helper('general');
		$this->testiMod = new TestimonialsMod();
	}
	public function index()
	{
		$data['list'] = $this->testiMod->orderBy('id', 'desc')->findAll();

		$data['page'] = "Testimoni Siswa";
		return view('administrator/testimoni_list', $data);
	}
	public function add()
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Testimoni Siswa";
		return view('administrator/testimoni_add', $data);
	}
	public function edit($id)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Testimoni Siswa";
		$data['data'] = $this->testiMod->where('id', $id)->first();
		return view('administrator/testimoni_edit', $data);
	}
	public function detail($id)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Testimoni Siswa";
		$data['data'] = $this->testiMod->where('id', $id)->first();
		return view('administrator/testimoni_detail', $data);
	}

	//--------------------------------------------------------------------

	public function act_add(){

		$validation = $this->validate([
			'name' => [
				'rules'  => 'required|is_unique[website_testimonials.name]',
				'errors' => [
					'required' => 'Nama harus diisi!',
					'is_unique' => 'Nama sudah digunakan!',
				]
			],
			'ratting' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Penilaian harus diisi!'
				]
			],
			'testimonial' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Testimoni harus diisi!'
				]
			],
			'video' => [
				'rules'  => 'uploaded[video]|mime_in[video,video/mp4,video/x-flv]',
				'errors' => [
					'uploaded' => 'Video harus diisi!',
					'mime_in' => 'Video harus mp4 atau flv!',
				]
			],
		]);

		$post = $this->request->getPost();
		$video = $this->request->getFile('video');

		if(!$validation) {
			return redirect()->to(base_url('administrator/testimoni/add'))->withInput();
		} else {

			$newName = $video->getRandomName();
			$video->move('assets/upload/video/testimoni', $newName);

			$post['video'] = $newName;
			$this->testiMod->insert($post);

			session()->setFlashdata('message', 'Data Testimoni telah ditambahkan!');
			return redirect()->to(base_url('administrator/testimoni'));
		}
	}
	public function act_edit(){

		$users = session()->get('administrator');
		$post = $this->request->getPost();

		$validation = $this->validate([
			'name' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Nama harus diisi!',
					'is_unique' => 'Nama sudah digunakan!',
				]
			],
			'ratting' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Penilaian harus diisi!'
				]
			],
			'testimonial' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Testimoni harus diisi!'
				]
			],
			'video' => [
				'rules'  => 'mime_in[video,video/mp4,video/x-flv]',
				'errors' => [
					'uploaded' => 'Video harus diisi!',
					'mime_in' => 'Video harus mp4 atau flv!',
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/testimoni/edit/'.$post['id']))->withInput();
		} else {

			$video = $this->request->getFile('video');
			if($video->getError() != 4){
				$newName = $video->getRandomName();
				$video->move('assets/upload/video/testimoni', $newName);
				$post['video'] = $newName;
			}

			$this->testiMod->save($post);

			session()->setFlashdata('message', 'Data Testimoni telah di ubah!');
			return redirect()->to(base_url('administrator/testimoni'));
		}
	}
	public function act_delete(){
		$this->testiMod->delete($this->request->getPost('id'));
		unlink('assets/upload/video/testimoni/'.$this->request->getPost('video'));

		session()->setFlashdata('message', 'Data Testimoni telah dihapus!');
		return redirect()->to(base_url('administrator/testimoni'));
	}
}
