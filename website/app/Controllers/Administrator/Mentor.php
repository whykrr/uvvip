<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\MentorMod;
use Cocur\Slugify\Slugify;

class Mentor extends BaseController
{
	public function __construct()
	{
		helper('general');
		$this->mentorMod = new MentorMod();
	}
	public function index()
	{
		$data['list'] = $this->mentorMod->orderBy('id', 'desc')->findAll();

		$data['page'] = "Mentor";
		return view('administrator/mentor_list', $data);
	}
	public function add()
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Mentor";
		return view('administrator/mentor_add', $data);
	}
	public function edit($id)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Mentor";
		$data['data'] = $this->mentorMod->where('id', $id)->first();
		return view('administrator/mentor_edit', $data);
	}
	public function detail($id)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Mentor";
		$data['data'] = $this->mentorMod->where('id', $id)->first();
		return view('administrator/mentor_detail', $data);
	}

	//--------------------------------------------------------------------

	public function act_add(){

		$validation = $this->validate([
			'name' => [
				'rules'  => 'required|is_unique[website_mentor.name]',
				'errors' => [
					'required' => 'Nama Mentor harus diisi!',
					'is_unique' => 'Nama Mentor sudah digunakan!',
				]
			],
			'photo' => [
				'rules'  => 'uploaded[photo]|mime_in[photo,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Foto Mentor harus diisi!',
					'mime_in' => 'Foto Mentor harus berupa gambar!',
				]
			],
			'audio' => [
				'rules'  => 'uploaded[audio]|mime_in[audio,audio/mpeg,audio/wav]',
				'errors' => [
					'uploaded' => 'Rekaman Mentor harus diisi!',
					'mime_in' => 'Rekaman Mentor harus mp3 atau wav!',
				]
			],
			'message' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Pesan Mentor harus diisi!'
				]
			],
		]);

		$post = $this->request->getPost();
		$photo = $this->request->getFile('photo');
		$audio = $this->request->getFile('audio');

		if(!$validation) {
			return redirect()->to(base_url('administrator/mentor/add'))->withInput();
		} else {

			$newName = $photo->getRandomName();
			$photo->move('assets/upload/image/mentor', $newName);

			$newNameAudio = $audio->getRandomName();
			$audio->move('assets/upload/audio/mentor', $newNameAudio);

			$post['photo'] = $newName;
			$post['audio'] = $newNameAudio;
			$this->mentorMod->insert($post);

			session()->setFlashdata('message', 'Data Mentor telah ditambahkan!');
			return redirect()->to(base_url('administrator/mentor'));
		}
	}
	public function act_edit(){

		$users = session()->get('administrator');
		$post = $this->request->getPost();

		$validation = $this->validate([
			'name' => [
				'rules'  => 'required|is_unique[website_mentor.name]',
				'errors' => [
					'required' => 'Nama Mentor harus diisi!',
					'is_unique' => 'Nama Mentor sudah digunakan!',
				]
			],
			'photo' => [
				'rules'  => 'mime_in[photo,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Foto Mentor harus diisi!',
					'mime_in' => 'Foto Mentor harus berupa gambar!',
				]
			],
			'audio' => [
				'rules'  => 'mime_in[audio,audio/mpeg,audio/wav]',
				'errors' => [
					'uploaded' => 'Rekaman Mentor harus diisi!',
					'mime_in' => 'Rekaman Mentor harus mp3 atau wav!',
				]
			],
			'message' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Pesan Mentor harus diisi!'
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/mentor/edit/'.$post['id']))->withInput();
		} else {

			$photo = $this->request->getFile('photo');
			if($photo->getError() != 4){
				$newName = $photo->getRandomName();
				$photo->move('assets/upload/image/mentor', $newName);
				$post['photo'] = $newName;
			}
			$audio = $this->request->getFile('audio');
			if($audio->getError() != 4){
				$newNameAudio = $audio->getRandomName();
				$audio->move('assets/upload/audio/mentor', $newNameAudio);
				$post['audio'] = $newNameAudio;
			}

			$this->mentorMod->save($post);

			session()->setFlashdata('message', 'Data Mentor telah di ubah!');
			return redirect()->to(base_url('administrator/mentor'));
		}
	}
	public function act_delete(){
		$this->mentorMod->delete($this->request->getPost('id'));
		@unlink('assets/upload/image/mentor/'.$this->request->getPost('photo'));
		@unlink('assets/upload/audio/mentor/'.$this->request->getPost('audio'));

		session()->setFlashdata('message', 'Data Mentor telah dihapus!');
		return redirect()->to(base_url('administrator/mentor'));
	}
}
