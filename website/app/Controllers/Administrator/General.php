<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;

class General extends BaseController
{
	public function __construct()
	{
		helper('general');
	}
	public function index()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('website_settings');
		$get_setting = $builder->get();

		$data['validation'] = \Config\Services::validation();
		$data['page'] = "General";
		$data['data'] =  $get_setting->getRowArray();
		return view('administrator/general', $data);
	}
	public function act_save()
	{
		$validation = $this->validate([
			'title' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Judul harus diisi!',
				]
			],
			'title_sub1' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Sub Judul 1 harus diisi!',
				]
			],
			'title_sub2' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Sub Judul 2 harus diisi!',
				]
			],
			'title_sub2' => [
				'rules'  => 'mime_in[banner_general,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
		]);
		if(!$validation) {
			return redirect()->to(base_url('administrator/general'))->withInput();
		} else {
			$post = $this->request->getPost();
			$banner = $this->request->getFile('banner_general');

			if($banner->getError() != 4){
				$newName = $banner->getRandomName();
				$banner->move('assets/upload/image/general', $newName);
				@unlink('assets/upload/image/general/'.$post['old_banner']);
				$post['banner_general'] = $newName;
			}
			unset($post['old_banner']);

			$db = \Config\Database::connect();

			$builder = $db->table('website_settings');
			$builder->where('id', '1');
			$builder->update($post);

			session()->setFlashdata('message', 'General Form berhasil disimpan!');
			return redirect()->to(base_url('administrator/general'));
		}
	}

}
