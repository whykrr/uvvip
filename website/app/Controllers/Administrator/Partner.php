<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\PartnerMod;
use Cocur\Slugify\Slugify;

class Partner extends BaseController
{
	public function __construct()
	{
		helper('general');
		$this->partnerMod = new PartnerMod();
		$this->slugify = new Slugify();
	}
	public function index()
	{
		$data['list'] = $this->partnerMod->orderBy('id', 'desc')->findAll();

		$data['page'] = "Partner";
		return view('administrator/partner_list', $data);
	}
	public function add()
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Partner";
		return view('administrator/partner_add', $data);
	}
	public function edit($slug)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Partner";
		$data['data'] = $this->partnerMod->where('slug', $slug)->first();
		return view('administrator/partner_edit', $data);
	}
	public function detail($slug)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Partner";
		$data['data'] = $this->partnerMod->where('slug', $slug)->first();
		return view('administrator/partner_detail', $data);
	}

	//--------------------------------------------------------------------

	public function act_add(){

		$validation = $this->validate([
			'name' => [
				'rules'  => 'required|is_unique[website_partners.name]',
				'errors' => [
					'required' => 'Nama Partner harus diisi!',
					'is_unique' => 'Nama Partner sudah digunakan!',
				]
			],
			'photo' => [
				'rules'  => 'uploaded[photo]|mime_in[photo,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Foto Partner harus diisi!',
					'mime_in' => 'Foto Partner harus berupa gambar!',
				]
			],
			'content' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);

		$post = $this->request->getPost();
		$photo = $this->request->getFile('photo');

		if(!$validation) {
			return redirect()->to(base_url('administrator/partner/add'))->withInput();
		} else {

			$newName = $photo->getRandomName();
			$photo->move('assets/upload/image/partner', $newName);

			$post['photo'] = $newName;
			$post['slug'] = $this->slugify->slugify($post['name']);
			$this->partnerMod->insert($post);

			session()->setFlashdata('message', 'Data Partner telah ditambahkan!');
			return redirect()->to(base_url('administrator/partner'));
		}
	}
	public function act_edit(){

		$users = session()->get('administrator');
		$post = $this->request->getPost();

		$validation = $this->validate([
			'name' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Nama Partner harus diisi!',
					'is_unique' => 'Nama Partner sudah digunakan!',
				]
			],
			'photo' => [
				'rules'  => 'mime_in[photo,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Foto Partner harus diisi!',
					'mime_in' => 'Foto Partner harus berupa gambar!',
				]
			],
			'content' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/partner/edit/'.$post['slug']))->withInput();
		} else {

			$photo = $this->request->getFile('photo');
			if($photo->getError() != 4){
				$newName = $photo->getRandomName();
				$photo->move('assets/upload/image/partner', $newName);
				$post['photo'] = $newName;
			}

			$post['slug'] = $this->slugify->slugify($post['name']);
			$this->partnerMod->save($post);

			session()->setFlashdata('message', 'Data Partner telah di ubah!');
			return redirect()->to(base_url('administrator/partner'));
		}
	}
	public function act_delete(){
		$this->partnerMod->delete($this->request->getPost('id'));
		unlink('assets/upload/image/partner/'.$this->request->getPost('photo'));

		session()->setFlashdata('message', 'Data Partner telah dihapus!');
		return redirect()->to(base_url('administrator/partner'));
	}
}
