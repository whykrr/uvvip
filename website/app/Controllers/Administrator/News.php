<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\NewsMod;
use Cocur\Slugify\Slugify;

class News extends BaseController
{
	public function __construct()
	{
		helper('general');
		$this->newsMod = new NewsMod();
		$this->slugify = new Slugify();
	}
	public function index()
	{
		$data['list'] = $this->newsMod->orderBy('id', 'desc')->findAll();

		$data['page'] = "Berita";
		return view('administrator/news_list', $data);
	}
	public function add()
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Berita";
		return view('administrator/news_add', $data);
	}
	public function edit($slug)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Berita";
		$data['data'] = $this->newsMod->where('slug', $slug)->first();
		return view('administrator/news_edit', $data);
	}
	public function detail($slug)
	{
		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Berita";
		$data['data'] = $this->newsMod->where('slug', $slug)->first();
		return view('administrator/news_detail', $data);
	}

	//--------------------------------------------------------------------

	public function act_add(){

		$validation = $this->validate([
			'title' => [
				'rules'  => 'required|is_unique[website_news.title]',
				'errors' => [
					'required' => 'Judul harus diisi!',
					'is_unique' => 'Judul sudah digunakan!',
				]
			],
			'banner' => [
				'rules'  => 'uploaded[banner]|mime_in[banner,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Gambar Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'content_wysiwig' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);

		$post = $this->request->getPost();
		$banner = $this->request->getFile('banner');

		if(!$validation) {
			return redirect()->to(base_url('administrator/news/add'))->withInput();
		} else {

			$newName = $banner->getRandomName();
			$banner->move('assets/upload/image/news', $newName);

			$post['banner'] = $newName;
			$post['slug'] = $this->slugify->slugify($post['title']);
			$post['content'] = strip_tags($post['content_wysiwig']);
			$this->newsMod->insert($post);

			session()->setFlashdata('message', 'Data Berita telah ditambahkan!');
			return redirect()->to(base_url('administrator/news'));
		}
	}
	public function act_edit(){

		$users = session()->get('administrator');
		$post = $this->request->getPost();

		$validation = $this->validate([
			'title' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Judul harus diisi!',
					'is_unique' => 'Judul sudah digunakan!',
				]
			],
			'banner' => [
				'rules'  => 'mime_in[banner,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Gambar Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'content_wysiwig' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/news/edit/'.$post['slug']))->withInput();
		} else {

			$banner = $this->request->getFile('banner');
			if($banner->getError() != 4){
				$newName = $banner->getRandomName();
				$banner->move('assets/upload/image/news', $newName);
				$post['banner'] = $newName;
			}

			$post['slug'] = $this->slugify->slugify($post['title']);
			$post['content'] = strip_tags($post['content_wysiwig']);
			$this->newsMod->save($post);

			session()->setFlashdata('message', 'Data Berita telah di ubah!');
			return redirect()->to(base_url('administrator/news'));
		}
	}
	public function act_delete(){
		$this->newsMod->delete($this->request->getPost('id'));

		session()->setFlashdata('message', 'Data Berita telah dihapus!');
		return redirect()->to(base_url('administrator/news'));
	}
}
