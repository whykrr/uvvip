<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;

class Procedure extends BaseController
{
	public function __construct()
	{
		helper('general');
	}
	public function index()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('website_procedure');
		$get_setting = $builder->get();

		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Cara Kerja";
		$data['data'] =  $get_setting->getRowArray();
		return view('administrator/procedure', $data);
	}
	public function act_save()
	{
		$validation = $this->validate([
			'step1' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Step 1 harus diisi!',
				]
			],
			'step2' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Step 2 harus diisi!',
				]
			],
			'step3' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Step 3 harus diisi!',
				]
			],
		]);
		if(!$validation) {
			return redirect()->to(base_url('administrator/procedure'))->withInput();
		} else {
			$post = $this->request->getPost();

			$db = \Config\Database::connect();

			$builder = $db->table('website_procedure');
			$builder->where('id', '1');
			$builder->update($post);

			session()->setFlashdata('message', 'Cara Kerja berhasil disimpan!');
			return redirect()->to(base_url('administrator/procedure'));
		}
	}

}
