<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;

class About_us extends BaseController
{
	public function __construct()
	{
		helper('general');
	}
	public function greeting()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('website_about_us');
		$get_about = $builder->get();

		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Tentang Kami";
		$data['sub_page'] = "sambutan";
		$data['data'] =  $get_about->getRowArray();
		return view('administrator/about_greetings', $data);
	}
	public function vision()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('website_about_us');
		$get_about = $builder->get();

		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Tentang Kami";
		$data['sub_page'] = "visi_misi";
		$data['data'] =  $get_about->getRowArray();
		return view('administrator/about_vision', $data);
	}
	public function structure()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('website_about_us');
		$get_about = $builder->get();

		$data['validation'] = \Config\Services::validation();
		$data['page'] = "Tentang Kami";
		$data['sub_page'] = "struktur";
		$data['data'] =  $get_about->getRowArray();
		return view('administrator/about_structure', $data);
	}
	public function act_save_greeting()
	{
		
		$post = $this->request->getPost();
		$banner = $this->request->getFile('greeting_banner');

		if($banner->getError() != 4){
			$newName = $banner->getRandomName();
			$banner->move('assets/upload/image/about_us', $newName);
			@unlink('assets/upload/image/about_us/'.$post['old_banner']);
			$post['greeting_banner'] = $newName;
		}
		unset($post['old_banner']);

		$db = \Config\Database::connect();

		$builder = $db->table('website_about_us');
		$builder->where('id', '1');
		$builder->update($post);

		session()->setFlashdata('message', 'Sambutan berhasil disimpan!');
		return redirect()->to(base_url('administrator/about_us/greeting'));
	}
	public function act_save_vision()
	{
		
		$post = $this->request->getPost();
		$banner = $this->request->getFile('vision_banner');

		if($banner->getError() != 4){
			$newName = $banner->getRandomName();
			$banner->move('assets/upload/image/about_us', $newName);
			@unlink('assets/upload/image/about_us/'.$post['old_banner']);
			$post['vision_banner'] = $newName;
		}
		unset($post['old_banner']);

		$db = \Config\Database::connect();

		$builder = $db->table('website_about_us');
		$builder->where('id', '1');
		$builder->update($post);

		session()->setFlashdata('message', 'Visi Misi berhasil disimpan!');
		return redirect()->to(base_url('administrator/about_us/vision'));
	}
	public function act_save_structure()
	{
		
		$post = $this->request->getPost();
		$banner = $this->request->getFile('structure_banner');

		if($banner->getError() != 4){
			$newName = $banner->getRandomName();
			$banner->move('assets/upload/image/about_us', $newName);
			@unlink('assets/upload/image/about_us/'.$post['old_banner']);
			$post['structure_banner'] = $newName;
		}
		unset($post['old_banner']);

		$db = \Config\Database::connect();

		$builder = $db->table('website_about_us');
		$builder->where('id', '1');
		$builder->update($post);

		session()->setFlashdata('message', 'Struktur berhasil disimpan!');
		return redirect()->to(base_url('administrator/about_us/structure'));
	}
}
