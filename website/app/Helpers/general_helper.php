<?php
function getInput($name, $type = 'text', $validation,  $val = '', $status = ''){
	?>
	<input type="<?= $type; ?>" <?= $status; ?> class="form-control <?= ($validation->hasError($name)) ? 'is-invalid' : '' ; ?>" id="<?= $name; ?>" name="<?= $name; ?>" value="<?= (old($name)) ? old($name) : $val ; ?>">
	<div class="invalid-feedback"><?= $validation->getError($name); ?></div>
	<?php
}
function getInputFIle($name, $validation,  $val = '', $status = ''){
	?>
	<div class="custom-file">
		<input type="file" class="custom-file-input <?= ($validation->hasError($name)) ? 'is-invalid' : '' ; ?>" id="<?= $name; ?>" name="<?= $name; ?>" value="<?= (old($name)) ? old($name) : $val ; ?>">
		<div class="invalid-feedback"><?= $validation->getError($name); ?></div>
		<label class="custom-file-label" for="<?= $name; ?>">Choose file</label>
	</div>
	<?php
}
function getTextArea($name, $validation,  $val = '', $status = ''){
	?>
	<textarea <?= $status; ?> class="form-control <?= ($validation->hasError($name)) ? 'is-invalid' : '' ; ?>" id='<?= $name; ?>' name="<?= $name; ?>"><?= (old($name)) ? old($name) : $val; ?></textarea>
	<div class="invalid-feedback"><?= $validation->getError($name); ?></div>	
	<?php
}
function getSelect($name, $option = [], $validation, $val = '', $status = ''){
	?>
	<select name="<?= $name; ?>" <?= $status; ?> class="form-control <?= ($validation->hasError($name)) ? 'is-invalid' : '' ; ?>">
		<option value="">-- PILIH --</option>
		<?php
		foreach ($option as $data) {
			if((old($name)) ? old($name) : $val == $data['id']){
				echo "<option value='$data[id]' selected >$data[name]</option>";
			}else{
				echo "<option value='$data[id]'>$data[name]</option>";
			}
		}
		?>
	</select>
	<div class="invalid-feedback"><?= $validation->getError($name); ?></div>
	<?php
	if($status == 'disabled'){
		?>
		<input type="hidden" name="<?= $name; ?>" value="<?= $val; ?>">
		<?php
	}
	?>
	<?php
}

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
	
	// variabel pecahkan 0 = tanggal
	// variabel pecahkan 1 = bulan
	// variabel pecahkan 2 = tahun

	return @$pecahkan[2] . ' ' . @$bulan[ (int)$pecahkan[1] ] . ' ' . @$pecahkan[0];
}
function readMoreHelper($story_desc, $chars = 300) {
	$story_desc = substr($story_desc,0,$chars);  
	$story_desc = substr($story_desc,0,strrpos($story_desc,' '));  
	$story_desc = $story_desc . '...';
	return $story_desc;  
} 
function format_rupiah($angka){
	
	$hasil_rupiah = "Rp. " . number_format($angka, 0,',','.');
	return $hasil_rupiah;

}
function format_kilo($angka){
	
	$hasil_rupiah = number_format($angka / 1000, 0,',','.');
	return $hasil_rupiah . 'K';

}
function getAcronym($name){
	$words = explode(" ", $name);
	$acronym = "";
	$initial = 1;

	if($name){
		foreach ($words as $w) {
			if($initial <= 2){
				$acronym .= substr($w, 0, 1);
				$initial++;
			}
		}
	}

	return strtoupper($acronym);
}
?>