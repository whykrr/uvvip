<?php namespace App\Models;

use CodeIgniter\Model;

class ContactMod extends Model
{
	protected $table      = 'website_contact_us';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'name',
    	'phone',
        'email',
    	'content',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}