<?php namespace App\Models;

use CodeIgniter\Model;

class MentorMod extends Model
{
	protected $table      = 'website_mentor';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'name',
    	'photo',
        'audio',
    	'message',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}