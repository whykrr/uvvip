<?php namespace App\Models;

use CodeIgniter\Model;

class PackageMod extends Model
{
	protected $table      = 'packages';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'name',
    	'desc',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}