<?php namespace App\Models;

use CodeIgniter\Model;

class NewsMod extends Model
{
	protected $table      = 'website_news';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'title',
    	'slug',
    	'banner',
    	'content',
    	'content_wysiwig',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}