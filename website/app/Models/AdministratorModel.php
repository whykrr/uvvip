<?php namespace App\Models;

use CodeIgniter\Model;

class AdministratorModel extends Model
{
	protected $table      = 'administrators';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'name',
    	'address',
    	'phone',
    	'email',
    	'password',
    	'photo',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    public function login($data){
        $users = $this->where('email', $data['email'])->first();
        $password = $data['password'];
        $feedback = [];

        if($users){
            $hash = $users['password'];
            if (password_verify($password, $hash)) {
                unset($users['password']);
                $feedback['session'] = $users;
                $feedback['res'] = 'success';
                $feedback['msg'] = '';
            } else {
                $feedback['res'] = 'fail';
                $feedback['msg'] = 'Username and password is incorrect!';
            }
        }else{
            $feedback['res'] = 'fail';
            $feedback['msg'] = 'Email not found!';
        }
        return $feedback;
    }
}

?>