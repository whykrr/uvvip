<?php namespace App\Models;

use CodeIgniter\Model;

class PartnerMod extends Model
{
	protected $table      = 'website_partners';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'name',
    	'slug',
    	'photo',
    	'content',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}