<?php namespace App\Models;

use CodeIgniter\Model;

class TestimonialsMod extends Model
{
	protected $table      = 'website_testimonials';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'name',
    	'ratting',
        'testimonial',
    	'video',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}