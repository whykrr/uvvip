$(document).ready(function(){
    $('#harga .col-lg-4').hover(
        function(){
            $(this).animate({
                marginTop: "-=1%",
            }, 200);
        },
    
        function () {
            $(this).animate({
                marginTop: "0%",
            }, 200);
        }
    );
});

$(document).ready(function () {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 3,
        loop: true,
        autoplay: true,
        autoplayTimeout: 2500,
        autoplayHoverPause: true,
        lazyLoad:true,
        autoHeight:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
        }
    });
});

$(document).ready(function () {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 3,
        loop: true,
        autoplay: false,
        lazyLoad:true,
        autoplayTimeout: 2500,
        autoplayHoverPause: true,
        autoWidth:true,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:3
            },
        }
    });
});
