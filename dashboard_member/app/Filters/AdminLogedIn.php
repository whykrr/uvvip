<?php 
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AdminLogedIn implements FilterInterface
{
	public function before(RequestInterface $request, $arguments = null) {
		$session = session();
		$user = $session->get('administrator');
		if($user) {
			return redirect()->to(base_url('administrator/dashboard')); 
		}
	}
    //--------------------------------------------------------------------

	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null) {
        // Do something here
	}

}