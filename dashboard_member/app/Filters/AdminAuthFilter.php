<?php 
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AdminAuthFilter implements FilterInterface
{
	public function before(RequestInterface $request, $arguments = null) {
		$session = session();
		$user = $session->get('administrator');
		if(!$user) {
			session()->set('last_page_adm', uri_string());
			return redirect()->to(base_url('administrator/login')); 
		}
	}
    //--------------------------------------------------------------------

	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null) {
        // Do something here
	}

}