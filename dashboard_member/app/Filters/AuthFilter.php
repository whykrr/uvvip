<?php 
namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;

class AuthFilter implements FilterInterface
{
	public function before(RequestInterface $request, $arguments = null) {
		$session = session();
		$users = $session->get('users');
		if(!$users) {
			session()->set('last_page', uri_string());
			return redirect()->to(base_url('login')); 
		}
	}
    //--------------------------------------------------------------------

	public function after(RequestInterface $request, ResponseInterface $response, $arguments = null) {
        // Do something here
	}

}