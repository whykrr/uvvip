<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Dashboard');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Dashboard::index', ['filter' => 'auth']);
$routes->get('/account', function(){
	return redirect()->to(base_url('account/profile'));
});
$routes->get('/classroom', function(){
	return redirect()->to(base_url('classroom/dashboard'));
});
$routes->get('/login', 'Authenticate::login', ['filter' => 'logedin']);
$routes->get('/register', 'Authenticate::register', ['filter' => 'logedin']);
$routes->get('/forget_password', 'Authenticate::forget_password', ['filter' => 'logedin']);

$routes->group('/', ['filter' => 'auth'], function($routes)
{
	$routes->add('dashboard', 'Dashboard::index');
	$routes->add('logout', 'Authenticate::logout');

	$routes->group('account', function($routes)
	{
		$routes->add('profile', 'Account\Profile::index');
		$routes->add('profile/(:segment)', 'Account\Profile::$1');
		$routes->add('class_shop', 'Account\Class_shop::index');
		$routes->add('class_shop/(:segment)', 'Account\Class_shop::$1');
		$routes->add('transaction', 'Account\Transaction::index');
		$routes->add('transaction/(:segment)', 'Account\Transaction::$1');
		$routes->add('offers', 'Account\Offers::index');


		$routes->add('transaction_detail/(:alphanum)', 'Account\Transaction::detail/$1');
		$routes->add('confirm_payment', 'Account\Transaction::confirm_payment');
		$routes->add('detail_offer/(:any)', 'Account\Offers::detail/$1');
	});
	$routes->group('classroom', function($routes)
	{
		$routes->add('dashboard', 'Classroom\Dashboard::index');
		$routes->add('dashboard/(:segment)', 'Classroom\Dashboard::$1');
		$routes->add('room', 'Classroom\Room::index');
		$routes->add('room/(:segment)', 'Classroom\Room::$1');
		$routes->add('information', 'Classroom\Information::index');
		$routes->add('detail_information/(:any)', 'Classroom\Informations::detail/$1');
	});
});

// Administrator Routes

$routes->get('/administrator', 'Administrator\Dashboard::index', ['filter' => 'admin_auth']);
$routes->get('administrator/login','Administrator\Authenticate::index', ['filter' => 'admin_logedin']);
$routes->get('administrator/logout','Administrator\Authenticate::logout', ['filter' => 'admin_auth']);

$routes->group('/administrator', ['filter' => 'admin_auth'], function($routes)
{

	$routes->add('dashboard', 'Administrator\Dashboard::index');
	$routes->add('member', 'Administrator\Member::index');
	$routes->add('member/(:segment)', 'Administrator\Member::detail/$1');
	$routes->add('transaction', 'Administrator\Transaction::index');
	$routes->add('confirm_payment', 'Administrator\Transaction::confirm_payment');
	$routes->add('list_payment', 'Administrator\Transaction::list_payment');
	$routes->add('history_transaction', 'Administrator\Transaction::list_history');
	$routes->add('transaction/(:segment)', 'Administrator\Transaction::$1');


	$routes->add('suspend_member', 'Administrator\Member::suspend');
	$routes->add('activate_member', 'Administrator\Member::activate');
	$routes->add('offer', 'Administrator\Offer::index');
	$routes->add('offer/(:segment)', 'Administrator\Offer::$1');
	$routes->add('information', 'Administrator\Information::index');
	$routes->add('information/(:segment)', 'Administrator\Information::$1');
});




/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
