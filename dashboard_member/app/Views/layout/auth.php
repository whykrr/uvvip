<html><head>
	<title>UVVIP | Dashboard</title>
	<link rel="icon" href="<?= base_url('component/image/asset/app-logo.png'); ?>">

	<!-- Javascript External -->
	<script src="<?= base_url('component/external/js/jquery.js'); ?>"></script>
	<script src="<?= base_url('component/external/js/bootstrap.min.js'); ?>"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url('component/internal/js/button_action.js'); ?>"></script>
	<script src="<?= base_url('component/internal/js/modal.js'); ?>"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/external/css/bootstrap.min.css'); ?>">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/authenticate.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/loading.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/modal.css'); ?>">
</head>

<body class="login-body">
	<div class="loading">
		<img src="<?= base_url('component/image/asset/loading.gif') ?>" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="background-glass">
	</div>
	<div class="login-container">
	<div class="img-container">
		<img src="<?= base_url('component/image/asset/app-logo.png'); ?>" alt="">
		<!-- UVVIP -->
	</div>
	<div class="form-container">
		<?= $this->renderSection('content'); ?>
	</div>
</div>
<script src="<?= base_url('component/internal/js/general.js'); ?>"></script>
</body></html>