<!DOCTYPE html>
<html>
<head>
	<title>UVVIP | Dashboard</title>
	<link rel="icon" href="<?= base_url('component/image/asset/app-logo.png'); ?>">

	<!-- Javascript External -->
	<script src="<?= base_url('component/external/js/jquery.js') ?>"></script>
	<script src="<?= base_url('component/external/js/bootstrap.min.js') ?>"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url('component/internal/js/button_action.js') ?>"></script>
	<script src="<?= base_url('component/internal/js/modal.js') ?>"></script>
	

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/external/css/bootstrap.min.css') ?>">
	
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/dashboard.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/modal.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/loading.css') ?>">
</head>

<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url('component/image/asset/loading.gif') ?>" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="background-glass">
	</div>
	<div class="header-container">
		<div class="logo-container">
			<img src="<?= base_url('component/image/asset/login-logo.png') ?>" alt="">	
		</div>
		<a class="telegram-container" href="https://t.me/6283833134333">
			<img src="<?= base_url('component/image/asset/telegram.png') ?>" alt="">
		</a>
		<div class="logout-container logout-btn" data-link="<?= base_url('logout') ?>">
			<img src="<?= base_url('component/image/asset/wt-logout.png') ?>" alt="">
			<p>Logout</p>
		</div>
	</div>
	<div class="announcement pt-2 pb-1">
		<marquee scrolldelay="100"><?= $announcement; ?></marquee>	
	</div>
	<div class="content-container">
		<div class="content-component">
			<?php 
			$users = session()->get('users');
			?>
			<div class="text-1">Selamat Datang</div>
			<div class="textc-1"><?= strtoupper($users['name']); ?></div>
			<div class="text-1">Semoga Hari anda Menyenangkan</div>
			<br>
			<br>
			<div class="text-3 running-time"></div>
			<div class="text-3 running-date"></div>
		</div>
		<div class="right-component">
			<div class="right-head p-1">
				<h4 class="m-0">INFORMASI</h4>
			</div>
			<div class="right-body">
				<?php
				foreach ($information as $data) { 
					$date = date('Y-m-d', strtotime($data['created_at']));
					$time = date('H:i:s', strtotime($data['created_at']));
					?>
					<a href="<?= base_url('classroom/detail_information/' . $data['slug']); ?>" class="text-decoration-none text-reset">
						<div class="card mb-3">
							<div class="row no-gutters">
								<div class="col-md-12">
									<div class="card-body p-2">
										<h5 class="card-title text-left"><?= $data['title']; ?></h5>
										<p class="card-text text-left"><?= readMoreHelper($data['content']); ?></p>
										<p class="card-text text-left"><small class="text-muted"><?= tgl_indo($date); ?> <?= $time; ?></small></p>
									</div>
								</div>
							</div>
						</div>
					</a>
					<?php
				} ?>
			</div>
		</div>
	</div>
	<div class="footer-container">
		<a class="button-menu" href="<?= base_url('account'); ?>">
			<div class="img-menu">
				<img src="<?= base_url('component/image/asset/wt-curriculum.png') ?>" alt="">
			</div>
			<div class="header-menu">
				AKUN SAYA
			</div>
			<div class="desc-menu">
				Informasi tentang personal data dan akses sistem
			</div>
		</a>
		<a class="button-menu" href="<?= base_url('classroom'); ?>">
			<div class="img-menu">
				<img src="<?= base_url('component/image/asset/wt-speed.png') ?>" alt="">
			</div>
			<div class="header-menu">
				KELAS
			</div>
			<div class="desc-menu">
				Pergi ke kelas untuk memulai aktifitas
			</div>
		</a>
		<a class="button-menu coming-soon" href="#">
			<div class="img-menu">
				<img src="<?= base_url('component/image/asset/wt-backup.png') ?>" alt="">
			</div>
			<div class="header-menu">
				PARTNERSHIP
			</div>
			<div class="desc-menu">
				Merupakan program kerjasama yang ditawarkan kepada setiap member.
			</div>
		</a>
	</div>
</body>
<script src="<?= base_url('component/internal/js/general.js') ?>"></script>
</html>