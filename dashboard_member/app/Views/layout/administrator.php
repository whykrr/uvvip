<!DOCTYPE html>
<html>

<head>
	<title>UVVIP | Dashboard</title>
	<link rel="icon" href="<?= base_url('component/image/asset/app-logo.png'); ?>">

	<!-- Javascript External -->
	<script src="<?= base_url('component/external/js/jquery.js') ?>"></script>
	<script src="<?= base_url('component/external/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('component/external/js/highcharts.js') ?>"></script>
	<script src="<?= base_url('component/external/js/highcharts-exporting.js') ?>"></script>
	<script src="<?= base_url('component/external/js/highcharts-export-data.js') ?>"></script>
	<script src="<?= base_url('component/external/js/highcharts-accessibility.js') ?>"></script>

	<!-- Javascript Internal -->
	<script src="<?= base_url('component/internal/js/button_action.js') ?>"></script>
	<script src="<?= base_url('component/internal/js/modal.js') ?>"></script>
	<script src="<?= base_url('component/internal/js/general.js') ?>"></script>

	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/external/css/bootstrap.min.css') ?>">
	<link href="<?= base_url('component/template/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">

	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/form.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/content.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/button_action.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/modal.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/loading.css') ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
</head>

<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url('component/image/asset/loading.gif') ?>" alt="">
	</div>
	<div class="modal-item"></div>
	<?php
	$users = session()->get('users');

	$words = explode(" ", $users['name']);
	$acronym = "";
	$initial = 1;

	if ($users['name']) {
		foreach ($words as $w) {
			if ($initial <= 2) {
				$acronym .= substr($w, 0, 1);
				$initial++;
			}
		}
	}
	?>
	<div class="header-container">
		<div class="rounded clr-1">
			<?php
			if ($menu == 'account') {
				echo 'Akun Saya';
			} else if ($menu == 'classroom') {
				echo 'Kelas';
			}
			?>
		</div>
		<div class="rounded clr-trans">
			<div class="account-img clr-1 rounded">
				<label class="account-init"><?= strtoupper($acronym); ?></label>
				<?php
				if ($users['photo']) {
				?>
					<img src="<?= base_url('component/image/profile_photo/' . $users['photo']); ?>">
				<?php
				}
				?>
			</div>
			<label class="account-name"><?= $users['name']; ?></label>
		</div>
		<a href="<?= base_url('dashboard'); ?>" class="pull-right rounded btn-danger">Close</a>
	</div>
	<div class="content-container">
		<div class="col col-content p-2">
			<?= $this->renderSection('content'); ?>
		</div>
		<div class="col col-menu p-2">
			<div class="menu-logo">
				<img src="<?= base_url('component/image/asset/app-logo.png') ?>" alt="">
			</div>
			<?php
			if ($menu == 'account') {
			?>
				<div class="menu-button">
					<a href="<?= base_url('account/profile'); ?>" class="menu-item <?= ($menu_active == 'profile') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/avatar.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Profil</div>
							<div class="menu-desc">Mengetahui detail pengguna</div>
						</div>
					</a>
				</div>
				<div class="menu-button">
					<a href="<?= base_url('account/class_shop'); ?>" class="menu-item <?= ($menu_active == 'class_shop') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/kelas.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Kelas</div>
							<div class="menu-desc">Melakukan pembelian kelas</div>
						</div>
					</a>
				</div>
				<div class="menu-button">
					<a href="<?= base_url('account/transaction'); ?>" class="menu-item <?= ($menu_active == 'transaction') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/pay.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Pendaftaran</div>
							<div class="menu-desc">Data pendaftaran yang anda lakukan dan konfirmasi pembayaran</div>
						</div>
					</a>
				</div>
				<div class="menu-button" style="display: none">
					<a href="#" class="menu-item coming-soon <?= ($menu_active == 'study') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/form.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Laporan Study</div>
							<div class="menu-desc">Untuk mengetahui perkembangan studi</div>
						</div>
					</a>
				</div>
				<div class="menu-button">
					<a href="<?= base_url('account/offers'); ?>" class="menu-item <?= ($menu_active == 'offers') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/product.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Penawaran</div>
							<div class="menu-desc">Temukan berbagai penawaran menarik dari UVVIP</div>
						</div>
					</a>
				</div>
			<?php
			} else if ($menu == 'classroom') {
			?>
				<div class="menu-button">
					<a href="<?= base_url('classroom/dashboard'); ?>" class="menu-item <?= ($menu_active == 'dashboard') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/web.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Dashboard</div>
							<div class="menu-desc">Merupakan preview dari aktifitas yang anda lakukan</div>
						</div>
					</a>
				</div>
				<div class="menu-button">
					<a href="<?= base_url('classroom/room'); ?>" class="menu-item <?= ($menu_active == 'room') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/kelas.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Kelas</div>
							<div class="menu-desc">Tempat memulai aktifitas untuk mengenal dunia cryptocurrency</div>
						</div>
					</a>
				</div>
				<div class="menu-button">
					<a href="<?= base_url('classroom/informations'); ?>" class="menu-item <?= ($menu_active == 'informations') ? 'active' : ''; ?>">
						<div class="menu-icon">
							<img src="<?= base_url('component/image/asset/trading.png') ?>" alt="">
						</div>
						<div class="menu-text">
							<div class="menu-title">Informasi</div>
							<div class="menu-desc">Tempat untuk mengetahui program dan penawaran dari UVVIP</div>
						</div>
					</a>
				</div>
			<?php
			}
			?>

		</div>
	</div>
	<div class="footer-container">
		<label>Nine Cloud 2019</label>
	</div>
</body>
<script src="<?= base_url('component/internal/js/general.js') ?>"></script>
<script src="<?= base_url('component/template/vendor/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('component/template/vendor/datatables/dataTables.bootstrap4.min.js'); ?>"></script>

<script type="text/javascript">
	$(document).ready(function() {
		table = $('.table').DataTable();
	})

	function shp(obj) {
		if ($(obj).parent().find('input').attr("type") == "text") {
			$(obj).parent().find('input').attr('type', 'password');
			$(obj).find('i').addClass("fa-eye-slash");
			$(obj).find('i').removeClass("fa-eye");
		} else if ($(obj).parent().find('input').attr("type") == "password") {
			$(obj).parent().find('input').attr('type', 'text');
			$(obj).find('i').removeClass("fa-eye-slash");
			$(obj).find('i').addClass("fa-eye");
		}
	}
</script>

</html>