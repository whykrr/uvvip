<?= $this->extend('layout/auth') ?>

<?= $this->section('content') ?>
<form class="form-ajax mb-0" data-uri="<?= base_url('authenticate/act_register'); ?>" data-redirect="<?= base_url('dashboard'); ?>">
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/avatar.png'); ?>" alt="">
		</div>
		<input type="text" class="form-control" name="name" placeholder="Nama Lengkap">			
		<label>Nama Lengkap</label>
	</div>
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/avatar.png'); ?>" alt="">
		</div>
		<input type="text" class="form-control" name="phone" placeholder="Nomor Telepon">			
		<label>Nomor Telepon</label>
	</div>
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/avatar.png'); ?>" alt="">
		</div>
		<input type="email" class="form-control" name="email" placeholder="Email">			
		<label>Email</label>
	</div>
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/avatar.png'); ?>" alt="">
		</div>
		<input type="text" class="form-control" name="username" placeholder="Username">			
		<label>Usename</label>
	</div>
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/lock.png') ?>" alt="">
		</div>
		<input type="password" class="form-control" name="password" placeholder="Password">	
		<label>Password</label>
	</div>
	<button class="btn-login btn-lg btn-block" type="submit">REGISTRATION</button>
	<div class="fpass-container mt-2">Sudah punya akun ? <a href="<?= base_url('login'); ?>">Login</a></div>
</form>
<?= $this->endSection('content'); ?>
