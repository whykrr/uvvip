<?= $this->extend('layout/auth') ?>

<?= $this->section('content') ?>
<form class="form-ajax mb-0" data-uri="<?= base_url('authenticate/act_login'); ?>" data-redirect="<?= (session()->get('last_page')) ? base_url(session()->get('last_page')) : base_url('dashboard'); ?>">
	<?= csrf_field(); ?>
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/avatar.png'); ?>" alt="">
		</div>
		<input type="text" class="form-control" name="username" placeholder="Username">			
		<label>Username</label>
	</div>
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/lock.png') ?>" alt="">
		</div>
		<input type="password" class="form-control" name="password" placeholder="Password">
		<label>Password</label>
	</div>
	<div class="fpass-container mb-2">Lupa password ? <a href="<?= base_url('forget_password'); ?>">Klik disini</a></div>
	<button class="btn-login btn-lg btn-block" type="submit">LOGIN</button>
	<hr class="my-2">
	<a href="<?= base_url('register'); ?>" class="btn btn-lg btn-block btn-primary btn-rounded" >REGISTRATION</a>

</form>
<?= $this->endSection('content'); ?>
