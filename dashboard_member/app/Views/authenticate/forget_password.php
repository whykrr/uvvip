<?= $this->extend('layout/auth') ?>

<?= $this->section('content') ?>
<h3 class="text-white mb-4">Form Lupa Password</h3>
<form class="form-ajax mb-0" data-uri="<?= base_url('authenticate/act_fpassword'); ?>" data-redirect="<?= base_url('login'); ?>">
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/avatar.png'); ?>" alt="">
		</div>
		<input type="email" class="form-control" name="email" placeholder="Email">			
		<label>Email</label>
	</div>
	<div class="form-group">
		<div class="fg-img">
			<img src="<?= base_url('component/image/asset/avatar.png'); ?>" alt="">
		</div>
		<input type="text" class="form-control" name="phone" placeholder="Nomor Telepon">			
		<label>Nomor Telepon</label>
	</div>
	<button class="btn-login btn-lg btn-block" type="submit">Kirim Password Baru</button>
	<div class="fpass-container mt-2">Kembali ke <a href="<?= base_url('login'); ?>">Login</a></div>
</form>
<?= $this->endSection('content'); ?>
