<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/announcement'); ?>">Pengumuman</a></li>
		<li class="breadcrumb-item active" aria-current="page">Form <?= ($action == 'add') ? 'Tambah' : 'Ubah'; ?> Pengumuman</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form <?= ($action == 'add') ? 'Tambah' : 'Ubah'; ?> Pengumuman</h6>
	</div>
	<div class="card-body">
		<form class="form-row" method="post" action="<?= ($action == 'add') ? base_url('administrator/announcement/act_add') : base_url('administrator/announcement/act_edit'); ?>" enctype="multipart/form-data">
			<div class="col-md-12 mb-3">
				<?= ($action == 'add') ? '' : "<input type='hidden' name='id' value='$data[id]'>"; ?>
				<label for="content_title">Konten Pengumuman</label>
				<?php old('content'); ?>
				<?= getInput('content', 'text', $validation, @$data['content']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary float-right">Simpan</button>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>