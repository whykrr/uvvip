<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page">Interaksi</li>
	</ol>
</nav>
<?php
if (!empty(session()->getFlashdata('message'))) { ?>
	<div class="alert alert-success">
		<?php echo session()->getFlashdata('message'); ?>
	</div>
<?php
}
?>
<div class="table-responsive">
	<table class="table table-borderless" id="dataTable" width="100%" cellspacing="0">
		<thead>
			<tr>
				<td class="p-0">
					<h3>Interaksi Member</h3>
				</td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($interaksi as $row) {

				if ($row['attachment'] != '') {
					$photo = base_url('component/image/uploads_photo/' . $row['attachment']);
				} else {
					$photo = base_url('component/image/default-content.png');
				}
			?>
				<tr>
					<td class="p-0">
						<div class="card shadow mb-4">
							<div class="card-header py-3">
								<a href="<?= base_url('administrator/interaction/delete_feed/' . $row['id']); ?>" class="btn btn-danger btn-icon-split float-right" data-toggle="modal" data-target="#deleteModal">
									<span class="icon text-white-50">
										<i class="fas fa-trash"></i>
									</span>
									<span class="text">Hapus</span>
								</a>
								<h5 class="d-inline float-left mb-0"><?= $row['name']; ?></h5><br>
								<?php
								$date = date('Y-m-d', strtotime($row['created_at']));
								$time = date('H:i', strtotime($row['created_at']));
								?>
								<p class="d-inline float-left mb-0"><small class="text-muted"><?= tgl_indo($date); ?> <?= $time; ?></small></p>
							</div>
							<div class="card-body">
								<div class="row">

									<div class="col-lg-4">
										<img src="<?= $photo; ?>" class="img-fluid">
										<div class="row" style="margin-top: 15px;">
											<div class="col-lg-7">
												Disukai oleh <span id="total_like<?= $row['id']; ?>"><?= $row['total_like']; ?></span> orang
											</div>
											<div class="col-lg-5">
												<span id="total_comment<?= $row['id']; ?>"><?= $row['total_comment']; ?></span> Komentar
											</div>
										</div>
									</div>
									<div class="col-lg-8 pl-0">
										<h4><?= $row['title']; ?></h4>
										<p><?= $row['content']; ?></p>
									</div>
									<div class="col-lg-12">
										<hr>
										<div class="d-flex justify-content-center">

											<?php
											$users = session()->get('users');
											$likes = $komen->ForumLike($row['id'], $users['id']);

											if ($likes > 0) {
												$text = 'Unlike';
											} else {
												$text = 'Like';
											}
											?>
										</div>
										<hr>
										<h4>Komentar</h4>
										<div class="p-0" id="cont-comment<?= $row['id']; ?>" style="max-height: 110px;overflow-y: scroll;overflow-x: hidden;">
											<?php
											$komentar = $komen->ForumComment($row['id'])->getResultArray();

											foreach ($komentar as $kom) {
											?>
												<div class="row mb-0">
													<div class="col-lg-12">
														<div class="card card-comment">
															<div class="card-body p-1">
																<h6 class="mb-0 d-inline"><?= $kom['name']; ?></h6>
																<p class="mb-0 d-inline"><?= $kom['comment']; ?></p>
																<?php
																$datepb = date('Y-m-d', strtotime($kom['created_at']));
																$timepb = date('H:i', strtotime($kom['created_at']));
																?>
																<p class="card-text"><small class="text-muted"><?= tgl_indo($datepb); ?> <?= $timepb; ?></small></p>
															</div>
														</div>
													</div>
												</div>
											<?php } ?>
										</div>
										<hr>
									</div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<?= $this->endSection('content'); ?>