<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page">Penawaran</li>
	</ol>
</nav>
<div class="row">
	<div class="col-lg-12">
		<a href="<?= base_url('administrator/information/add'); ?>" class="btn btn-success btn-icon-split float-right ">
			<span class="icon text-white-50">
				<i class="fas fa-plus"></i>
			</span>
			<span class="text">Tambah Penawaran</span>
		</a>
	</div>
</div>
<div class="row">
</div>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<div class="col-sm-12 px-0">
			<?php 
			if(!empty(session()->getFlashdata('message'))) { ?>
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message');?>
				</div>
				<?php
			} 
			?>
		</div>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>Judul</th>
						<th>Banner</th>
						<th>Konten</th>
						<th>Tanggal Terbit</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($list as $data) {
						$ext = pathinfo($data['banner_img'], PATHINFO_EXTENSION); 
						?>
						<tr>
							<td><?= $data['title']; ?></td>
							<td class="w-25">
								<?php
								if($ext=='mp4' || $ext =='flv'){
									?>
									<video controls height="200px">
										<source src="<?= base_url('component/image/informations/' . $data['banner_img']); ?>"></source>
									</video>
									<?php
								}else{
									?>
									<img src="<?= base_url('component/image/informations/' . $data['banner_img']); ?>" class="img-thumbnail"></img>
									<?php
								}
								?>
							</td>
							<td class="w-25"><?= readMoreHelper($data['content'], 100); ?></td>
							<td><?= tgl_indo(date("Y-m-d", strtotime($data['created_at']))); ?></td>
							<td>
								<a href="<?= base_url('administrator/information/detail/'. $data['slug']); ?>" class="btn btn-info btn-block">Detail</a>
								<a href="<?= base_url('administrator/information/edit/'. $data['slug']); ?>" class="btn btn-warning btn-block">Ubah</a>
								<a href="<?= base_url('administrator/information/act_delete/'. $data['id']); ?>" class="btn btn-danger btn-block mt-2" data-toggle="modal" data-target="#deleteModal">Hapus</button>
								</td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<?= $this->endSection('content'); ?>