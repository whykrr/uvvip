<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/information'); ?>">Penawaran</a></li>
		<li class="breadcrumb-item active" aria-current="page">Form <?= ($action == 'add') ? 'Tambah' : 'Ubah'; ?> Penawaran</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form <?= ($action == 'add') ? 'Tambah' : 'Ubah'; ?> Penawaran</h6>
	</div>
	<div class="card-body">
		<form class="form-row" method="post" action="<?= ($action == 'add') ? base_url('administrator/information/act_add') : base_url('administrator/information/act_edit'); ?>" enctype="multipart/form-data">
			<div class="col-md-12 mb-3">
				<?= ($action == 'add') ? '' : "<input type='hidden' name='id' value='$data[id]'>"; ?>
				<label for="content_title">Judul</label>
				<?php old('title'); ?>
				<?= getInput('title', 'text', $validation, @$data['title']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="banner_img">Banner</label>
				<?= getInput('banner_img', 'file', $validation, @$data['banner_img']); ?>
				<img src="<?= ($action == 'add') ? '' : base_url('component/image/informations/' . $data['banner_img']); ?>" class="mt-2 w-25 img-thumbnail img-preview">
				<video controls class="mt-2">
					<?php 
					$ext = pathinfo(@$data['banner_img'], PATHINFO_EXTENSION); 
					if($ext == 'mp4'){
						?>
						<source src="<?= ($action == 'add') ? '' : base_url('component/image/informations/' . $data['banner_img']); ?>"></source>
						<?php
					}
					?>
				</video>
			</div>
			<div class="col-md-12 mb-3">
				<label for="content_wysiwig">Konten</label>
				<?= getTextarea('content_wysiwig', $validation, @$data['content_wysiwig']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary float-right">Simpan</button>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>