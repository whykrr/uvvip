<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/transaction'); ?>"><?= ($sub_page=='history') ? 'Riwayat ' : '' ; ?>Pendaftaran</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Pendaftaran</li>
	</ol>
</nav>
<div class="card shadow mb-4 mt-3">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Detail Pendaftaran <?= $data['transaction_code']; ?></h6>
	</div>
	<div class="card-body">
		<?php
		$date = date('Y-m-d', strtotime($data['created_at']));
		$time = date('H:i:s', strtotime($data['created_at']));
		?>
		<div class="row mx-1">
			<?php 
			if(!empty(session()->getFlashdata('message'))) { ?>
				<div class="col-md-12 p-1">
					<div class="alert alert-success">
						<?php echo session()->getFlashdata('message');?>
					</div>
				</div>
				<?php
			} 
			?>
			<div class="col-md-6 p-1">
				<label>Nomor Pendaftaran</label>
				<input disabled class="form-control" value="<?= $data['transaction_code']; ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Nama Pengguna</label>
				<input disabled class="form-control" value="<?= $data['name']; ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Tanggal</label>
				<input disabled class="form-control" value="<?= tgl_indo($date); ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Waktu</label>
				<input disabled class="form-control" value="<?= $time; ?>">
			</div>
			<div class="col-md-12 p-1">
				<label>Tipe Paket</label>
				<input disabled class="form-control" value="<?= $data['package_name']; ?>">
			</div>
			<div class="col-md-12 p-1">
				<label>Tipe Kelas</label>
				<input disabled class="form-control" value="<?= $data['class_name']; ?>">
			</div>
			<div class="col-md-12 p-1">
				<label>Status Pembayaran</label><br>
				<span class="badge badge-<?= $sp['style']; ?> p-2"><?= $sp['status']; ?></span>
			</div>
			<div class="col-md-12 p-1">
				<br>
				<?php
				if($sp['status'] == 'Review'){
					?>
					<a href="<?= base_url('administrator/confirm_payment?code='.$data['transaction_code']); ?>" class="btn btn-primary float-right">Konfirmasi Pembayaran</a>
					<?php
				}else if($sp['style'] == 'danger'){
					?>
					<form method="post" action="<?= base_url('administrator/transaction/repayment'); ?>">
						<input type="hidden" name="id" value="<?= $data['id']; ?>">
						<input type="hidden" name="class_id" value="<?= $data['class_id']; ?>">
						<input type="hidden" name="user_id" value="<?= $data['user_id']; ?>">
						<input type="hidden" name="transaction_code" value="<?= $data['transaction_code']; ?>">
						<button type="submit" class="btn btn-info float-right">Pembayaran Ulang</button>
					</form>
					<?php
				}
				?>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>