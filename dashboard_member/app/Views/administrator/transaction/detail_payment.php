<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/transaction'); ?>">Pendaftaran</a></li>
		<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/list_payment'); ?>">Konfirmasi Pembayaran</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Konfirmasi Pembayaran</li>
	</ol>
</nav>
<div class="card shadow mb-4 mt-3">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Detail Pembayaran <?= $data['transaction_code']; ?></h6>
	</div>
	<div class="card-body">
		<?php
		$date = date('Y-m-d', strtotime($data['created_at']));
		$time = date('H:i:s', strtotime($data['created_at']));
		?>
		<div class="row mx-1">

			<div class="col-md-6 p-1">
				<label>Nomor Pendaftaran</label>
				<input disabled class="form-control" value="<?= $data['transaction_code']; ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Nama Pengguna</label>
				<input disabled class="form-control" value="<?= $data['user']; ?>">
			</div>
			<div class="col-md-12 p-1">
				<label>Bank Pengirim</label>
				<input disabled class="form-control" value="<?= $data['bank']; ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Nama Pengirim</label>
				<input disabled class="form-control" value="<?= $data['bank_acc_name']; ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Rekening Pengirim</label>
				<input disabled class="form-control" value="<?= $data['bank_acc']; ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Tanggal</label>
				<input disabled class="form-control" value="<?= tgl_indo($date); ?>">
			</div>
			<div class="col-md-6 p-1">
				<label>Waktu</label>
				<input disabled class="form-control" value="<?= $time; ?>">
			</div>
			<div class="col-md-12 p-1">
				<label>Tipe Paket</label>
				<input disabled class="form-control" value="<?= $data['package']; ?>">
			</div>
			<div class="col-md-12 p-1">
				<label>Tipe Kelas</label>
				<input disabled class="form-control" value="<?= $data['class']; ?>">
			</div>
			<div class="col-md-12 p-1">
				<label class="d-block mt-2 mb-0">Bukti Transfer</label>
				<img src="<?= base_url('component/image/payment/'.$data['attachment']); ?>" class="mt-2 w-50 img-thumbnail img-preview">
			</div>
			<div class="col-md-12 p-1">
				<br>
				<form method="post" action="<?= base_url('administrator/transaction/accept_payment'); ?>">
					<input type="hidden" name="id" value="<?= $data['id']; ?>">
					<input type="hidden" name="user_id" value="<?= $data['user_id']; ?>">
					<input type="hidden" name="class_id" value="<?= $data['class_id']; ?>">
					<input type="hidden" name="transaction_code" value="<?= $data['transaction_code']; ?>">
					<button type="submit" class="btn btn-success float-right">Terima Pembayaran</button>
				</form>
				<form method="post" action="<?= base_url('administrator/transaction/reject_payment'); ?>">
					<input type="hidden" name="id" value="<?= $data['id']; ?>">
					<input type="hidden" name="user_id" value="<?= $data['user_id']; ?>">
					<input type="hidden" name="class_id" value="<?= $data['class_id']; ?>">
					<input type="hidden" name="transaction_code" value="<?= $data['transaction_code']; ?>">
					<button type="submit" class="btn btn-danger float-right mr-2">Tolak Pembayaran</button>
				</form>
			</div>
		</div>
	</div>
</div>

<?= $this->endSection('content'); ?>