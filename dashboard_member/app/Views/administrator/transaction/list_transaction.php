<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page"><?= ($sub_page=='history') ? 'Riwayat ' : '' ; ?>Pendaftaran</li>
	</ol>
</nav>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message');?>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th scope="col">No</th>
							<th scope="col">Kode Pendaftaran</th>
							<th scope="col">Nama Member</th>
							<th scope="col">Paket</th>
							<th scope="col">Kelas</th>
							<th scope="col">Tanggal Pendaftaran</th>
							<th scope="col">Status Pembayaran</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;
						foreach ($list as $data) {
							?>
							<tr>
								<th scope="row"><?= $no++; ?></th>
								<td><?= $data['transaction_code']; ?></td>
								<td><?= $data['name']; ?></td>
								<td><?= $data['package_name']; ?></td>
								<td><?= $data['class_name']; ?></td>
								<td><?= tgl_indo($data['created_at']); ?></td>
								<td><span class="badge badge-<?= $data['ps_style']; ?> p-2"><?= $data['payment_status']; ?></span></td>
								<td><a href="<?= base_url("administrator/transaction/detail/$data[transaction_code]?sub=$sub_page"); ?>" class="btn btn-success btn-sm">Detail</a></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>