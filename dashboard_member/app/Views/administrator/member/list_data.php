<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page">Member</li>
	</ol>
</nav>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message');?>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>Nama</th>
						<th>E-Mail</th>
						<th>Nomor Telepon</th>
						<th>Member Sejak</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($member as $data) {
						?>
						<a href="#1">
							<tr>
								<td><?= $data['name']; ?></td>
								<td><?= $data['email']; ?></td>
								<td><?= $data['phone']; ?></td>
								<td><?= tgl_indo(date("Y-m-d", strtotime($data['created_at']))); ?></td>
								<td>
									<?php 
									if ($data['status'] == 'Active') {
										?>
										<h4><span class="badge badge-success p-2">Aktif</span></h4>
										<?php
									}else{
										?>
										<h4><span class="badge badge-danger p-2">Ditangguhkan</span></h4>
										<?php
									} ?>
								</td>
								<td><a href="<?= base_url('administrator/member/'. $data['username']); ?>" class="btn btn-info">Detail</a></td>
							</tr>
						</a>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>