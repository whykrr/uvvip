<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/member'); ?>">Member</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Member</li>
	</ol>
</nav>
<?php 
if(!empty(session()->getFlashdata('message'))) { ?>
	<div class="col-sm-12 px-0">
		<div class="alert alert-info">
			<?php echo session()->getFlashdata('message');?>
		</div>
	</div>
	<?php
} 
?>
<div class="row">
	<div class="col-lg-6">

		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Foto Profil</h6>
			</div>
			<div class="card-body">
				<div class="container-pp p-2">
					<div class="rounded-pp">
						<?php 
						if($detail['photo']){
							?>
							<img src="<?= base_url('component/image/profile_photo/' . $detail['photo']); ?>">
							<?php
						}
						?>
						<div class="name-tag">
							<?= getAcronym($detail['name']); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Sosial Media</h6>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-sm-4">
						<label>Facebook</label>
						<input type="text" readonly class="form-control" value="<?= $detail['facebook']; ?>">
					</div>
					<div class="col-sm-4">
						<label>Instagram</label>
						<input type="text" readonly class="form-control" value="<?= $detail['instagram']; ?>">
					</div>
					<div class="col-sm-4">
						<label>Twitter</label>
						<input type="text" readonly class="form-control" value="<?= $detail['twitter']; ?>">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6">

		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Personal Data</h6>
			</div>
			<div class="card-body">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Nama</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control" value="<?= $detail['name']; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">E-Mail</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control" value="<?= $detail['email']; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Nomor Telepon</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control" value="<?= $detail['phone']; ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Alamat</label>
					<div class="col-sm-8">
						<textarea class="form-control" readonly><?= $detail['address']; ?></textarea>
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Bergabung Sejak</label>
					<div class="col-sm-8">
						<input type="text" readonly class="form-control" value="<?= tgl_indo(date("Y-m-d", strtotime($detail['created_at']))); ?>">
					</div>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Status</label>
					<div class="col-sm-8">
						<?php 
						if ($detail['status'] == 'Active') {
							?>
							<h4><span class="badge badge-success p-2">Aktif</span></h4>
							<?php
						}else{
							?>
							<h4><span class="badge badge-danger p-2">Ditangguhkan</span></h4>
							<?php
						} ?>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<?php 
						if ($detail['status'] == 'Active') {
							?>
							<form action="<?= base_url('administrator/suspend_member'); ?>" method="post">
								<input type="hidden" name="id" value="<?= $detail['id']; ?>">
								<input type="hidden" name="status" value="Suspend">
								<input type="hidden" name="username" value="<?= $detail['username']; ?>">
								<button type="submit" class="btn btn-danger">Tangguhkan</button>
							</form>
							<?php
						}else{
							?>
							<form action="<?= base_url('administrator/activate_member'); ?>" method="post">
								<input type="hidden" name="id" value="<?= $detail['id']; ?>">
								<input type="hidden" name="status" value="Active">
								<input type="hidden" name="username" value="<?= $detail['username']; ?>">
								<button type="submit" class="btn btn-success">Aktifkan</button>
							</form>
							<?php
						} ?>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="col-lg-12">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Pendaftaran</h6>
			</div>
			<div class="card-body">
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					<thead>
						<tr>
							<th scope="col">No</th>
							<th scope="col">Kode Pendaftaran</th>
							<th scope="col">Paket</th>
							<th scope="col">Kelas</th>
							<th scope="col">Tanggal Pendaftaran</th>
							<th scope="col">Status Pembayaran</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;
						foreach ($transaction as $data) {
							?>
							<tr>
								<th scope="row"><?= $no++; ?></th>
								<td><?= $data['transaction_code']; ?></td>
								<td><?= $data['package_name']; ?></td>
								<td><?= $data['class_name']; ?></td>
								<td><?= tgl_indo($data['created_at']); ?></td>
								<td><?= ($data['payment_status'] == 'Unpaid')? '<span class="badge badge-warning p-2">UNPAID</span>' : '<span class="badge badge-success p-2">PAID</span>'; ?></td>
								<td><a href="<?= base_url("administrator/transaction/detail/$data[transaction_code]"); ?>" class="btn btn-success btn-sm">Detail Transaski</a></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>