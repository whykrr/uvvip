<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Kontes Trading</li>
		<li class="breadcrumb-item active" aria-current="page">Data Kontes Berlangsung</li>
	</ol>
</nav>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php
		if (!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message'); ?>
			</div>
		<?php
		}
		?>
		<?php
		if (!empty(session()->getFlashdata('message_danger'))) { ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata('message_danger'); ?>
			</div>
		<?php
		}
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>No</th>
						<th>Title</th>
						<th width="20%">Banner</th>
						<th>Description</th>
						<th>Tanggal Mulai</th>
						<th width="15%">Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($list as $data) {
					?>
						<a href="#1">
							<tr>
								<td><?= $no++; ?></td>
								<td><?= $data['title']; ?></td>
								<td><img src="<?= base_url('component/image/uploads_photo/' . $data['banner']); ?>" class="img-thumbnail"></img></td>
								<td><?= readMoreHelper($data['desc'], 50); ?></td>
								<td><?= tgl_indo($data['start_date']); ?></td>
								<td>
									<a href="<?= base_url('administrator/contest/participant/' . $data['slug']); ?>" class="btn btn-primary mt-2">Peserta</a>
									<a href="#" class="btn btn-warning mt-2" data-trading="<?= $data['id']; ?>" data-zoom="<?= $data['zoom_link']; ?>" data-toggle="modal" data-target="#zoomModal">Ubah Link ZOOM</a>
									<a href="#" class="btn btn-danger mt-2" data-trading="<?= $data['id'] ?>" data-title="Tutup Kontes <?= $data['title'] ?>" data-toggle="modal" data-target="#closeContestModal" data-backdrop="static">Tutup Kontes</button>
								</td>
							</tr>
						</a>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>
<?= $this->section('custom-modal'); ?>
<div class="modal fade" id="zoomModal" tabindex="-1" role="dialog" aria-labelledby="linkZoomModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="linkZoomModal">Ubah ZOOM</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="post" action="<?= base_url('administrator/contest/change_zoom'); ?>">
					<input type="hidden" name="id">
					<label for="link">Link ZOOM</label>
					<input type="url" name="zoom_link" class="form-control">
					<button class="btn btn-primary float-right mt-2">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="closeContestModal" tabindex="-1" role="dialog" aria-labelledby="closeTitleModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="closeTitleModal"></h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Apakah anda yakin untuk menutup kontes ini?</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
				<form action="<?= base_url('administrator/contest/close_contest') ?>" method="post">
					<input type="hidden" name="id" value="">
					<button type="submit" class="btn btn-primary">Ya</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('custom-modal'); ?>
<?= $this->section('custom-js'); ?>
<script type="text/javascript">
	$('#zoomModal').on('show.bs.modal', function(e) {
		$(this).find(':input[name=id]').val($(e.relatedTarget).data('trading'));
		$(this).find(':input[name=zoom_link]').val($(e.relatedTarget).data('zoom'));
	});
	$('#closeContestModal').on('show.bs.modal', function(e) {
		$(this).find(':input[name=id]').val($(e.relatedTarget).data('trading'));
		$(this).find('#closeTitleModal').text($(e.relatedTarget).data('title'));
	});
</script>
<?= $this->endSection('custom-js'); ?>