<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">Kontes Trading</li>
        <li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/contest'); ?>">Data Kontes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Calon Peserta</li>
    </ol>
</nav>
<div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Judul Kontes</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $contest['title']; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Annual) Card Example -->
    <div class="col-xl-4 col-md-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Peserta</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= count($list); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-md-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Modal Awal</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= format_rupiah($contest['coin_capital']); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card shadow mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Ranking</th>
                        <th>Nama</th>
                        <th>Telepon</th>
                        <th>Email</th>
                        <th>Hasil</th>
                        <th>Persentasi Profit</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($list as $data) {
                    ?>
                        <a href="#1">
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $data['user_name']; ?></td>
                                <td><?= $data['user_phone']; ?></td>
                                <td><?= $data['user_email']; ?></td>
                                <td><?= ($data['result']) ? format_rupiah($data['result']) : ''; ?></td>
                                <td><?= $data['result_percentage']; ?> <?= ($data['result_percentage']) ? '%' : ''; ?></td>
                                <td>
                                    <a href="<?= base_url('administrator/member/' . $data['user_username']); ?>" class="btn btn-info" target="_blank">Detail</a>
                                </td>
                            </tr>
                        </a>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('administrator/contest/history') ?>" class="btn btn-lg btn-danger float-right mt-2">Kembali</a>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>