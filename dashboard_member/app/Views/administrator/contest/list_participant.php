<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item" aria-current="page">Kontes Trading</li>
        <li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/contest'); ?>">Data Kontes</a></li>
        <li class="breadcrumb-item active" aria-current="page">Calon Peserta</li>
    </ol>
</nav>
<div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <div class="col-xl-4 col-md-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Judul Kontes</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= $contest['title']; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Annual) Card Example -->
    <div class="col-xl-4 col-md-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Peserta</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= count($list); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-md-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Modal Awal</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"><?= format_rupiah($contest['coin_capital']); ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card shadow mb-4 mt-3">
    <div class="card-body">
        <?php
        if (!empty(session()->getFlashdata('message'))) { ?>
            <div class="alert alert-success">
                <?php echo session()->getFlashdata('message'); ?>
            </div>
        <?php
        }
        ?>
        <?php
        if (!empty(session()->getFlashdata('message_danger'))) { ?>
            <div class="alert alert-danger">
                <?php echo session()->getFlashdata('message_danger'); ?>
            </div>
        <?php
        }
        ?>
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>Telepon</th>
                        <th>Email</th>
                        <th>Hasil</th>
                        <th>Persentasi Profit</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($list as $data) {
                    ?>
                        <a href="#1">
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $data['user_name']; ?></td>
                                <td><?= $data['user_phone']; ?></td>
                                <td><?= $data['user_email']; ?></td>
                                <td><?= ($data['result']) ? format_rupiah($data['result']) : ''; ?></td>
                                <td><?= $data['result_percentage']; ?> <?= ($data['result_percentage']) ? '%' : ''; ?></td>
                                <td>
                                    <?php if (!$data['result']) : ?>
                                        <a href="#" id="<?= $data['id'] ?>" data-label="Input Hasil <?= $data['user_name'] ?>" data-user="<?= $data['user_id'] ?>" class="btn btn-primary" data-toggle="modal" data-target="#resultTradingModal">Input Hasil</a>
                                    <?php endif; ?>
                                    <?php if ($data['result']) : ?>
                                        <a href="#" id="<?= $data['id'] ?>" data-label="Edit Hasil <?= $data['user_name'] ?>" data-user="<?= $data['user_id'] ?>" data-result="<?= $data['result'] ?>" class="btn btn-warning" data-toggle="modal" data-target="#resultTradingModal">Ubah Hasil</a>
                                    <?php endif; ?>
                                    <a href="<?= base_url('administrator/member/' . $data['user_username']); ?>" class="btn btn-info" target="_blank">Detail</a>
                                </td>
                            </tr>
                        </a>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a href="<?= base_url('administrator/contest/ongoing') ?>" class="btn btn-lg btn-danger float-right mt-2">Kembali</a>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>
<?= $this->section('custom-modal'); ?>
<div class="modal fade" id="resultTradingModal" tabindex="-1" role="dialog" aria-labelledby="resultModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="resultModalLabel"></h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-ajax" data-uri="<?= base_url('administrator/contest/input_result'); ?>">
                    <input type="hidden" name="id">
                    <input type="hidden" name="slug" value="<?= $contest['slug'] ?>">
                    <input type="hidden" name="capital" value="<?= $contest['coin_capital'] ?>">
                    <label for="result">Hasil</label>
                    <input type="number" class="form-control" name="result" onkeypress="return isNumber(event)">
                    <button type="submit" class="btn btn-primary float-right mt-2">Simpan</button>
                    <button disabled class="btn btn-primary float-right mt-2" id="button-load" hidden>
                        <i class="fa fa-spinner fa-spin"></i>Loading
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('custom-modal'); ?>
<?= $this->section('custom-js'); ?>
<script>
    $('#resultTradingModal').on('show.bs.modal', function(e) {
        $(this).find('#resultModalLabel').text($(e.relatedTarget).data('label'));
        $(this).find(':input[name=id]').val($(e.relatedTarget).attr('id'));
        if($(e.relatedTarget).data('result')){
            $(this).find(':input[name=result]').val($(e.relatedTarget).data('result'));
        }
    });
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode;
        if (iKeyCode = 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57 || iKeyCode == 190) )
            return false;
        return true;
    } 
</script>
<?= $this->endSection('custom-js'); ?>