<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Kontes Trading</li>
		<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/contest'); ?>">Data Kontes</a></li>
		<li class="breadcrumb-item active" aria-current="page">Tambah Kontes</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Tambah Kontes</h6>
	</div>
	<div class="card-body">
		<form class="form-row form-ajax" data-uri="<?= base_url('administrator/contest/insert_contest'); ?>">
			<div class="col-md-12 mb-3">
				<label for="content_title">Judul</label>
				<?= getInput('title', 'text', $validation); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="banner">Banner</label>
				<?= getInput('banner', 'file', $validation); ?>
				<img src="" class="mt-2 w-50 img-thumbnail img-preview">
			</div>
			<div class="col-md-6 mb-3">
				<label for="desc">Deskripsi Kontes</label>
				<?= getTextarea('desc', $validation); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Mulai Kontes</label>
				<?php $min_sd = date('Y-m-d', strtotime(date('Y-m-d'). '+1 day')) ?>
				<?= getInput('start_date', 'date', $validation, "", "min='$min_sd'"); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="coin_capital">Modal Awal</label>
				<?= getInput('coin_capital', 'number', $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary float-right">Simpan</button>
				<button disabled class="btn btn-primary float-right" id="button-load" hidden>
					<i class="fa fa-spinner fa-spin"></i>Loading
				</button>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>