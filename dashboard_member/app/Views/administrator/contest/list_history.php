<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Kontes Trading</li>
		<li class="breadcrumb-item active" aria-current="page">Data Riwayat Kontes</li>
	</ol>
</nav>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php
		if (!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message'); ?>
			</div>
		<?php
		}
		?>
		<?php
		if (!empty(session()->getFlashdata('message_danger'))) { ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata('message_danger'); ?>
			</div>
		<?php
		}
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>No</th>
						<th>Title</th>
						<th width="20%">Banner</th>
						<th>Description</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($list as $data) {
					?>
						<a href="#1">
							<tr>
								<td><?= $no++; ?></td>
								<td><?= $data['title']; ?></td>
								<td><img src="<?= base_url('component/image/uploads_photo/' . $data['banner']); ?>" class="img-thumbnail"></img></td>
								<td><?= readMoreHelper($data['desc'], 150); ?></td>
								<td>
									<a href="<?= base_url('administrator/contest/detail_history/' . $data['slug']); ?>" class="btn btn-info">Detail</a>
								</td>
							</tr>
						</a>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>