<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Kontes Trading</li>
		<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/contest'); ?>">Data Kontes</a></li>
		<li class="breadcrumb-item active" aria-current="page">Calon Peserta</li>
	</ol>
</nav>
<div class="row">

	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-6 col-md-6">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Judul Kontes</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $contest['title']; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Earnings (Annual) Card Example -->
	<div class="col-xl-6 col-md-6">
		<div class="card border-left-success shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Peserta</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?= count($list); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php
		if (!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message'); ?>
			</div>
		<?php
		}
		?>
		<?php
		if (!empty(session()->getFlashdata('message_danger'))) { ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata('message_danger'); ?>
			</div>
		<?php
		}
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama</th>
						<th>Telepon</th>
						<th>Email</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($list as $data) {
					?>
						<a href="#1">
							<tr>
								<td><?= $no++; ?></td>
								<td><?= $data['user_name']; ?></td>
								<td><?= $data['user_phone']; ?></td>
								<td><?= $data['user_email']; ?></td>
								<td>
									<a href="#" id="<?= $data['id'] ?>" data-trading="<?= $data['trading_contest_id'] ?>" data-user="<?= $data['user_id'] ?>" class="btn btn-danger" data-toggle="modal" data-target="#refuseModal">Tolak Peserta</a>
									<a href="<?= base_url('administrator/member/' . $data['user_username']); ?>" class="btn btn-info" target="_blank">Detail</a>
								</td>
							</tr>
						</a>
					<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-12">
				<a href="<?= base_url('administrator/contest') ?>" class="btn btn-lg btn-danger float-right mt-2">Kembali</a>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>
<?= $this->section('custom-modal'); ?>
<div class="modal fade" id="refuseModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Tolak Peserta?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Apakah Anda yakin untuk menolak peserta ini?</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
				<form action="<?= base_url('administrator/contest/refuse_candidate'); ?>" method="post">
					<input type="hidden" name="id">
					<input type="hidden" name="id_trading">
					<input type="hidden" name="id_user">
					<button type="submit" class="btn btn-danger btn-block">Ya</button>
				</form>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('custom-modal'); ?>
<?= $this->section('custom-js'); ?>
<script>
	$('#refuseModal').on('show.bs.modal', function(e) {
		$(this).find(':input[name=id]').val($(e.relatedTarget).attr('id'));
		$(this).find(':input[name=id_trading]').val($(e.relatedTarget).data('trading'));
		$(this).find(':input[name=id_user]').val($(e.relatedTarget).data('user'));
	});
</script>
<?= $this->endSection('custom-js'); ?>