<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page">Kontes Trading</li>
		<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/contest'); ?>">Data Kontes</a></li>
		<li class="breadcrumb-item active" aria-current="page">Ubah Kontes</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Ubah Kontes</h6>
	</div>
	<div class="card-body">
		<form class="form-row form-ajax" data-uri="<?= base_url('administrator/contest/update_contest'); ?>">
			<input type="hidden" name="id" value="<?= $data['id']; ?>">
			<input type="hidden" name="old_title" value="<?= $data['title']; ?>">
			<input type="hidden" name="old_banner" value="<?= $data['banner']; ?>">
			<div class="col-md-12 mb-3">
				<label for="content_title">Judul</label>
				<?= getInput('title', 'text', $validation, $data['title'], 'disabled'); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="banner">Banner</label>
				<img src="<?= base_url('component/image/uploads_photo/'.$data['banner']); ?>" class="w-100 img-thumbnail img-preview">
			</div>
			<div class="col-md-6 mb-3">
				<label for="desc">Deskripsi Kontes</label>
				<?= getTextarea('desc', $validation, $data['desc'], 'disabled'); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Mulai Kontes</label>
				<?php $min_sd = date('Y-m-d', strtotime(date('Y-m-d'). '+1 day')) ?>
				<?= getInput('start_date', 'date', $validation, $data['start_date'], "min='$min_sd' disabled"); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="coin_capital">Modal Awal</label>
				<?= getInput('coin_capital', 'number', $validation, $data['coin_capital'], 'disabled'); ?>
			</div>
			<div class="col-md-12 mb-3">
				<a href="<?= base_url('administrator/contest'); ?>" class="btn btn-danger">Kembali</a>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>