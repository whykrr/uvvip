<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/offer'); ?>">Penawaran</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Penawaran</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><?= $data['title']; ?></h6>
	</div>
	<div class="card-body">
		<div class="row position-relative">
			<div class="col-md-12 pt-3">
				<?php
				$date = date('Y-m-d', strtotime($data['created_at']));
				$time = date('H:i:s', strtotime($data['created_at']));
				?>
				<b>Tanggal Terbit : </b><?= tgl_indo($date); ?> <?= $time; ?>
			</div>
			<div class="col-md-12 p-3">
				<img src="<?= base_url('component/image/offers/' . $data['banner_img']); ?>" class="w-50 mx-auto d-block">
			</div>
			<div class="col-md-12 p-3">
				<p><?= $data['content_wysiwig']; ?></p>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>