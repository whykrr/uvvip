<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>UVVIP | Administrator</title>
	<link rel="icon" href="<?= base_url('component/image/asset/app-logo.png'); ?>">

	<!-- Custom fonts for this template-->
	<link href="<?= base_url('component/template/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('component/template/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
	<link href="<?= base_url('component/template/vendor/datatables/dataTables.bootstrap4.min.css'); ?>" rel="stylesheet">

	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('component/internal/css/additional.css') ?>">

	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">

</head>

<body id="page-top">
	<div id="wrapper">
		<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

			<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('administrator/dashboard'); ?>">
				<!-- <div class="sidebar-brand-icon">
					<i class="fas fa-laugh-wink"></i>
				</div> -->
				<div class="sidebar-brand-text mx-3">ADMINISTRATOR</div>
			</a>

			<hr class="sidebar-divider my-0">

			<li class="nav-item <?= ($page == 'Dashboard') ? 'active' : ''; ?>">
				<a class="nav-link" href="<?= base_url('administrator/dashboard'); ?>">
					<i class="fas fa-fw fa-tachometer-alt"></i>
					<span>Dashboard</span>
				</a>
			</li>

			<li class="nav-item <?= ($page == 'Member') ? 'active' : ''; ?>">
				<a class="nav-link" href="<?= base_url('administrator/member'); ?>">
					<i class="fas fa-fw fa-users"></i>
					<span>Member</span>
				</a>
			</li>

			<li class="nav-item <?= ($page == 'Kelas') ? 'active' : ''; ?>">
				<a class="nav-link collapse-header" href="#" data-toggle="collapse" data-target="#menu-kelas" aria-expanded="true" aria-controls="menu-kelas">
					<i class="fas fa-fw fa-file-invoice-dollar"></i>
					<span>Kelas</span>
				</a>
				<div id="menu-kelas" class="collapse <?= ($page == 'Kelas') ? 'show' : ''; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">
						<h6 class="collapse-header">Kelas</h6>
						<a class="collapse-item <?= ($page == 'Kelas') ? ($sub_page == 'schedule') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/classroom/schedule'); ?>">Jadwal Kelas</a>
						<a class="collapse-item <?= ($page == 'Kelas') ? ($sub_page == 'room') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/classroom'); ?>">Ruang Kelas</a>
					</div>
				</div>
			</li>

			<li class="nav-item <?= ($page == 'Pendaftaran') ? 'active' : ''; ?>">
				<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu-transaction" aria-expanded="true" aria-controls="menu-transaction">
					<i class="fas fa-fw fa-file-invoice-dollar"></i>
					<span>Pendaftaran</span>
				</a>
				<div id="menu-transaction" class="collapse <?= ($page == 'Pendaftaran') ? 'show' : ''; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">
						<h6 class="collapse-header">Pendaftaran</h6>
						<a class="collapse-item <?= ($page == 'Pendaftaran') ? ($sub_page == 'data') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/transaction'); ?>">Data</a>
						<a class="collapse-item <?= ($page == 'Pendaftaran') ? ($sub_page == 'list_payment') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/list_payment'); ?>">Konfirmasi Pembayaran</a>
						<a class="collapse-item <?= ($page == 'Pendaftaran') ? ($sub_page == 'history') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/history_transaction'); ?>">History</a>
					</div>
				</div>
			</li>

			<li class="nav-item <?= ($page == 'Kontes Trading') ? 'active' : ''; ?>">
				<a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#menu-contest" aria-expanded="true" aria-controls="menu-contest">
					<i class="fas fa-fw fa-trophy"></i>
					<span>Kontes Trading</span>
				</a>
				<div id="menu-contest" class="collapse <?= ($page == 'Kontes Trading') ? 'show' : ''; ?>" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
					<div class="bg-white py-2 collapse-inner rounded">
						<h6 class="collapse-header">Kontes Trading</h6>
						<a class="collapse-item <?= ($page == 'Kontes Trading') ? ($sub_page == 'data') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/contest'); ?>">Data Kontes</a>
						<a class="collapse-item <?= ($page == 'Kontes Trading') ? ($sub_page == 'ongoing_contest') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/contest/ongoing'); ?>">Kontes Berlangsung</a>
						<a class="collapse-item <?= ($page == 'Kontes Trading') ? ($sub_page == 'history') ? 'active' : '' : ''; ?>" href="<?= base_url('administrator/contest/history'); ?>">Riwayat Kontes</a>
					</div>
				</div>
			</li>

			<hr class="sidebar-divider">
			<div class="sidebar-heading">
				Informasi dan Penawaran
			</div>

			<li class="nav-item <?= ($page == 'Pengumuman') ? 'active' : ''; ?>">
				<a class="nav-link" href="<?= base_url('administrator/announcement'); ?>">
					<i class="fas fa-fw fa-bullhorn"></i>
					<span>Pengumuman</span>
				</a>
			</li>

			<li class="nav-item <?= ($page == 'Informasi') ? 'active' : ''; ?>">
				<a class="nav-link" href="<?= base_url('administrator/information'); ?>">
					<i class="fas fa-fw fa-newspaper"></i>
					<span>Informasi</span>
				</a>
			</li>

			<li class="nav-item <?= ($page == 'Penawaran') ? 'active' : ''; ?>">
				<a class="nav-link" href="<?= base_url('administrator/offer'); ?>">
					<i class="fas fa-fw fa-gift"></i>
					<span>Penawaran</span>
				</a>
			</li>

			<hr class="sidebar-divider">
			<div class="sidebar-heading">
				Interaksi Member
			</div>
			<li class="nav-item <?= ($page == 'Interaksi') ? 'active' : ''; ?>">
				<a class="nav-link" href="<?= base_url('administrator/interaction'); ?>">
					<i class="fas fa-fw fa-comments"></i>
					<span>Interaksi</span>
				</a>
			</li>

		</ul>
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
					<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
						<i class="fa fa-bars"></i>
					</button>
					<ul class="navbar-nav">
						<h1 class="h3 text-gray-800"><?= $page; ?></h1>
					</ul>
					<ul class="navbar-nav ml-auto">
						<?php $user = session()->get('administrator'); ?>
						<li class="nav-item dropdown no-arrow">
							<a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= $user['name']; ?></span>
								<img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
							</a>
							<div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
								<a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
									<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
									Logout
								</a>
							</div>
						</li>

					</ul>

				</nav>
				<div class="container-fluid">
					<?= $this->renderSection('content'); ?>
				</div>

			</div>
			<footer class="sticky-footer bg-white">
				<div class="container my-auto">
					<div class="copyright text-center my-auto">
						<span>Copyright &copy; Nine CLoud 2020</span>
					</div>
				</div>
			</footer>
			<!-- End of Footer -->

		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top">
		<i class="fas fa-angle-up"></i>
	</a>
	<div id="component-cloning" style="display: none;">
		<?= $this->renderSection('component-cloning'); ?>
	</div>
	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="<?= base_url('administrator/logout'); ?>">Logout</a>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Apakah anda yakin untuk menghapus data ini?</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<form method="post">
						<input type="hidden" name="_method" value="DELETE">
						<button type="submit" class="btn btn-danger btn-block">Delete</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="mediaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel"></h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">
					<img src="" class="mt-2 w-100 img-thumbnail img-preview">
					<embed src="" type="" width="100%" height="500px" allowfullscreen></embed>
					<video controls width="100%" height="500px"></video>
				</div>
			</div>
		</div>
	</div>
	<?= $this->renderSection('custom-modal'); ?>

	<!-- Bootstrap core JavaScript-->
	<script src="<?= base_url('component/template/vendor/jquery/jquery.min.js'); ?>"></script>
	<script src="<?= base_url('component/template/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

	<!-- Core plugin JavaScript-->
	<script src="<?= base_url('component/template/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

	<!-- Custom scripts for all pages-->
	<script src="<?= base_url('component/template/js/sb-admin-2.min.js'); ?>"></script>

	<!-- Page level plugins -->
	<script src="<?= base_url('component/template/vendor/chart.js/Chart.min.js'); ?>"></script>
	<script src="<?= base_url('component/template/vendor/datatables/jquery.dataTables.min.js'); ?>"></script>
	<script src="<?= base_url('component/template/vendor/datatables/dataTables.bootstrap4.min.js'); ?>"></script>

	<!-- Page level custom scripts -->
	<script src="<?= base_url('component/template/js/demo/chart-area-demo.js'); ?>"></script>
	<script src="<?= base_url('component/template/js/demo/chart-pie-demo.js'); ?>"></script>
	<script src="<?= base_url('component/template/js/demo/datatables-demo.js'); ?>"></script>

	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
	<script type="text/javascript">
		setTimeout(function() {
			$('.alert').fadeOut();
		}, 3500);

		function clone_form(obj, target, place) {
			cloning = $(target).clone();
			name = $(target).data('input');

			clone_no = parseInt($(obj).next('input').val()) + 1;
			$(obj).next('input').val(clone_no);

			$(cloning).show();
			$(cloning).find('label').attr('for', name + clone_no);
			$(cloning).find(':input').attr('id', name + clone_no);
			$(cloning).find(':input').attr('disabled', false);
			$(cloning).removeAttr('id');
			$(place).append(cloning);
		}

		function clone_del(obj, count_target) {
			clone_no = parseInt($(count_target).val()) - 1;
			$(count_target).val(clone_no);
			$(obj).parent().parent().remove();
		}

		function del_file(obj, count_target) {
			$.ajax({
				type: 'post',
				url: $(obj).data('uri'),
				data: {
					id: $(obj).attr('id'),
					file: $(obj).data('file')
				},
				dataType: 'json',
				beforeSend: function() {
					$(obj).attr('disabled', true);
				},
				success: function(data) {
					if (data.res == 'success') {
						clone_no = parseInt($(count_target).val()) - 1;
						$(count_target).val(clone_no);
						$(obj).parent().parent().remove();
					}
				},
				error: function() {
					$(obj).attr('disabled', false);
				}
			});
		}

		function get_dataTable(target) {
			action = $(target).data('action');
			key_table = 'id|' + $(target).data('key');

			$.getJSON($(target).data('source'), function(data) {
				counter = 1;

				t = $(target).DataTable();
				t.clear().draw();
				$.each(data, function(key, item) {
					action_clone = $(action).clone();
					stkey = key_table.split('|');
					val_arr = [];
					val_dtRow = [counter];

					$.each(item, function(key_sub, item_sub) {
						if (stkey.includes(key_sub)) {
							val_arr.push(item_sub);
						}
						if (stkey.includes(key_sub) && key_sub != 'id') {
							val_dtRow.push(item_sub);
						}
					})
					val_arr = val_arr.join('|');
					action_clone.find('.act-edit').attr("onclick", "edit_temp(this, '" + val_arr + "','" + key_table + "')");
					action_clone.find('.delete-ajax-temp input[name=id]').val(item['id']);

					val_dtRow.push($(action_clone).html());

					t.row.add(val_dtRow).draw();
					counter++;
				})
			});
		}

		function edit_temp(obj, data, key) {
			data = data.split('|');
			key = key.split('|');
			targetForm = $(obj).data('targetform');
			$(targetForm).prepend('<input type="hidden" name="id" class="temp">');
			$.each(key, function(key, item) {
				$(targetForm).find(':input[name=' + item + ']').val(data[key]);
			})
			$('html, body').animate({
				scrollTop: $(targetForm).offset().top
			}, 'slow');
			$(targetForm).find('button[type=submit]').after('<button type="button" class="btn btn-danger float-right temp" onclick="cancel_edit_temp(`' + targetForm + '`)">Batal</button>');
		}

		function cancel_edit_temp(targetForm) {
			$(targetForm).find('.temp').remove();
			$(targetForm).find(':input:not(.invincible)').val('');
		}

		$(document).ready(function() {
			get_dataTable('.dataTable');
			$('#content_wysiwig').summernote({
				toolbar: [
					['style', ['bold', 'italic', 'underline', 'clear']],
					['font', ['strikethrough', 'superscript', 'subscript']],
					['fontsize', ['fontsize']],
					['color', ['color']],
					['para', ['ul', 'ol', 'paragraph']],
					['height', ['height']]
				]
			});
			$('input[type=file]').change(function() {
				if (this.files && this.files[0]) {
					var reader = new FileReader();
					input = this;

					reader.onload = function(e) {
						console.log(e);
						$(input).parent().find('.img-preview').attr('src', e.target.result);
						$(input).parent().find('video').html('<source src="' + e.target.result + '"></source>');
						$(input).parent().find('audio').html('<source src="' + e.target.result + '"></source>');
					}

					reader.readAsDataURL(this.files[0]);
				}
			})
			$('#mediaModal').on('show.bs.modal', function(e) {
				$(this).find('img').hide();
				$(this).find('embed').hide();
				$(this).find('video').hide();
				$(this).find('.modal-title').text($(e.relatedTarget).data('title'));
				if ($(e.relatedTarget).data('media') == 'img') {
					$(this).find('img').attr('src', $(e.relatedTarget).data('src'));
					$(this).find('img').show();
				} else if ($(e.relatedTarget).data('media') == 'embed') {
					$(this).find('embed').attr('src', $(e.relatedTarget).data('src'));
					$(this).find('embed').attr('type', $(e.relatedTarget).data('type'));
					$(this).find('embed').show();
				} else if ($(e.relatedTarget).data('media') == 'video') {
					$(this).find('video').html('<source src="' + $(e.relatedTarget).data('src') + '">');
					$(this).find('video').show();
				}
			});
			$('#deleteModal').on('show.bs.modal', function(e) {
				$(this).find('form').attr('action', $(e.relatedTarget).attr('href'));
			});
		});

		$('.form-ajax').submit(function() {
			form = $(this);
			$.ajax({
				type: 'post',
				url: form.data('uri'),
				data: new FormData($(this)[0]),
				dataType: 'json',
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				beforeSend: function() {
					form.find('button[type=submit]').attr('hidden', true);
					form.find('#button-load').attr('hidden', false);
				},
				success: function(data) {
					if (data.res == 'success') {
						window.location = data.redirect;
					} else {
						form.find('button[type=submit]').attr('hidden', false);
						form.find('#button-load').attr('hidden', true);
						form.find(':input').removeClass('is-invalid');
						form.find('.clone-feedback').remove();
						$.each(data.validation, function(key, data) {
							form.find(':input[name=' + key + ']').addClass('is-invalid');
							form.find(':input[name=' + key + ']').next('.invalid-feedback').text(data);
							form.find('#place_' + key).prepend('<div class="alert alert-danger clone-feedback">' + data + '</div>');
						});
					}
				},
				error: function() {
					form.find('button[type=submit]').attr('hidden', false);
					form.find('#button-load').attr('hidden', true);
				}
			});
			return false;
		});
		$('.form-ajax-temp').submit(function() {
			form = $(this);
			targetCB = $(this).data('callback');
			$.ajax({
				type: 'post',
				url: form.data('uri'),
				data: form.serialize(),
				dataType: 'json',
				beforeSend: function() {
					form.find('button[type=submit]').attr('disabled', true);
				},
				success: function(data) {

					if (data.res == 'success') {
						get_dataTable(targetCB);
						form.find('.temp').remove();
						form.find(':input:not(.invincible)').val('');
					}
					if (data.res == 'success_callback') {
						callback(data);
						form.find('.temp').remove();
						form.find(':input:not(.invincible)').val('');
					}
					form.find('button[type=submit]').attr('disabled', false);
				},
				error: function() {
					form.find('button[type=submit]').attr('disabled', false);
				}
			});
			return false;
		});
		$(document).on('submit', '.delete-ajax-temp', function() {
			form = $(this);
			targetCB = $(this).data('callback');
			$.ajax({
				type: 'post',
				url: form.data('uri'),
				data: form.serialize(),
				dataType: 'json',
				beforeSend: function() {
					form.find('button[type=submit]').attr('disabled', true);
				},
				success: function(data) {
					if (data.res == 'success') {
						get_dataTable(targetCB);
					}
				},
				error: function() {
					$(this).find('button[type=submit]').attr('disabled', false);
				}
			});
			return false;
		});
	</script>
	<?= $this->renderSection('custom-js'); ?>
</body>

</html>