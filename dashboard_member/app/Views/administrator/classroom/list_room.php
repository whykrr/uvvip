<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page">Ruang Kelas</li>
	</ol>
</nav>
<div class="row">
	<div class="col-lg-12">
		<a href="<?= base_url('administrator/classroom/add_class'); ?>" class="btn btn-success btn-icon-split float-right ">
			<span class="icon text-white-50">
				<i class="fas fa-plus"></i>
			</span>
			<span class="text">Tambah Kelas</span>
		</a>
	</div>
</div>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message');?>
			</div>
			<?php
		} 
		?>
		<?php 
		if(!empty(session()->getFlashdata('message_danger'))) { ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata('message_danger');?>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>No</th>
						<th>Judul</th>
						<th>Paket</th>
						<th>Periode</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($list as $data) {
						?>
						<a href="#1">
							<tr>
								<td><?= $no++; ?></td>
								<td><?= $data['title']; ?></td>
								<td><?= $data['package_name']; ?></td>
								<td><?= tgl_indo2($data['start_date']); ?> - <?= tgl_indo2($data['end_date']); ?></td>
								<td>
									<?php 
									if ($data['status'] == 'Scheduled') {
										?>
										<h5><span class="badge badge-info p-2">Scheduled</span></h5>
										<?php
									}else if ($data['status'] == 'Open'){
										?>
										<h5><span class="badge badge-success p-2">Open</span></h5>
										<?php
									}else {
										?>
										<h5><span class="badge badge-danger p-2">Closed</span></h5>
										<?php
									} ?>
								</td>
								<td>
									<a href="<?= base_url('administrator/classroom/students/'. $data['id']); ?>" class="btn btn-primary">Siswa</a>
									<a href="<?= base_url('administrator/classroom/message/'. $data['id']); ?>" class="btn btn-info" data-class="<?= $data['id']; ?>" data-head="Pesan <?= $data['title']; ?>" data-toggle="modal" data-target="#messageModal">Pesan 
										<?php
										$tno = 1;
										foreach ($chat as $item) {
											if($item['class_id']==$data['id']){
												?>
												<span class="badge badge-light"><?= $item['unread']; ?></span>
												<?php
											}
										}
										?>
									</a>
									<a href="#" class="btn btn-warning" data-classid="<?= $data['id']; ?>" data-zoom="<?= $data['zoom_link']; ?>" data-toggle="modal" data-target="#zoomModal">Ubah Link ZOOM</a>
									<form method="post" action="<?= base_url('administrator/classroom/close_class'); ?>" class="d-inline">
										<input type="hidden" name="class_id" value="<?= $data['id']; ?>">
										<button type="submit" class="btn btn-danger">Tutup Kelas</button>
									</form>
								</td>
							</tr>
						</a>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>
<?= $this->section('custom-modal'); ?>
<div class="modal fade" id="zoomModal" tabindex="-1" role="dialog" aria-labelledby="linkZoomModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="linkZoomModal">Ubah ZOOM</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<form method="post" action="<?= base_url('administrator/classroom/change_zoom'); ?>">
					<input type="hidden" name="id">
					<label for="link">Link ZOOM</label>
					<input type="url" name="zoom_link" class="form-control">
					<button class="btn btn-primary float-right mt-2">Simpan</button>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageHeadModal" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="messageHeadModal"></h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<form class="row w-100 form-ajax-temp" data-uri="<?= base_url('administrator/classroom/save_msg'); ?>">
					<input type="hidden" name="class_id" id="class_id" class="invincible" value="">
					<div class="col-sm-10 pl-0">
						<textarea class="form-control" name="message" id="message" required=""></textarea>
					</div>
					<div class="col-sm-2 px-0">
						<button type="submit" class="btn btn-primary btn-block btn-kirim">Kirim</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('custom-modal'); ?>
<?= $this->section('custom-js'); ?>
<script type="text/javascript">
	$('#zoomModal').on('show.bs.modal', function(e) {
		$(this).find(':input[name=id]').val($(e.relatedTarget).data('classid'));
		$(this).find(':input[name=zoom_link]').val($(e.relatedTarget).data('zoom'));
	});
	$('#messageModal').on('show.bs.modal', function(e) {
		$(e.relatedTarget).find('span').hide();
		$(this).find(':input[name=class_id]').val($(e.relatedTarget).data('class'));
		$(this).find('#messageHeadModal').text($(e.relatedTarget).data('head'));
		get_message($(e.relatedTarget).attr('href'));
	});
	function get_message(uri){
		$.getJSON( uri, function( data ) {
			$('#messageModal').find('.modal-body').html(data.chat);
			if(data.unread>0){
				if($('#body-msg')[0].scrollHeight > $('#unread-msg').offset().top-150){
					$('#body-msg').animate({ scrollTop: $('#unread-msg').offset().top-150 }, 'slow');
				}else{
					$('#body-msg').scrollTop($('#body-msg')[0].scrollHeight);
				}
			}else{
				$('#body-msg').scrollTop($('#body-msg')[0].scrollHeight);
			}
		})
	}
	function callback(data){
		get_message(data.uri);
	}
</script>
<?= $this->endSection('custom-js'); ?>