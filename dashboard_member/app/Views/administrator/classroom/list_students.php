<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page"><a href="<?= base_url('administrator/classroom'); ?>">Ruang Kelas</a></li>
		<li class="breadcrumb-item" aria-current="page">Siswa</li>
	</ol>
</nav>
<div class="row">

	<!-- Earnings (Monthly) Card Example -->
	<div class="col-xl-6 col-md-6">
		<div class="card border-left-primary shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Judul Kelas</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?= $class['title']; ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Earnings (Annual) Card Example -->
	<div class="col-xl-6 col-md-6">
		<div class="card border-left-success shadow h-100 py-2">
			<div class="card-body">
				<div class="row no-gutters align-items-center">
					<div class="col mr-2">
						<div class="text-xs font-weight-bold text-success text-uppercase mb-1">Jumlah Siswa</div>
						<div class="h5 mb-0 font-weight-bold text-gray-800"><?= count($list); ?></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message');?>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>No</th>
						<th>Nama Siswa</th>
						<th>Telepon</th>
						<th>Email</th>
						<th>Score Quiz</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($list as $data) {
						?>
						<tr>
							<td><?= $no++; ?></td>
							<td><?= $data['name']; ?></td>
							<td><?= $data['phone']; ?></td>
							<td><?= $data['email']; ?></td>
							<td><?= $data['score']; ?></td>
							<td>
								<?php
								if(!empty($data['attachment'])){
									?>
									<button type="button" 
									data-title="Video Trading <?= $data['name']; ?>" 
									data-user="<?= $data['user_id']; ?>" 
									data-src="<?= base_url('component/videos/video_trading/'.$data['attachment']); ?>"
									data-package="<?= $class['package_id']; ?>"
									data-analitic="<?= ($data['id_analitics']) ? 'done' : '' ?>"
									data-character-score="<?= $data['character_score']; ?>"
									data-intuition-score="<?= $data['intuition_score']; ?>"
									data-character-review="<?= $data['character_review']; ?>"
									data-intiution-review="<?= $data['intiution_review']; ?>"
									data-toggle="modal" 
									data-target="#mediatradeModal"
									class="btn btn-info" 
									>Lihat Video Trading</button>
									<?php	
								}
								?>
								<a href="<?= base_url('administrator/member/'.$data['username']); ?>" target="_blank" class="btn btn-success">Detail Siswa</a>
							</td>
						</tr>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
		<a href="<?= base_url('administrator/classroom'); ?>" class="btn btn-danger btn-lg float-right mt-2">Kembali</a>
	</div>
</div>
<?= $this->endSection('content'); ?>

<?= $this->section('custom-modal'); ?>
<div class="modal fade" id="mediatradeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel"></h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">
				<video controls width="100%" height="300px" class="mb-1" id="video"></video>
				<form class="row form-ajax" data-uri="<?= base_url('administrator/classroom/save_analitics'); ?>">
					<input type="hidden" name="class_id" value="<?= $class['id']; ?>">
					<input type="hidden" name="user_id">
					<div class="col-sm-6">
						<label for="character_score">Skor Karakter</label>
						<input type="number" name="character_score" max="100" class="form-control">
					</div>
					<div class="col-sm-6">
						<label for="intuition_score">Skor Intuisi</label>
						<input type="number" name="intuition_score" max="100" class="form-control">
					</div>
					<div class="col-sm-6">
						<label for="character_review">Review Karakter</label>
						<textarea name="character_review" class="form-control"></textarea>
					</div>
					<div class="col-sm-6">
						<label for="intiution_review">Review Intuisi</label>
						<textarea name="intiution_review" class="form-control"></textarea>
					</div>
					<div class="col-sm-12">
						<button type="submit" class="btn btn-primary mt-2 float-right">Simpan</button>
					</div>
				</form>	
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('custom-modal'); ?>

<?= $this->section('custom-js'); ?>
<script type="text/javascript">
	$('#mediatradeModal').on('show.bs.modal', function(e) {
		$(this).find('.modal-title').text($(e.relatedTarget).data('title'));
		$(this).find('video').html('<source src="'+$(e.relatedTarget).data('src')+'">');
		if($(e.relatedTarget).data('package')=='3'){
			$(this).find('form').show();
			$(this).find(':input').attr('readonly', false);
			$(this).find('button[type=submit]').show();
			$(this).find(':input[name=user_id]').val($(e.relatedTarget).data('user'));
			$(this).find(':input[name=character_score]').val($(e.relatedTarget).data('character-score'));
			$(this).find(':input[name=intuition_score]').val($(e.relatedTarget).data('intuition-score'));
			$(this).find(':input[name=character_review]').val($(e.relatedTarget).data('character-review'));
			$(this).find(':input[name=intiution_review]').val($(e.relatedTarget).data('intiution-review'));
			if($(e.relatedTarget).data('analitic')=='done'){
				$(this).find(':input').attr('readonly', true);
				$(this).find('button[type=submit]').hide();
			}
		}else {
			$(this).find('form').hide();
		}
	});
	$('#mediatradeModal').on('hide.bs.modal', function(e) {
		$(this).find('video').get(0).pause();
	});
</script>
<?= $this->endSection('custom-js'); ?>