<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/classroom/schedule'); ?>">Jadwal Kelas</a></li>
		<li class="breadcrumb-item active" aria-current="page">Form Tambah Kelas</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Tambah Kelas</h6>
	</div>
	<div class="card-body">
		<form class="form-row form-ajax" data-uri="<?= base_url('administrator/classroom/insert_class'); ?>">
			<div class="col-md-12 mb-3">
				<label for="content_title">Nama Kelas</label>
				<?= getInput('title', 'text', $validation); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="banner">Banner</label>
				<?= getInput('banner', 'file', $validation); ?>
				<img src="" class="mt-2 w-50 img-thumbnail img-preview">
			</div>
			<div class="col-md-6 mb-3">
				<label for="desc">Deskripsi Kelas</label>
				<?= getTextarea('desc', $validation); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="package_id">Jenis Kelas</label>
				<?= getSelect('package_id', $package, $validation); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="capacity">Kapasitas</label>
				<?= getInput('capacity', 'number', $validation); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Mulai Kelas</label>
				<?= getInput('start_date', 'date', $validation); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Akhir Kelas</label>
				<?= getInput('end_date', 'date', $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="market_link">Link Market</label>
				<?= getInput('market_link', 'url', $validation); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="banner">Video Ringkasan Materi</label>
				<?= getInput('review_materi', 'file', $validation); ?>
				<video controls class="mt-2 w-50 img-thumbnail">
				</video>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Jadwal Zoom</h6>
						<button type="button" class="btn btn-success btn-sm btn-icon-split float-right" onclick="clone_form(this, '#zoom_clone', '#place_zoom')">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text">Tambah Jadwal</span>
						</button>
						<input type="hidden" name="zoom_count" id="zoom_count" value="0">
					</div>
					<div class="card-body" id="place_zoom">
						<div class="form-group row pl-2 mb-1" id="zoom_clone" data-input='zoom' style="display: none;">
							<div class="col-sm-10 pl-0">
								<?= getInput('zoom[]', 'datetime-local', $validation, '', 'disabled'); ?>
							</div>
							<div class="col-sm-2 pl-0">
								<button type="button" class="btn btn-danger float-right btn-block" onclick="clone_del(this, '#zoom_count')">
									<span class="icon text-white">
										<i class="fas fa-trash"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Treat</h6>
						<button type="button" class="btn btn-success btn-sm btn-icon-split float-right" onclick="clone_form(this, '#treat_clone', '#place_treat')">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text">Tambah Treat</span>
						</button>
						<input type="hidden" name="treat_count" id="treat_count" value="0">
					</div>
					<div class="card-body" id="place_treat">
						<div class="form-group row pl-2 mb-1" id="treat_clone" data-input='treat' style="display: none;">
							<label for="" class="col-sm-2 col-form-label pl-0">Treat</label>
							<div class="col-sm-9">
								<?= getInput('treat[]', 'file', $validation, '', 'disabled'); ?>
							</div>
							<div class="col-sm-1">
								<button type="button" class="btn btn-danger btn-icon-split float-right" onclick="clone_del(this, '#treat_count')">
									<span class="icon text-white">
										<i class="fas fa-trash"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Video</h6>
						<button type="button" class="btn btn-success btn-sm btn-icon-split float-right" onclick="clone_form(this, '#video_clone', '#place_video')">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text">Tambah Video</span>
						</button>
						<input type="hidden" name="video_count" id="video_count" value="0">
					</div>
					<div class="card-body" id="place_video">
						<div class="form-group row pl-2 mb-1" id="video_clone" data-input='video' style="display: none;">
							<label for="" class="col-sm-2 col-form-label pl-0">Video</label>
							<div class="col-sm-9">
								<?= getInput('video[]', 'file', $validation, '', 'disabled'); ?>
							</div>
							<div class="col-sm-1">
								<button type="button" class="btn btn-danger btn-icon-split float-right" onclick="clone_del(this, '#video_count')">
									<span class="icon text-white">
										<i class="fas fa-trash"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary float-right">Simpan</button>
				<button disabled class="btn btn-primary float-right" id="button-load" hidden>
					<i class="fa fa-spinner fa-spin"></i>Loading
				</button>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>