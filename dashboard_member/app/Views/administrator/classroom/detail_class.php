<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/classroom/schedule'); ?>">Jadwal Kelas</a></li>
		<li class="breadcrumb-item active" aria-current="page">Detail Kelas</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Detail Kelas</h6>
	</div>
	<div class="card-body">
		<div class="form-row">
			
			<input type="hidden" name="id" value="<?= $class['id']; ?>">
			<input type="hidden" name="old_title" value="<?= $class['title']; ?>">
			<div class="col-md-12 mb-3">
				<label for="content_title">Nama Kelas</label>
				<?= getInput('title', 'text', $validation, $class['title'], 'disabled'); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="banner" class="d-block">Banner</label>
				<img src="<?= base_url('component/image/uploads_photo/'.$class['banner']); ?>" class="w-50 img-thumbnail img-preview">
			</div>
			<div class="col-md-6 mb-3">
				<label for="desc">Deskripsi Kelas</label>
				<?= getTextarea('desc', $validation, $class['desc'], 'disabled'); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="package_id">Jenis Kelas</label>
				<?= getSelect('package_id', $package, $validation, $class['package_id'], 'disabled'); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="capacity">Kapasitas</label>
				<?= getInput('capacity', 'number', $validation, $class['capacity'], 'disabled'); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Mulai Kelas</label>
				<?= getInput('start_date', 'date', $validation, $class['start_date'], 'disabled'); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Akhir Kelas</label>
				<?= getInput('end_date', 'date', $validation, $class['end_date'], 'disabled'); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="market_link">Link Market</label>
				<?= getInput('market_link', 'text', $validation, $class['market_link'], 'disabled'); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="banner" class="d-block">Video Ringkasan Materi</label>
				<video controls class="w-25 img-thumbnail">
					<source src="<?= base_url('component/videos/summary/' . $sumclass['attachment']); ?>"></source>
				</video>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Jadwal Zoom</h6>
					</div>
					<div class="card-body" id="place_zoom">
						<?php
						$tno = 1;
						foreach ($zclass as $zoom) {
							$date = date('Y-m-d', strtotime($zoom['schedule']));
							$time = date('H:i', strtotime($zoom['schedule']));
							?>
							<div class="row pl-2 mb-1">
								<label for="" class="col-sm-10 col-form-label pl-0"><?= tgl_indo2($date).' Pukul '.$time; ?></label>
								<div class="col-sm-2">
									
								</div>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Treat</h6>
					</div>
					<div class="card-body" id="place_treat">
						<?php
						$tno = 1;
						foreach ($tclass as $treat) {
							?>
							<div class="row pl-2 mb-1">
								<label for="" class="col-sm-8 col-form-label pl-0">Treat <?= $tno; ?></label>
								<div class="col-sm-4">
									<button type="button" data-title="Treat <?= $tno; ?>" data-media="embed" data-src="<?= base_url('component/pdf/treats/'.$treat['attachment']); ?>" data-type="application/pdf" class="btn btn-info btn-icon-split float-right" data-toggle="modal" data-target="#mediaModal">
										<span class="icon text-white">
											<i class="fas fa-eye"></i>
										</span>
									</button>
								</div>
							</div>
							<?php
							$tno++;
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Video</h6>
					</div>
					<div class="card-body" id="place_video">
						<?php
						$vno = 1;
						foreach ($vclass as $video) {
							?>
							<div class="row pl-2 mb-1">
								<label for="" class="col-sm-8 col-form-label pl-0">Video <?= $vno; ?></label>
								<div class="col-sm-4">
									<button type="button" data-title="Video <?= $vno; ?>" data-media="video" data-src="<?= base_url('component/videos/class_video/'.$video['attachment']); ?>" class="btn btn-info btn-icon-split float-right" data-toggle="modal" data-target="#mediaModal">
										<span class="icon text-white">
											<i class="fas fa-eye"></i>
										</span>
									</button>
								</div>
							</div>
							<?php
							$vno++;
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-12 mb-3">
				<a href="<?= base_url('administrator/classroom/schedule'); ?>" class="btn btn-danger float-right mr-2">Kembali</a>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>