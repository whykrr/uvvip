<div class="card bg-light"id="body-msg" style="height: 400px; overflow-y: scroll;">
	<div class="card-body" style="padding-bottom: 20px;">
		<?php foreach ($read_chat as $row) {
			?>
			<div class="d-flex <?= ($row['user_id'] == 'A'.$users['id']) ? 'flex-row-reverse' : 'flex-row' ; ?> mb-2">
				<div class="card" style="background-color: #048894;color: #FFF;">
					<div class="card-body" style="padding: 10px;">
						<b><p class="mb-0"><?= $row['name']; ?></p></b>
						<p style="margin: 0;"><?= $row['message']; ?> <br> <span style="color: #FFF;font-size: 12px;"><?= $row['created_at']; ?></span></p>
					</div>
				</div>
			</div>
			<?php
		}
		if($unread_chat){
			?>
			<div class="m-0 p-0" id="unread-msg">
				<hr class="mb-1 ">
				<p class="text-center">Pesan belum terbaca</p>
			</div>
			<?php
		}
		foreach ($unread_chat as $row) {
			?>
			<div class="d-flex <?= ($row['user_id'] == 'M'.$users['id']) ? 'flex-row-reverse' : 'flex-row' ; ?> mb-2">
				<div class="card" style="background-color: #048894;color: #FFF;">
					<div class="card-body" style="padding: 10px;">
						<b><p class="mb-0"><?= $row['name']; ?></p></b>
						<p style="margin: 0;"><?= $row['message']; ?> <br> <span style="color: #FFF;font-size: 12px;"><?= $row['created_at']; ?></span></p>
					</div>
				</div>


			</div>
			<?php
		} ?>
	</div>
</div>