<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item active" aria-current="page">Jadwal Kelas</li>
	</ol>
</nav>
<div class="row">
	<div class="col-lg-12">
		<a href="<?= base_url('administrator/classroom/add_class'); ?>" class="btn btn-success btn-icon-split float-right ">
			<span class="icon text-white-50">
				<i class="fas fa-plus"></i>
			</span>
			<span class="text">Tambah Kelas</span>
		</a>
	</div>
</div>
<div class="card shadow mb-4 mt-3">
	<div class="card-body">
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-success">
				<?php echo session()->getFlashdata('message');?>
			</div>
			<?php
		} 
		?>
		<?php 
		if(!empty(session()->getFlashdata('message_danger'))) { ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata('message_danger');?>
			</div>
			<?php
		} 
		?>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>No</th>
						<th>Judul</th>
						<th>Paket</th>
						<th>Kapasitas</th>
						<th>Periode</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$no = 1;
					foreach ($list as $data) {
						?>
						<a href="#1">
							<tr>
								<td><?= $no++; ?></td>
								<td><?= $data['title']; ?></td>
								<td><?= $data['package_name']; ?></td>
								<td><?= $data['capacity']; ?> Orang</td>
								<td><?= tgl_indo2($data['start_date']); ?> - <?= tgl_indo2($data['end_date']); ?></td>
								<td>
									<?php 
									if ($data['status'] == 'Scheduled') {
										?>
										<h5><span class="badge badge-info p-2">Scheduled</span></h5>
										<?php
									}else if ($data['status'] == 'Open'){
										?>
										<h5><span class="badge badge-success p-2">Open</span></h5>
										<?php
									}else {
										?>
										<h5><span class="badge badge-danger p-2">Closed</span></h5>
										<?php
									} ?>
								</td>
								<td>
									<a href="<?= base_url('administrator/classroom/quiz/'. $data['id']); ?>" class="btn btn-primary">Quiz</a>
									<a href="<?= base_url('administrator/classroom/edit_class/'. $data['id']); ?>" class="btn btn-warning">Edit</a>
									<a href="<?= base_url('administrator/classroom/detail_class/'. $data['id']); ?>" class="btn btn-info">Detail</a>
									<form method="post" action="<?= base_url('administrator/classroom/open_class'); ?>" class="d-inline">
										<input type="hidden" name="class_id" value="<?= $data['id']; ?>">
										<button type="submit" class="btn btn-success">Buka Kelas</button>
									</form>
								</td>
							</tr>
						</a>
						<?php
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>