<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/classroom/schedule'); ?>">Jadwal Kelas</a></li>
		<li class="breadcrumb-item active" aria-current="page">Form Kuis</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Kuis</h6>
	</div>
	<div class="card-body" id="place_video">
		<form class="form-ajax-temp" data-uri="<?= base_url('administrator/classroom/save_temp_quiz'); ?>" data-callback="#table-temp">
			<input type="hidden" name="class_id" class="invincible" value="<?= $class_id; ?>">
			<div class="row pl-2 mb-1">
				<div class="col-md-12 pl-0">
					<label for="question">Pertanyaan</label>
					<?= getTextarea('question', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="option_a" class="col-sm-2 col-form-label pl-0">Pilihan A</label>
				<div class="col-sm-10">
					<?= getTextarea('option_a', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="option_b" class="col-sm-2 col-form-label pl-0">Pilihan B</label>
				<div class="col-sm-10">
					<?= getTextarea('option_b', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="option_c" class="col-sm-2 col-form-label pl-0">Pilihan C</label>
				<div class="col-sm-10">
					<?= getTextarea('option_c', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="option_d" class="col-sm-2 col-form-label pl-0">Pilihan D</label>
				<div class="col-sm-10">
					<?= getTextarea('option_d', $validation); ?>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="option_d" class="col-sm-2 col-form-label pl-0">Kunci Jawaban</label>
				<div class="col-sm-10">
					<select class="form-control" name="key">
						<option value="" selected>-- PILIH --</option>
						<option value="a">A</option>
						<option value="b">B</option>
						<option value="c">C</option>
						<option value="d">D</option>
					</select>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 my-2">
					<button type="submit" class="btn btn-primary ml-2 float-right">Simpan Pertanyaan</button>
				</div>
			</div>
		</form>
		<hr>
		<div class="table-responsive">
			<table class="table table-bordered dataTable" id="table-temp" data-source="<?= base_url('administrator/classroom/data_temp/'.$class_id); ?>" data-action="#action-quiz" data-key="question|option_a|option_b|option_c|option_d|key">
				<thead>
					<tr>
						<td>No</td>
						<td>Pertanyaan</td>
						<td>Pilihan A</td>
						<td>Pilihan B</td>
						<td>Pilihan C</td>
						<td>Pilihan D</td>
						<td>Kunci Jawaban</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="col-md-12 my-2 px-0">
			<a href="<?= base_url('administrator/classroom/schedule'); ?>" class="btn btn-lg btn-danger ml-2 float-right">Kembali</a>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>
<?= $this->section('component-cloning') ?>
<div class='btn-group' id="action-quiz">
	<button type='button' class='btn btn-warning btn-icon-split act-edit' data-targetform=".form-ajax-temp">
		<span class="icon text-white">
			<i class="fas fa-pencil-alt"></i>
		</span>
	</button>
	<form class="delete-ajax-temp d-inline" data-uri="<?= base_url('administrator/classroom/delete_temp_quiz'); ?>" data-callback="#table-temp">
		<input type="hidden" name="id">
		<button type='submit' class='btn btn-danger btn-icon-split'>
			<span class="icon text-white">
				<i class="fas fa-trash"></i>
			</span>
		</button>
	</form>
</div>
<?= $this->endSection('component-cloning'); ?>
