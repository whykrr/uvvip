<?= $this->extend('administrator/layout/administrator') ?>

<?= $this->section('content') ?>
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item" aria-current="page"><a href="<?= base_url('administrator/classroom/schedule'); ?>">Jadwal Kelas</a></li>
		<li class="breadcrumb-item active" aria-current="page">Form Ubah Kelas</li>
	</ol>
</nav>

<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary">Form Ubah Kelas</h6>
	</div>
	<div class="card-body">
		<form class="form-row form-ajax" data-uri="<?= base_url('administrator/classroom/update_class'); ?>">
			<input type="hidden" name="id" value="<?= $class['id']; ?>">
			<input type="hidden" name="old_title" value="<?= $class['title']; ?>">
			<div class="col-md-12 mb-3">
				<label for="content_title">Nama Kelas</label>
				<?= getInput('title', 'text', $validation, $class['title']); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="banner">Banner</label>
				<?= getInput('banner', 'file', $validation); ?>
				<img src="<?= base_url('component/image/uploads_photo/'.$class['banner']); ?>" class="mt-2 w-50 img-thumbnail img-preview">
			</div>
			<div class="col-md-6 mb-3">
				<label for="desc">Deskripsi Kelas</label>
				<?= getTextarea('desc', $validation, $class['desc']); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="package_id">Jenis Kelas</label>
				<?= getSelect('package_id', $package, $validation, $class['package_id']); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="capacity">Kapasitas</label>
				<?= getInput('capacity', 'number', $validation, $class['capacity']); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Mulai Kelas</label>
				<?= getInput('start_date', 'date', $validation, $class['start_date']); ?>
			</div>
			<div class="col-md-6 mb-3">
				<label for="start_date">Tanggal Akhir Kelas</label>
				<?= getInput('end_date', 'date', $validation, $class['end_date']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="market_link">Link Market</label>
				<?= getInput('market_link', 'url', $validation, $class['market_link']); ?>
			</div>
			<div class="col-md-12 mb-3">
				<label for="banner">Video Ringkasan Materi</label>
				<?= getInput('review_materi', 'file', $validation); ?>
				<video controls class="mt-2 w-25 img-thumbnail">
					<source src="<?= base_url('component/videos/summary/' . $sumclass['attachment']); ?>"></source>
				</video>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Jadwal Zoom</h6>
						<button type="button" class="btn btn-success btn-sm btn-icon-split float-right" onclick="clone_form(this, '#zoom_clone', '#place_zoom')">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text">Tambah Jadwal</span>
						</button>
						<input type="hidden" name="zoom_count" id="zoom_count" value="<?= count($zclass); ?>">
					</div>
					<div class="card-body" id="place_zoom">
						<div class="form-group row pl-2 mb-1" id="zoom_clone" data-input='zoom' style="display: none;">
							<div class="col-sm-10 pl-0">
								<?= getInput('zoom[]', 'datetime-local', $validation, '', 'disabled'); ?>
							</div>
							<div class="col-sm-2 pl-0">
								<button type="button" class="btn btn-danger float-right btn-block" onclick="clone_del(this, '#zoom_count')">
									<span class="icon text-white">
										<i class="fas fa-trash"></i>
									</span>
								</button>
							</div>
						</div>
						<?php
						$tno = 1;
						foreach ($zclass as $zoom) {
							$date = date('Y-m-d', strtotime($zoom['schedule']));
							$time = date('H:i', strtotime($zoom['schedule']));
							?>
							<div class="row pl-2 mb-1">
								<label for="" class="col-sm-10 col-form-label pl-0"><?= tgl_indo2($date).' Pukul '.$time; ?></label>
								<div class="col-sm-2">
									<button type="button" class="btn btn-danger btn-icon-split float-right" data-uri="<?= base_url('administrator/classroom/remove_schedule'); ?>" data-file="" id="<?= $zoom['id']; ?>" onclick="del_file(this, '#zoom_count')">
										<span class="icon text-white">
											<i class="fas fa-trash"></i>
										</span>
									</button>
								</div>
							</div>
							<?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Treat</h6>
						<button type="button" class="btn btn-success btn-sm btn-icon-split float-right" onclick="clone_form(this, '#treat_clone', '#place_treat')">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text">Tambah Treat</span>
						</button>
						<input type="hidden" name="treat_count" id="treat_count" value="<?= count($tclass); ?>">
					</div>
					<div class="card-body" id="place_treat">
						<div class="form-group row pl-2 mb-1" id="treat_clone" data-input='treat' style="display: none;">
							<label for="" class="col-sm-2 col-form-label pl-0">Treat</label>
							<div class="col-sm-9">
								<?= getInput('treat[]', 'file', $validation, ''); ?>
							</div>
							<div class="col-sm-1">
								<button type="button" class="btn btn-danger btn-icon-split float-right" onclick="clone_del(this, '#treat_count')">
									<span class="icon text-white">
										<i class="fas fa-trash"></i>
									</span>
								</button>
							</div>
						</div>
						<?php
						$tno = 1;
						foreach ($tclass as $treat) {
							?>
							<div class="row pl-2 mb-1">
								<label for="" class="col-sm-8 col-form-label pl-0">Treat <?= $tno; ?></label>
								<div class="col-sm-4">
									<button type="button" class="btn btn-danger btn-icon-split float-right" data-uri="<?= base_url('administrator/classroom/remove_treat'); ?>" data-file="<?= $treat['attachment']; ?>" id="<?= $treat['id']; ?>" onclick="del_file(this, '#treat_count')">
										<span class="icon text-white">
											<i class="fas fa-trash"></i>
										</span>
									</button>
									<button type="button" data-title="Treat <?= $tno; ?>" data-media="embed" data-src="<?= base_url('component/pdf/treats/'.$treat['attachment']); ?>" data-type="application/pdf" class="btn btn-info btn-icon-split float-right" data-toggle="modal" data-target="#mediaModal">
										<span class="icon text-white">
											<i class="fas fa-eye"></i>
										</span>
									</button>
								</div>
							</div>
							<?php
							$tno++;
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-4 mb-3">
				<div class="card">
					<div class="card-header p-2">
						<h6 class="m-0 font-weight-bold text-primary d-inline">Video</h6>
						<button type="button" class="btn btn-success btn-sm btn-icon-split float-right" onclick="clone_form(this, '#video_clone', '#place_video')">
							<span class="icon text-white-50">
								<i class="fas fa-plus"></i>
							</span>
							<span class="text">Tambah Video</span>
						</button>
						<input type="hidden" name="video_count" id="video_count" value="<?= count($vclass); ?>">
					</div>
					<div class="card-body" id="place_video">
						<div class="form-group row pl-2 mb-1" id="video_clone" data-input='video' style="display: none;">
							<label for="" class="col-sm-2 col-form-label pl-0">Video</label>
							<div class="col-sm-9">
								<?= getInput('video[]', 'file', $validation, ''); ?>
							</div>
							<div class="col-sm-1">
								<button type="button" class="btn btn-danger btn-icon-split float-right" onclick="clone_del(this, '#video_count')">
									<span class="icon text-white">
										<i class="fas fa-trash"></i>
									</span>
								</button>
							</div>
						</div>
						<?php
						$vno = 1;
						foreach ($vclass as $video) {
							?>
							<div class="row pl-2 mb-1">
								<label for="" class="col-sm-8 col-form-label pl-0">Video <?= $vno; ?></label>
								<div class="col-sm-4">
									<button type="button" class="btn btn-danger btn-icon-split float-right" data-uri="<?= base_url('administrator/classroom/remove_video'); ?>" data-file="<?= $video['attachment']; ?>" id="<?= $video['id']; ?>" onclick="del_file(this, '#video_count')">
										<span class="icon text-white">
											<i class="fas fa-trash"></i>
										</span>
									</button>
									<button type="button" data-title="Video <?= $vno; ?>" data-media="video" data-src="<?= base_url('component/videos/class_video/'.$video['attachment']); ?>" class="btn btn-info btn-icon-split float-right" data-toggle="modal" data-target="#mediaModal">
										<span class="icon text-white">
											<i class="fas fa-eye"></i>
										</span>
									</button>
								</div>
							</div>
							<?php
							$vno++;
						}
						?>
					</div>
				</div>
			</div>
			<div class="col-md-12 mb-3">
				<button type="submit" class="btn btn-primary float-right">Simpan</button>
				<button disabled class="btn btn-primary float-right" id="button-load" hidden>
					<i class="fa fa-spinner fa-spin"></i>Loading
				</button>
				<a href="<?= base_url('administrator/classroom/schedule'); ?>" class="btn btn-danger float-right mr-2">Cancel</a>
			</div>
		</form>
	</div>
</div>
<?= $this->endSection('content'); ?>