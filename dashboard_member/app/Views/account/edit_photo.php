<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="container p-0 mt-2 mb-3">
	<form method="post" action="<?= base_url('account/profile/act_photo'); ?>" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-12 mt-2">
				<h3>Ubah Foto Profil</h3>
			</div>
		</div>
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata('message');?>
			</div>
			<?php
		} 
		?>
		<input type="hidden" name="id" value="<?= $user['id']; ?>">
		<div class="col-md-12 mb-3">
			<?= getInput('photo', 'file', $validation); ?>
			<img src="" class="mt-2 w-25 img-thumbnail img-preview">
		</div>

		<div class="row">
			<div class="col-md-12 mt-2">
				<button type="submit" class="btn btn-success pull-right">Simpan</button>
				<a href="<?= base_url('account/profile'); ?>" class="btn btn-danger pull-right">Batal</a>
			</div>
		</div>
	</form>
</div>
<?= $this->endSection('content') ?>