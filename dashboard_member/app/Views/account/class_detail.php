<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="m-3">
	<a href="<?= base_url("account/class_shop/class_list?package=$data[package_id]"); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h2 class="m-2"><?= $data['title']; ?></h2>
<hr>
<div class="container mt-3">
	<div class="row position-relative">
		<div class="col-md-4 p-3">
			<img src="<?= base_url('component/image/uploads_photo/' . $data['banner']); ?>" class="w-100 mx-auto d-block">
		</div>
		<div class="col-md-8 pt-3">
			<h5 class="m-0"><b>Tanggal Mulai </b></h5>
			<p class="mb-2"><?= tgl_indo($data['start_date']); ?></p>
			<h5 class="m-0"><b>Tanggal Berakhir </b></h5>
			<p class="mb-2"><?= tgl_indo($data['end_date']); ?></p>
			<h5 class="m-0"><b>Kapasitas Kelas </b></h5>
			<p class="mb-2"><?= $data['capacity']; ?> Orang</p>
			<a href="<?= base_url("account/class_shop/purchase_review?from=2&package=$data[package_id]&class=$data[id]"); ?>" class="btn btn-success">Pilih Kelas</a>
		</div>
		<div class="col-md-12 pt-3">
			<h4><b>Deskripsi : </b></h4><p><?= $data['desc']; ?></p>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>