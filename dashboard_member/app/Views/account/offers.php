<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Penawaran</h2>
<hr>
<div class="container mt-3">
	<?php
	foreach ($list as $data) { ?>
		<div class="row no-gutters bg-light position-relative">
			<div class="col-md-4 mb-md-0 p-3">
				<img src="<?= base_url('component/image/offers/' . $data['banner_img']); ?>" class="w-100 pull-right">
			</div>
			<div class="col-md-8 position-static p-3">
				<h5 class="mt-0"><?= $data['title']; ?></h5>
				<?php
				$date = date('Y-m-d', strtotime($data['created_at']));
				$time = date('H:i:s', strtotime($data['created_at']));
				?>
				<b>Tanggal Terbit : </b><?= tgl_indo($date); ?> <?= $time; ?>
				<p><?= readMoreHelper($data['content']); ?></p>
				<a href="<?= base_url('account/detail_offer/' . $data['slug']); ?>" class="stretched-link">Read More</a>
			</div>
		</div>
		<br>
		<?php
	} ?>
</div>
<?= $this->endSection('content'); ?>