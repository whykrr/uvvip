<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Metode Pembayaran</h2>
<hr>
<div class="container mt-3">
	<div class="row mx-1">
		<div class="col-md-12 px-0">
			<b>Nominal Pembayaran</b>
			<div class="alerts alert-dark text-center">
				<h2><?= format_rupiah($trans['price']); ?></h2>
			</div>
		</div>
		<div class="col-md-12 px-0 pb-3">
			<b>Pilih Metode Pembayaran</b>
		</div>
		<div class="col-md-12 px-0">
			<form method="get" action="<?= base_url('account/transaction/payment'); ?>">
				<input type="hidden" name="code" value="<?= $trans['transaction_code']; ?>">
				<div class="radio-group">
					<?php
					foreach ($data_bank as $data) {
						?>
						<div class="py-2">
							<input id="radio<?= $data['id']; ?>" type="radio" name="bank" value="<?= $data['id']; ?>" checked />
							<label for="radio<?= $data['id']; ?>">
								<img src="<?= base_url('component/image/bank/'.$data['logo']); ?>" class="radio" />
							</label>
						</div>
						<?php
					}
					?>
				</div>
				<button type="button" onclick="window.history.back();" class="btn btn-danger btn-lg">Kembali</a>
				<button type="submit" class="btn btn-success btn-lg pull-right">Pilih Metode Ini</button>
			</form>
		</div>
	</div>
</div>

<?= $this->endSection('content'); ?>