<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<?php 
$users = session()->get('users');
?>

<?php 
if ($edit == 'none') {		
	?>
	<div class="container-pp p-2">
		<?php 
		if(!$users['photo']){
			?>
			<div class="rounded-avatar">
				<img src="<?= base_url('component/image/profile_photo/avatar.png'); ?>">
			</div>
			<?php
		}else{
			?>
			<div class="rounded-pp">
				<img src="<?= base_url('component/image/profile_photo/' . $users['photo']); ?>">
			</div>
			<?php
		}
		?>
		<div class="name-tag">
			<?= ucwords($users['name']); ?>
		</div>
		<a href="<?= base_url('account/profile/edit_photo'); ?>" class="btn btn-secondary mt-2">Ubah Foto</a>
	</div>
	<?php
}
?>
<div class="container p-0 mt-2 mb-3">
	<?php 
	if ($edit == 'none' || $edit == 'personal') {		
		?>
		<form method="post" action="<?= base_url('account/profile/act_personal'); ?>" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">

					<h3>PERSONAL DATA</h3>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="name" class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-10">
					<input type="text" <?= $sf; ?> class="form-control <?= ($validation->hasError('name')) ? 'is-invalid' : '' ; ?>" id="name" name="name" value="<?= (old('name')) ? old('name') : $users['name']; ?>">
					<div class="invalid-feedback"><?= $validation->getError('name'); ?></div>	
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="address" class="col-sm-2 col-form-label">Alamat</label>
				<div class="col-sm-10">
					<textarea <?= $sf; ?> class="form-control <?= ($validation->hasError('address')) ? 'is-invalid' : '' ; ?>" id='address' name="address"><?= (old('address')) ? old('address') : $users['address']; ?></textarea>
					<div class="invalid-feedback"><?= $validation->getError('address'); ?></div>	
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="phone" class="col-sm-2 col-form-label">Nomor Telepon</label>
				<div class="col-sm-10">
					<input type="text" <?= $sf; ?> class="form-control <?= ($validation->hasError('phone')) ? 'is-invalid' : '' ; ?>" id="phone" name="phone" value="<?= (old('phone')) ? old('phone') : $users['phone']; ?>">
					<div class="invalid-feedback"><?= $validation->getError('phone'); ?></div>	
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="email" class="col-sm-2 col-form-label">E-Mail</label>
				<div class="col-sm-10">
					<input type="text" <?= $sf; ?> class="form-control <?= ($validation->hasError('email')) ? 'is-invalid' : '' ; ?>" id="email" name="email" value="<?= (old('email')) ? old('email') : $users['email']; ?>">
					<div class="invalid-feedback"><?= $validation->getError('email'); ?></div>	
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="facebook" class="col-sm-2 col-form-label">Facebook</label>
				<div class="col-sm-10">
					<input type="text" <?= $sf; ?> class="form-control" id="facebook" name="facebook" value="<?= (old('facebook')) ? old('facebook') : $users['facebook']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="instagram" class="col-sm-2 col-form-label">Instagram</label>
				<div class="col-sm-10">
					<input type="text" <?= $sf; ?> class="form-control" id="instagram" name="instagram" value="<?= (old('instagram')) ? old('instagram') : $users['instagram']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="twitter" class="col-sm-2 col-form-label">Twitter</label>
				<div class="col-sm-10">
					<input type="text" <?= $sf; ?> class="form-control" id="twitter" name="twitter" value="<?= (old('twitter')) ? old('twitter') : $users['twitter']; ?>">
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="identity_card" class="col-sm-2 col-form-label">Kartu Identitas</label>
				<div class="col-sm-10">
					<?= getInput('identity_card', 'file', $validation, $users['identity_card'], $sf); ?>
					<img src="<?= ($users['identity_card']) ? base_url('component/image/identity/'. $users['identity_card']) : base_url('component/image/identity/default.png') ; ?>" class="mt-2 w-25 img-thumbnail img-preview">
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 mt-2">
					<?php
					if ($edit == 'none') {	
						?>
						<a href="<?= base_url('account/profile/edit_personal'); ?>" class='btn btn-primary pull-right'>Ubah Personal Data</a>
						<?php
					} else if($edit == 'personal'){
						?>
						<button type="submit" class="btn btn-success pull-right">Simpan</button>
						<a href="<?= base_url('account/profile'); ?>" class="btn btn-danger pull-right">Batal</a>
						<?php
					}
					?>
				</div>
			</div>
		</form>
		<?php 
	}
	if ($edit == 'none' || $edit == 'security') {		
		?>
		<hr>
		<form method="post" action="<?= base_url('account/profile/act_username'); ?>">
			<div class="row">
				<div class="col-md-12 mt-2">
					<h3>KEAMANAN</h3>
				</div>
			</div>
			<div class="form-group row pl-2 mb-1">
				<label for="username" class="col-sm-2 col-form-label">Nama Pengguna</label>
				<div class="col-sm-10">
					<input type="text" <?= $sf; ?> class="form-control <?= ($validation->hasError('username')) ? 'is-invalid' : '' ; ?>" id="username" name="username" value="<?= (old('username')) ? old('username') : $users['username']; ?>">
					<div class="invalid-feedback"><?= $validation->getError('username'); ?></div>	
				</div>
			</div>
			<?php 
			if ($edit == 'none') {		
				?>
				<div class="form-group row pl-2 mb-1">
					<label for="password" class="col-sm-2 col-form-label">Password</label>
					<div class="col-sm-10">
						<input type="password" <?= $sf; ?> class="form-control" id="password" name="password" value="********">
					</div>
				</div>
				<?php 
			}
			?>
			<div class="row">
				<div class="col-md-12 mt-2">
					<?php
					if ($edit == 'none') {	
						?>
						<a href="<?= base_url('account/profile/edit_password'); ?>" class='btn btn-primary pull-right'>Ubah Password</a>
						<a href="<?= base_url('account/profile/edit_username'); ?>" class='btn btn-primary pull-right'>Ubah Nama Pengguna</a>
						<?php
					} 
					else if($edit == 'security'){
						?>
						<button type="submit" class="btn btn-success pull-right">Simpan</button>
						<a href="<?= base_url('account/profile'); ?>" class="btn btn-danger pull-right">Batal</a>
						<?php
					}
					?>
				</div>
			</div>
		</form>
		<?php 
	}
	if ($edit == 'none') {	
		?>
		<hr>
		<div class="row">
			<div class="col-md-12 mt-3">
				<center>
					<label>Kode Referal Anda</label>
					<h3><?= $users['referal_code']; ?></h3>
				</center>
			</div>
		</div>
		<?php
	}
	?>
</div>
<?= $this->endSection('content'); ?>