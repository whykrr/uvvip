<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="m-3">
	<a href="<?= base_url('account/transaction'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h2 class="m-2">Detail Pendaftaran</h2>
<hr>
<div class="container mt-3">
	<?php
	$date = date('Y-m-d', strtotime($data['created_at']));
	$time = date('H:i:s', strtotime($data['created_at']));
	?>
	<div class="row mx-1">
		<div class="col-md-6 p-1">
			<label>Nomor Pendafataran</label>
			<input disabled class="form-control" value="<?= $data['transaction_code']; ?>">
		</div>
		<div class="col-md-6 p-1">
			<label>Nama Pengguna</label>
			<input disabled class="form-control" value="<?= $data['name']; ?>">
		</div>
		<div class="col-md-6 p-1">
			<label>Tanggal</label>
			<input disabled class="form-control" value="<?= tgl_indo($date); ?>">
		</div>
		<div class="col-md-6 p-1">
			<label>Waktu</label>
			<input disabled class="form-control" value="<?= $time; ?>">
		</div>
		<div class="col-md-12 p-1">
			<label>Tipe Paket</label>
			<input disabled class="form-control" value="<?= $data['package_name']; ?>">
		</div>
		<div class="col-md-12 p-1">
			<label>Tipe Kelas</label>
			<input disabled class="form-control" value="<?= $data['class_name']; ?>">
		</div>
		<div class="col-md-12 p-1">
			<label>Status Pembayaran</label><br>
			<span class="badge badge-<?= $sp['style']; ?> p-2"><?= $sp['status']; ?></span>
		</div>
		<div class="col-md-12 p-1">
			<b class="text-danger">Note : </b>
			<?php 
			$datepb = date('Y-m-d', strtotime($data['pay_before']));
			$timepb = date('H:i', strtotime($data['pay_before']));
			?>
			<p class="text-danger d-inline">Harap bayar sebelum sebelum <?= tgl_indo($datepb); ?> <?= $timepb; ?></p>
		</div>
		<?php
		if($sp['status'] == 'Unpaid'){
			?>
			<div class="col-md-12 p-1">
				<a href="<?= base_url('account/transaction/payment_method?code='. $data['transaction_code']); ?>" class="btn btn-info btn-lg pull-right">Bayar</a>
				<form method="POST" action="<?= base_url('account/transaction/cancel_transaction'); ?>">
					<input type="hidden" name="transaction_code" value="<?= $data['transaction_code']; ?>">
					<button type="submit" class="btn btn-danger btn-lg pull-right">Batalkan Pendaftaran</button>
				</form>
			</div>
			<?php
		}?>
	</div>
</div>

<?= $this->endSection('content'); ?>