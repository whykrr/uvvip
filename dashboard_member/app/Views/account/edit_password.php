<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="container p-0 mt-2 mb-3">
	<form method="post" action="<?= base_url('account/profile/act_password'); ?>">
		<div class="row">
			<div class="col-md-12 mt-2">
				<h3>Ubah Password</h3>
			</div>
		</div>
		<?php 
		if(!empty(session()->getFlashdata('message'))) { ?>
			<div class="alert alert-danger">
				<?php echo session()->getFlashdata('message');?>
			</div>
			<?php
		} 
		?>
		<div class="form-group row pl-2 mb-1">
			<label for="username" class="col-sm-4 col-form-label">Password Lama</label>
			<div class="input-group col-sm-8">
				<?= getInput('old_password', 'password', $validation); ?>
				<div class="input-group-append" onclick="shp(this)">
					<span class="input-group-text"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
				</div>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="username" class="col-sm-4 col-form-label">Password Baru</label>
			<div class="input-group col-sm-8">
				<?= getInput('password', 'password', $validation); ?>
				<div class="input-group-append" onclick="shp(this)">
					<span class="input-group-text"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
				</div>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="username" class="col-sm-4 col-form-label">Konfirmasi Password Baru</label>
			<div class="input-group col-sm-8">
				<?= getInput('pass_confirm', 'password', $validation); ?>
				<div class="input-group-append" onclick="shp(this)">
					<span class="input-group-text"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 mt-2">
				<button type="submit" class="btn btn-success pull-right">Simpan</button>
				<a href="<?= base_url('account/profile'); ?>" class="btn btn-danger pull-right">Batal</a>
			</div>
		</div>
	</form>
</div>
<?= $this->endSection('content') ?>