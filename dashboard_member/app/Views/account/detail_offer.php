<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="m-3">
	<a href="<?= base_url('account/offers'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h2 class="m-2"><?= $data['title']; ?></h2>
<hr>
<div class="container mt-3">
	<div class="row position-relative">
		<div class="col-md-12 pt-3">
			<?php
			$date = date('Y-m-d', strtotime($data['created_at']));
			$time = date('H:i:s', strtotime($data['created_at']));
			?>
			<b>Tanggal Terbit : </b><?= tgl_indo($date); ?> <?= $time; ?>
		</div>
		<div class="col-md-12 p-3">
			<img src="<?= base_url('component/image/offers/' . $data['banner_img']); ?>" class="w-50 mx-auto d-block">
		</div>
		<div class="col-md-12 p-3">
			<p><?= $data['content_wysiwig']; ?></p>
		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>