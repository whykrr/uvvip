<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Pendaftaran</h2>

<div class="container mt-3">
	<form method="post" action="<?= base_url('account/transaction/save_confirm_payment'); ?>" enctype="multipart/form-data">
		<div class="row mx-1">
			<h5>Form Konfirmasi Pembayaran</h5>
			<hr>
		</div>
		<div class="form-group row pl-2 mb-1">
			<input type="hidden" name="transaction_id" value="<?= $trans['id']; ?>">
			<label for="transaction_code" class="col-sm-4 col-form-label">Nomor Pendaftaran</label>
			<div class="col-sm-8">
				<?= getInput('transaction_code', 'text', $validation, @$request['code'], 'readonly'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="bank_id" class="col-sm-4 col-form-label">Bank Asal</label>
			<div class="col-sm-8">
				<?= getSelect('bank_id', $data_bank, $validation, @$request['bank'], 'disabled'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="bank_acc_name" class="col-sm-4 col-form-label">Nama Pemilik Rekening</label>
			<div class="col-sm-8">
				<?= getInput('bank_acc_name', 'text', $validation); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="bank_acc" class="col-sm-4 col-form-label">Nomor Rekening</label>
			<div class="col-sm-8">
				<?= getInput('bank_acc', 'text', $validation); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="nominal" class="col-sm-4 col-form-label">Biaya Pendaftaran</label>
			<div class="col-sm-8">
				<?= getInput('nominal', 'number', $validation, @$trans['price'], 'readonly'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="class_id" class="col-sm-4 col-form-label">Jenis Paket</label>
			<div class="col-sm-8">
				<?= getSelect('package_id', $data_package, $validation, @$trans['package_id'], 'disabled'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="class_id" class="col-sm-4 col-form-label">Jenis Kelas</label>
			<div class="col-sm-8">
				<?= getSelect('class_id', $data_class, $validation, @$trans['class_id'], 'disabled'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="attachment" class="col-sm-4 col-form-label">Bukti Transfer</label>
			<div class="col-sm-8">
				<?= getInput('attachment', 'file', $validation, @$data['attachment']); ?>
				<img src="" class="mt-2 w-25 img-thumbnail img-preview">
			</div>
		</div>

		<button type="submit" class="btn btn-primary pull-right">SIMPAN</button>
	</form>
</div>

<?= $this->endSection('content'); ?>