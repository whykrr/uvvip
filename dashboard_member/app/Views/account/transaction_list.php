<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Pendaftaran</h2>
<nav class="nav nav-pills nav-fill mx-2">
	<a class="nav-link active" href="#">Pendaftaran</a>
	<div class="separator mx-2"></div>
	<a class="nav-link" href="<?= base_url('account/transaction/history'); ?>">Riwayat Pendaftaran</a>
</nav>
<div class="container mt-3">
	<div class="row mx-1">
		<h5>Informasi Pendaftaran</h5>
		<?php
		if (!empty(session()->getFlashdata('message'))) { ?>
			<div class="col-sm-12 px-0">
				<div class="alert alert-success">
					<?php echo session()->getFlashdata('message'); ?>
				</div>
			</div>
		<?php
		}
		?>
		<?php if ($empty_trans == 'true') {
		?>
			<div class="col-md-12 px-0">
				<div class="alerts alert-dark text-center" role="alert">Anda tidak memiliki data pendaftaran!</div>
			</div>
		<?php
		} else { ?>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">No</th>
							<th scope="col">Kode Pendaftaran</th>
							<th scope="col">Jenis Paket</th>
							<th scope="col">Jenis Kelas</th>
							<th scope="col">Tanggal Pendaftaran</th>
							<th scope="col">Status Pembayaran</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;
						foreach ($list as $data) {
						?>
							<tr>
								<th scope="row"><?= $no++; ?></th>
								<td><?= $data['transaction_code']; ?></td>
								<td><?= $data['package_name']; ?></td>
								<td><?= $data['class_name']; ?></td>
								<td><?= tgl_indo($data['created_at']); ?></td>
								<td><span class="badge badge-<?= $data['ps_style']; ?> p-2"><?= $data['payment_status']; ?></span></td>
								<td>
									<?php
									if ($data['payment_status'] == 'Unpaid') {
									?>
										<a href="<?= base_url("account/transaction_detail/$data[transaction_code]"); ?>" class="btn btn-success btn-sm">Konfirmasi Pembayaran</a>
									<?php
									} else {
									?>
										<a href="<?= base_url("account/transaction_detail/$data[transaction_code]"); ?>" class="btn btn-info btn-sm">Detail</a>
									<?php
									}
									?>
								</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		<?php
		} ?>
	</div>
</div>
<?= $this->endSection('content'); ?>