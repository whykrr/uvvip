<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Pembayaran</h2>
<hr>
<div class="container mt-3">
	<div class="row mx-1">
		
		<div class="col-md-6 px-0 pr-2">
			<b>Nominal Pembayaran</b>
			<div class="alerts alert-dark text-center mb-0">
				<h3><?= format_rupiah($trans['price']); ?></h3>
			</div>
			<br>
			<b>Virtual Account <?= $data_bank['name']; ?></b>
			<div class="alerts alert-dark text-center">
				<h3><?= $data_bank['va_code']; ?></h3>
			</div>
		</div>
		<div class="col-md-6 px-0 pl-2">
			<b>Tata Cara Pembayaran</b>
			<div class="accordion" id="accordionExample">
				<div class="card">
					<div class="card-header p-0" id="headingOne">
						<h2 class="mb-0">
							<button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								Melalui ATM
							</button>
						</h2>
					</div>
					<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
						<div class="card-body">
							<?= $data_bank['method_atm'] ?>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header p-0" id="headingTwo">
						<h2 class="mb-0">
							<button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								Melalui Internet Banking
							</button>
						</h2>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
						<div class="card-body">
							<?= $data_bank['method_internet_bank'] ?>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header p-0" id="headingThree">
						<h2 class="mb-0">
							<button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								Melalui Mobile Banking
							</button>
						</h2>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
						<div class="card-body">
							<?= $data_bank['method_mobile'] ?>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header p-0" id="headingFour">
						<h2 class="mb-0">
							<button class="btn btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
								Melalui Teler Bank
							</button>
						</h2>
					</div>
					<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
						<div class="card-body">
							<?= $data_bank['method_teller'] ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12 px-0 pt-4">
			<form method="get" action="<?= base_url('account/confirm_payment'); ?>">
				<input type="hidden" name="code" value="<?= $trans['transaction_code']; ?>">
				<input type="hidden" name="bank" value="<?= $data_bank['id'];; ?>">
				<button type="button" onclick="window.history.back();" class="btn btn-danger btn-lg">Kembali</a>
				<button type="submit" class="btn btn-success btn-lg pull-right">Konfirmasi Pembayaran</button>
			</form>
		</div>
	</div>
</div>

<?= $this->endSection('content'); ?>