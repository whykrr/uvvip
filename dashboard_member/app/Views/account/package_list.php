<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Kelas</h2>

<div class="container mt-3">
	<div class="row mx-1">
		<h5>Pilih Paket</h5>
	</div>
	<div class="row mx-0 p-0">
		<?php
		$no = 1;
		foreach ($list as $data) {
		?>
			<div class="col-md-4 px-2">
				<div class="card">
					<img src="<?= base_url('component/image/package/' . $data['image']); ?>" class="card-img-top">
					<div class="card-body">
						<h4 class="card-title text-center font-weight-bold"><?= $data['name']; ?></h4>
						<h5 class="card-title text-center font-weight-bold"><?= format_kilo($data['price']); ?></h5>
						<p style="height: 100px;" class="card-text">
							<?= $data['features']; ?>
						</p>
						<a href="<?= base_url('account/class_shop/class_list?package=' . $data['id']); ?>" class="btn btn-info btn-block">Pilih Kelas</a>
					</div>
				</div>
			</div>
		<?php
		}
		?>
	</div>
</div>
<?= $this->endSection('content'); ?>