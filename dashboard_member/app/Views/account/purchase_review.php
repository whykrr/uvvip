<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Kelas</h2>
<div class="container mt-3">
	<?php 
	if(!empty(session()->getFlashdata('message'))) { ?>
		<div class="col-sm-12 px-0">
			<div class="alert alert-info">
				<?php echo session()->getFlashdata('message');?>
			</div>
		</div>
		<?php
	} 
	?>
	<form method="post" action="<?= base_url('account/class_shop/act_purchase'); ?>">
		<div class="row mx-1">
			<h5>Form Pendaftaran</h5>
			<hr>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="transaction_code" class="col-sm-4 col-form-label">Nomor Pendaftaran</label>
			<div class="col-sm-8">
				<?= getInput('transaction_code', 'text', $validation, $transaction_code, 'readonly'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="transaction_code" class="col-sm-4 col-form-label">Nama Pengguna</label>
			<div class="col-sm-8">
				<?= getInput('users', 'text', $validation, $users['name'], 'readonly'); ?>
				<input type="hidden" name="user_id" value="<?= $users['id']; ?>">
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="transaction_code" class="col-sm-4 col-form-label">Nama Paket</label>
			<div class="col-sm-8">
				<?= getInput('package_name', 'text', $validation, $package['name'], 'readonly'); ?>
				<input type="hidden" name="package_id" value="<?= $package['id']; ?>">
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="transaction_code" class="col-sm-4 col-form-label">Nama Kelas</label>
			<div class="col-sm-8">
				<?= getInput('package_name', 'text', $validation, $class['title'], 'readonly'); ?>
				<input type="hidden" name="class_id" value="<?= $class['id']; ?>">
				<input type="hidden" name="capacity" value="<?= $class['capacity']; ?>">
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="transaction_code" class="col-sm-4 col-form-label">Tanggal Pendaftaran</label>
			<div class="col-sm-8">
				<?= getInput('transaction_date', 'text', $validation, tgl_indo(date('Y-m-d')), 'readonly'); ?>
			</div>
		</div>
		<div class="form-group row pl-2 mb-1">
			<label for="transaction_code" class="col-sm-4 col-form-label">Harga</label>
			<div class="col-sm-8">
				<?= getInput('price', 'text', $validation, $package['price'], 'readonly'); ?>
			</div>
		</div>
		<div class="col-md-12 px-0 pt-3">
			<button class="btn btn-success pull-right">Daftar Kelas</button>
			<a href="<?= ($from == 1) ? base_url("account/class_shop/class_list?package=$package[id]") : base_url("account/class_shop/class_detail?package=$package[id]&class=$class[id]"); ?>" class="btn btn-danger pull-right">Kembali</a>
		</div>
	</form>
</div>
<?= $this->endSection('content'); ?>