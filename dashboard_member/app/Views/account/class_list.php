<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="m-3">
	<a href="<?= base_url('account/class_shop'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h2 class="m-2">Kelas</h2>
<div class="container mt-3">
	<div class="row mx-1">
		<h5>Pilih Kelas</h5>
		<?php if ($empty_class == 'true') {
		?>
			<div class="col-md-12 px-0">
				<div class="alerts alert-dark text-center" role="alert">Tidak ada kelas di paket ini!</div>
			</div>
		<?php
		} else { ?>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">No</th>
							<th scope="col">Kelas</th>
							<th scope="col">Kapasias Kelas</th>
							<th scope="col">Periode</th>
							<th scope="col">Status</th>
							<th scope="col">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$no = 1;
						foreach ($list as $data) {
						?>
							<tr>
								<th scope="row"><?= $no++; ?></th>
								<td><?= $data['title']; ?></td>
								<td><?= $data['capacity']; ?> Orang</td>
								<td><?= tgl_indo($data['start_date']); ?> - <?= tgl_indo($data['end_date']); ?></td>
								<td><?= ($data['status_participant'] == 'Open') ? '<span class="badge badge-success">Open</span>' : '<span class="badge badge-danger">Full Booked</span>'; ?></td>
								<td>
									<?php
									if ($data['status_participant'] == 'Open') {
									?>
										<a href="<?= base_url("account/class_shop/class_detail?package=$data[package_id]&class=$data[id]"); ?>" class="btn btn-info btn-sm">Detail</a>
										<a href="<?= base_url("account/class_shop/purchase_review?from=1&package=$data[package_id]&class=$data[id]"); ?>" class="btn btn-warning btn-sm">Pilih</a>
									<?php
									}
									?>
								</td>
							</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
		<?php
		} ?>
	</div>
</div>
<?= $this->endSection('content'); ?>