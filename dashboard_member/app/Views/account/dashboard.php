<?= $this->extend('layout/account') ?>

<?= $this->section('content') ?>
<h4>GENERAL</h4>
<div class="date-info">
	1 Januari 1999
</div>

<div class="row" style="margin-right: 16px;margin-left: 10px;">
	<div class="col-lg-12">
		<div class="card no-border" style="margin-bottom: 25px;">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-4">
						<div class="card active-card no-border">
							<div class="card-body" style="padding-top: 10px;padding-bottom: 10px;">
								<div class="d-flex">
									<div class="menu-top">
									<img src="http://localhost:8080/component/image/asset/web.png" alt="" style="width: 100%;">
								</div>
								<div class="mr-auto align-self-center" style="padding-left: 20px;">
									<h6>PESERTA BARU</h6>
								</div>
							</div>
						</div>
					</div>
					</div>
					<div class="col-lg-4">
						<div class="card no-border">
							<div class="card-body" style="padding-top: 10px;padding-bottom: 10px;">
								<div class="d-flex">
									<div class="menu-top">
									<img src="http://localhost:8080/component/image/asset/web.png" alt="" style="width: 100%;">
								</div>
								<div class="mr-auto align-self-center" style="padding-left: 20px;">
									<h6>INTERAKSI</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
						<div class="card no-border">
							<div class="card-body" style="padding-top: 10px;padding-bottom: 10px;">
								<div class="d-flex">
									<div class="menu-top">
									<img src="http://localhost:8080/component/image/asset/web.png" alt="" style="width: 100%;">
								</div>
								<div class="mr-auto align-self-center" style="padding-left: 20px;">
									<h6>TRADING CONTEST</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<p>Perseta Baru</p>
	<hr>

	<?php dd($member); ?>

	<div class="row">
		<div class="col-lg-12">
			<table class="table table-stiped tabel-account">
				<thead>
					<tr>
						<td><div style="text-align: center;" class="col">Peserta</div></td>
						<td><div style="text-align: center;" class="col">Nama</div></td>
						<td><div style="text-align: center;" class="col">Alamat</div></td>
						<td><div style="text-align: center;" class="col">Kelas</div></td>
						<td><div style="text-align: center;width: 90%;" class="col">Date</div></td>
					</tr>
				</thead>
				<tr>
					<td>Foto</td>
					<td>Pevita</td>
					<td>Malang</td>
					<td>Basic</td>
					<td>28 Nov 2020</td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>Pevita</td>
					<td>Malang</td>
					<td>Basic</td>
					<td>28 Nov 2020</td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>Pevita</td>
					<td>Malang</td>
					<td>Basic</td>
					<td>28 Nov 2020</td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>Pevita</td>
					<td>Malang</td>
					<td>Basic</td>
					<td>28 Nov 2020</td>
				</tr>
				<tr>
					<td>Foto</td>
					<td>Pevita</td>
					<td>Malang</td>
					<td>Basic</td>
					<td>28 Nov 2020</td>
				</tr>
			</table>
		</div>
	</div>

	<p>Diskusi Hari ini</p>
	<hr>

	<div class="row">
		<div class="col-lg-6">
			<img src="<?= base_url('component/image/trading.png'); ?>" class="img-fluid">
		</div>
		<div class="col-lg-6">
			<h5>MENGENAL DUNIA CRYPTO</h5>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			<a href="" style="color: #FCAF01;">Read More</a>
		</div>
		<div class="col-lg-12">
			<hr>
			<div class="d-flex justify-content-center">Like | Komentar</div>
			<hr>
			<div class="row">
			<div class="col-lg-1">
				<img src="<?= base_url('component/image/user.png') ?>" class="img-fluid">
			</div>
			<div class="col-lg-11">
				<div class="card">
					<div class="card-body" style="padding: 10px;">
						<h6>Gabriella Putri</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>
				</div>
			</div>
			</div>
		</div>

	</div>
</div>
</div>
<?= $this->endSection('content'); ?>