<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<a href="<?= base_url('classroom/room'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
<h4>VIDEO KELAS</h4>

<div class="accordion" id="materi" style="height: 75%;">
	<div class="col-lg-12">
		<div id="flex-wrapper" style="width: 100%;">
			<div id="flex-container">
				<div id="flex-inside">
					<?php $no = 1; ?>
					<?php foreach ($video as $row) {
						?>
						<div class="flex-item">
							<a data-toggle="collapse" href="#materi<?= $no; ?>" role="button" class="text-reset">
								<img src="<?= base_url('component/image/asset/video.png'); ?>" alt="" class="img-horizontal">
								<h6>Materi Video <?= $no++; ?></h6>
							</a>
						</div>
						<?php
					} 
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12 pb-2" style="height: 100%;">
		<div class="row h-100">
			<?php $no = 1; ?>
			<?php foreach ($video as $row) {
				?>
				<div class="col-lg-12 h-100 collapse" id="materi<?= $no++; ?>" data-parent="#materi">
					<video width="100%;" height="100%" controls>
						<source src="<?= base_url('component/videos/class_video/'.$row['attachment']);?>" type="video/mp4">
							Your browser does not support the video tag.
						</source>
					</video>
				</div>
				<?php
			} 
			?>

		</div>		
	</div>
</div>
<?= $this->endSection('content'); ?>
