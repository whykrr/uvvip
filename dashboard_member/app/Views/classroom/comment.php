<?php 
$komentar = $komen->ForumComment($id)->getResultArray();

foreach ($komentar as $kom) {
	?>
	<div class="row mb-0">
		<div class="col-lg-12">
			<div class="card card-comment">
				<div class="card-body p-1">
					<h6 class="mb-0 d-inline"><?= $kom['name']; ?></h6>
					<p class="mb-0 d-inline"><?= $kom['comment']; ?></p>
					<?php 
					$datepb = date('Y-m-d', strtotime($kom['created_at']));
					$timepb = date('H:i', strtotime($kom['created_at']));
					?>
					<p class="card-text"><small class="text-muted"><?= tgl_indo($datepb); ?> <?= $timepb; ?></small></p>
				</div>
			</div>
		</div>
	</div>
	<?php 
} ?>