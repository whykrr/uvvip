<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>

<a href="<?= base_url('classroom/room'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
<h2 class="m-2">Message <?= $class['title']; ?></h2>

<div class="row" style="margin-right: 16px;margin-left: 10px;">

	<div class="col-lg-12 px-0">
		<div class="card bg-light"id="body-msg" style="height: 430px; overflow-y: scroll;">
			<div class="card-body" style="padding-bottom: 20px;">
				<?php foreach ($read_chat as $row) {
					?>
					<div class="d-flex <?= ($row['user_id'] == 'M'.$users['id']) ? 'flex-row-reverse' : 'flex-row' ; ?> mb-2">
						<div class="card" style="background-color: #048894;color: #FFF;">
							<div class="card-body" style="padding: 10px;">
								<b><p class="mb-0"><?= $row['name']; ?></p></b>
								<p style="margin: 0;"><?= $row['message']; ?> <br> <span style="color: #FFF;font-size: 12px;"><?= $row['created_at']; ?></span></p>
							</div>
						</div>
					</div>
					<?php
				}
				if($unread_chat){
					?>
					<hr class="mb-1 " id="unread-msg">
					<p class="text-center">Pesan belum terbaca</p>
					<?php
				}
				foreach ($unread_chat as $row) {
					?>
					<div class="d-flex <?= ($row['user_id'] == 'M'.$users['id']) ? 'flex-row-reverse' : 'flex-row' ; ?> mb-2">
						<div class="card" style="background-color: #048894;color: #FFF;">
							<div class="card-body" style="padding: 10px;">
								<b><p class="mb-0"><?= $row['name']; ?></p></b>
								<p style="margin: 0;"><?= $row['message']; ?> <br> <span style="color: #FFF;font-size: 12px;"><?= $row['created_at']; ?></span></p>
							</div>
						</div>


					</div>
					<?php
				} ?>
			</div>
		</div>
		<div class="row" style="margin-top: 20px;">
			<input type="hidden" name="class_id" id="class_id" value="<?= $class['id']; ?>">
			<div class="col-lg-10">
				<textarea class="form-control" name="message" id="message" required=""></textarea>
			</div>
			<div class="col-lg-2">
				<button type="button" class="btn btn-primary btn-block btn-kirim">Kirim</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		<?php
		if($unread_chat){
			?>
			$('#body-msg').animate({ scrollTop: $('#unread-msg').offset().top-150 }, 'slow');
			<?php
		}else{
			?>
			$('#body-msg').scrollTop($('#body-msg')[0].scrollHeight);
			<?php
		}
		?>
	})
	$('.btn-kirim').click(function(){
		var message = $('#message').val();
		var class_id = $('#class_id').val();

		if(message == ''){
			alert('Pesan Harus di isi');
			return false;
		}

		$.ajax({
			type: 'post',
			url: '<?= base_url('classroom/room/save_chat'); ?>',
			data: {message:message, class_id:class_id},
			beforeSend:function(){
				$(".loading").show();
			},
			success: function(data)
			{
				// $(".loading").hide();
				location.reload();
			},
			error : function() {

				// $(".loading").hide();
				location.reload();
			}
		});
	});
	
</script>
<?= $this->endSection('content'); ?>