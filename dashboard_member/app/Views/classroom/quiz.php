<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>

<a href="<?= base_url('classroom/room'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
<h4>QUIZ</h4>
<?php
$answer = explode('|', $score['answers']);
$no = 1;
?>
<form action="<?= base_url('classroom/room/send_answer');?>" method="POST">
	<div class="row">
		<div class="col-lg-12">
			<?php foreach ($quiz as $key => $row) {
				?>
				<div class="form-group">
					<label><?= $no.'. '.$row['question']; ?></label><br>
					<label class="ml-3"><input <?= ($scorenya != 0) ? ($answer[$key] == 'a') ? 'checked disabled' : 'disabled' : '' ; ?> type="radio" name="answer<?= $row['id'];?>" value="a"> <?= $row['option_a']; ?> </label><br>
					<label class="ml-3"><input <?= ($scorenya != 0) ? ($answer[$key] == 'b') ? 'checked disabled' : 'disabled' : '' ; ?> type="radio" name="answer<?= $row['id'];?>" value="b"> <?= $row['option_b']; ?> </label><br>
					<label class="ml-3"><input <?= ($scorenya != 0) ? ($answer[$key] == 'c') ? 'checked disabled' : 'disabled' : '' ; ?> type="radio" name="answer<?= $row['id'];?>" value="c"> <?= $row['option_c']; ?> </label><br>
					<label class="ml-3"><input <?= ($scorenya != 0) ? ($answer[$key] == 'd') ? 'checked disabled' : 'disabled' : '' ; ?> type="radio" name="answer<?= $row['id'];?>" value="d"> <?= $row['option_d']; ?> </label>
					<?php if($scorenya != 0){ ?>
						<div class="pl-3">Kunci Jawaban : <p class="text-uppercase pb-0 d-inline"><?= $row['key']; ?></p></div>
					<?php } ?>
				</div>
				<?php
				$no++;
			} ?>

		</div>
		<input type="hidden" name="id_class" value="<?php if(isset($id_class)){ echo $id_class; } ?>">
		<?php if($scorenya == 0){ ?>
			<div class="col-lg-2"><button type="submit" class="btn btn-primary btn-block" style="margin-top: 30px;">SEND</button></div>
		<?php } ?>
	</div>

</form>
<?php if($scorenya != 0){ ?>
	<div class="row">
		<div class="col-lg-12" style="text-align: center;">
			<p class="mb-3">Score Anda</p>
			<h1 class="mb-1" style="font-size: 6rem;"><?= $score['score']; ?></h1>
		</div>
	</div>
<?php } ?>
<?= $this->endSection('content'); ?>