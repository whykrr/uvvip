<div class="card w-100 mb-3 no-border bg-light">
	<div class="card-body p-3">
		<div class="row">
			<div class="col-lg-6 pr-2">
				<a class="text-dark" href="<?= base_url('classroom/room'); ?>">
					<div class="card card-menu no-border <?= ($submenu_active == 'room') ? 'active-card' : ''; ?>">
						<div class="card-body py-2">
							<div class="d-flex">
								<div class="menu-top">
									<img src="<?= base_url('component/image/asset/today-class.png'); ?>" alt="" class="w-100">
								</div>
								<div class="mr-auto align-self-center pl-2">
									<h5 class="mb-0">My Class</h5>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-6 px-2">
				<a class="text-dark" href="<?= base_url('classroom/room/histori'); ?>">
					<div class="card card-menu no-border <?= ($submenu_active == 'history') ? 'active-card' : ''; ?>">
						<div class="card-body py-2">
							<div class="d-flex">
								<div class="menu-top">
									<img src="<?= base_url('component/image/asset/history-class.png'); ?>" alt="" class="w-100">
								</div>
								<div class="mr-auto align-self-center pl-2">
									<h5 class="mb-0">History Class</h5>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>