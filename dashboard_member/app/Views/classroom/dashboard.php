<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>

<h2 class="m-2">Dashboard</h2>
<div class="date-info m-2">
	<?= tgl_indo(date('Y-m-d')); ?>
</div>

<div class="container">
	<div class="row">
		<div class="col-lg-12">

			<?= $submenu; ?>

			<h4>Perseta Baru</h4>
			

			<div class="row">
				<div class="col-lg-12">
					<table class="table table-stiped tabel-account">
						<thead>
							<tr>
								<th class="text-center py-2">Peserta</th>
								<th class="text-center py-2">Nama</th>
								<th class="text-center py-2">Alamat</th>
								<th class="text-center py-2">Kelas</th>
								<th class="text-center py-2">Tanggal Pendaftaran</th>
							</tr>
						</thead>
						<?php foreach ($member as $row) {
							?>
							<tr>
								<td class="align-middle">
									<?php 
									if(!$row['photo']){
										?>
										<div class="rounded-avatar" style="width: 100px; height: 100px;">
											<img src="<?= base_url('component/image/profile_photo/avatar.png'); ?>">
										</div>
										<?php
									}else{
										?>
										<div class="rounded-pp" style="width: 100px; height: 100px;">
											<img src="<?= base_url('component/image/profile_photo/' . $row['photo']); ?>">
										</div>
										<?php
									}
									?>
								</td>
								<td class="align-middle"><?= $row['name']; ?></td>
								<td class="align-middle"><?= $row['address']; ?></td>
								<td class="align-middle"><?= $row['package']; ?></td>
								<td class="align-middle"><?= tgl_indo(date('Y-m-d',strtotime($row['created_at']))); ?></td>
							</tr>
						<?php } ?>


					</table>
				</div>
			</div>


		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>