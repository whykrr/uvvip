<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<a href="<?= base_url('classroom/room'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
<h4>UPLOAD VIDEO TRADING</h4>

<div class="col-lg-12 py-1">
	<?php
	if($vt){
		?>
		<center>
			<video controls class="img-thumbnail w-75">
				<source src="<?= base_url('component/videos/video_trading/' . $vt['attachment']); ?>"></source>
			</video>
		</center>
		<?php
	}else{
		?>
		<form class="form-video" method="POST" enctype="multipart/form-data">
			<div class="row">
				<div class="col-lg-10">	
					<div class="form-group">
						<label>Unggah Video</label>
						<input type="file" name="video" class="form-control" id="customFile" onchange="openFile($(this).val());">

					</div>

				</div>
				<input type="hidden" name="id_class" value="<?php if(isset($id_class)){ echo $id_class; } ?>">
				<div class="col-lg-2"><button type="submit" class="btn btn-primary btn-block" style="margin-top: 30px;">PUBLISH</button></div>
			</div>
		</form>
		<?php
	}
	?>
</div>

<script type="text/javascript">
	
	function openFile(file) {
		var extension = file.substr( (file.lastIndexOf('.') +1) );
		
		if(extension == 'mp4'){
			return true;
		}else if(extension == 'avi'){
			return true;
		}else{
			alert('Formart video harus mp4 atau avi');
		}


	};


	$(".form-video").submit(function(event){
		$('.loading').show();
		event.preventDefault();

		var formData = new FormData(this);

		$.ajax({
			type: "POST",
			url: "<?php echo base_url('classroom/room/create_video'); ?>",
			data:formData,
			processData:false,
			contentType:false,
			cache:false,
			beforeSend: function() {
				$('.loading').show();
			},
			success: function(data) {
				// $('.loading').hide();
				location.reload();
			}
		});

	});
</script>
<?= $this->endSection('content'); ?>