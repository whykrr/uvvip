<div class="card w-100 mb-3 no-border bg-light">
	<div class="card-body">
		<div class="row">
			<div class="col-lg-4 pr-2">
				<a href="<?= base_url('classroom/dashboard'); ?>" style="color: #000;">
					<div class="card card-menu no-border <?= ($submenu_active == 'new_participant') ? 'active-card' : ''; ?>">
						<div class="card-body py-2">
							<div class="d-flex">
								<div class="menu-top">
									<img src="<?= base_url('component/image/asset/today-class.png'); ?> " alt="" style="width: 100%;">
								</div>
								<div class="mr-auto align-self-center" style="padding-left: 20px;">
									<h6>PESERTA BARU</h6>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-4 px-2">
				<a href="<?= base_url('classroom/dashboard/interaksi'); ?>" style="color: #000;">
					<div class="card card-menu no-border <?= ($submenu_active == 'interaction') ? 'active-card' : ''; ?>">
						<div class="card-body py-2">
							<div class="d-flex">
								<div class="menu-top">
									<img src="<?= base_url('component/image/asset/interaksi.png'); ?>" alt="" style="width: 100%;">
								</div>
								<div class="mr-auto align-self-center" style="padding-left: 20px;">
									<h6>INTERAKSI</h6>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="col-lg-4 pl-2">
				<a href="<?= base_url('classroom/dashboard/contest') ?>" style="color: #000;">
					<div class="card card-menu no-border <?= ($submenu_active == 'trading_contest') ? 'active-card' : ''; ?>">
						<div class="card-body py-2">
							<div class="d-flex">
								<div class="menu-top">
									<img src="<?= base_url('component/image/asset/trading-color.png'); ?> " alt="" style="width: 100%;">
								</div>
								<div class="mr-auto align-self-center" style="padding-left: 20px;">
									<h6>TRADING CONTEST</h6>
								</div>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>