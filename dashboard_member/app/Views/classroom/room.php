<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Kelas</h2>
<div class="date-info m-2">
	<?= tgl_indo(date('Y-m-d')); ?>
</div>

<div class="row ml-2 mr-1 mt-2">

	<?= $submenu; ?>

	<div class="col-lg-12">
		<?php foreach ($kelas as $row) {

			if($row['banner'] != ''){
				$photo = base_url('component/image/uploads_photo/'.$row['banner']);
			}else{
				$photo = base_url('component/image/default-content.png');
			}
			?>


			<div class="row p-0 pt-3 pb-1 bg-light">
				<div class="col-lg-12">
					<h3 class="pb-1 text-capitalize"><?= $row['title']; ?></h3>
				</div>
				<div class="col-lg-7">	

					<div id="flex-wrapper" style="width: 100%;">
						<div id="flex-container">
							<div id="flex-inside">
								<?php if($row['status'] == 'Scheduled' || $row['status'] == 'Closed') { ?>
									<div class="flex-item bg-gray">
										<img src="<?= base_url('component/image/asset/treat.png'); ?>" alt="" class="img-horizontal">
										<h6>Treat</h6>
									</div>
									<div class="flex-item bg-gray">
										<img src="<?= base_url('component/image/asset/web.png'); ?>" alt="" class="img-horizontal">
										<h6>Video</h6>
									</div>
									<div class="flex-item bg-gray">

										<img src="<?= base_url('component/image/asset/quiz.png'); ?>" alt="" class="img-horizontal">
										<h6>Quiz</h6>

									</div>

									<?php 
									if($row['package_id'] == 2 || $row['package_id'] == 3){ ?>
										<div class="flex-item bg-gray">

											<img src="<?= base_url('component/image/asset/speed.png'); ?>" alt="" class="img-horizontal">
											<h6>App Record</h6>

										</div>
										<div class="flex-item bg-gray">

											<img src="<?= base_url('component/image/asset/video.png'); ?>" alt="" class="img-horizontal">
											<h6>Upload Video</h6>

										</div>
									<?php } ?>
									<div class="flex-item bg-gray">
										<img src="<?= base_url('component/image/asset/kelas2.png'); ?>" alt="" class="img-horizontal">
										<h6>Zoom</h6>
									</div>
									<?php if($row['package_id'] == 3){ ?>
										<div class="flex-item bg-gray">

											<img src="<?= base_url('component/image/asset/report.png'); ?>" alt="" class="img-horizontal">
											<h6>Analisa</h6>

										</div>
									<?php } ?>
									<div class="flex-item bg-gray">
										<img src="<?= base_url('component/image/asset/market.png'); ?>" alt="" class="img-horizontal">
										<h6>Market</h6>

									</div>
									<?php 
								}else{
									?>
									<div class="flex-item">
										<a href="<?= base_url('classroom/room/treat/'.$row['id']); ?>" class="text-reset">
											<img src="<?= base_url('component/image/asset/treat.png'); ?>" alt="" class="img-horizontal">
											<h6>Treat</h6>
										</a>
									</div>
									<div class="flex-item">
										<a href="<?= base_url('classroom/room/video/'.$row['id']); ?>" class="text-reset">
											<img src="<?= base_url('component/image/asset/web.png'); ?>" alt="" class="img-horizontal">
											<h6>Video</h6>
										</a>
									</div>
									<div class="flex-item ">
										<a href="<?= base_url('classroom/room/quiz/'.$row['id']); ?>" class="text-reset">
											<img src="<?= base_url('component/image/asset/quiz.png'); ?>" alt="" class="img-horizontal">
											<h6>Quiz</h6>
										</a>
									</div>

									<?php if($row['package_id'] == 2 || $row['package_id'] == 3){ ?>
										<div class="flex-item">
											<a href="https://icecreamapps.com/Download-Screen-Recorder/" target="_blank" class="text-reset">
												<img src="<?= base_url('component/image/asset/speed.png'); ?>" alt="" class="img-horizontal">
												<h6>App Record</h6>
											</a>
										</div>
										<div class="flex-item">
											<a href="<?= base_url('classroom/room/upload_video/'.$row['id']); ?>" class="text-reset">
												<img src="<?= base_url('component/image/asset/video.png'); ?>" alt="" class="img-horizontal">
												<h6>Upload Video</h6>
											</a>
										</div>
									<?php } ?>
									<div class="flex-item">
										<a href="<?= ($row['zoom_link']) ? $row['zoom_link'] : '#' ?>" class="text-reset" target="<?= ($row['zoom_link']) ? '_blank' : '' ?>">
											<img src="<?= base_url('component/image/asset/kelas2.png'); ?>" alt="" class="img-horizontal">
											<h6>Zoom</h6>
										</a>
									</div>
									<?php if($row['package_id'] == 3){ ?>
										<div class="flex-item">
											<a href="<?= base_url('classroom/room/analisa/'.$row['id']); ?>" class="text-reset">
												<img src="<?= base_url('component/image/asset/report.png'); ?>" alt="" class="img-horizontal">
												<h6>Analisa</h6>
											</a>
										</div>
									<?php } ?>
									<div class="flex-item">
										<a href="<?= $row['market_link']; ?>" target="_blank" class="text-reset">
											<img src="<?= base_url('component/image/asset/market.png'); ?>" alt="" class="img-horizontal">
											<h6>Market</h6>
										</a>
									</div>

								<?php } ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 pl-0">
					<div class="card h-80">
						<div class="card-body p-2">
							<div class="row mx-0">
								<div class="col-lg-4 text-center pl-0 pr-1">
									<h6 class="mb-1">Kelas</h6>
									<div class="info-class" style="background: #FCAF01;"><?= $row['paket']; ?></div>
								</div>
								<div class="col-lg-4 text-center px-1">
									<h6 class="mb-1">Status Kelas</h6>
									<div class="info-class" style="background: #048894;"><?= $row['status']; ?></div>
								</div>
								<div class="col-lg-4 text-center pl-1 pr-0">
									<h6 class="mb-1">Peserta</h6>
									<div class="info-class" style="background: #FD4B05;"><?= $row['participant']; ?> Orang</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row p-0 pb-3 mb-2 bg-light">
				<div class="col-lg-7">
					<img src="<?= $photo; ?>" class="img-fluid">
				</div>
				<div class="col-lg-5 pl-0">
					<div class="card">
						<div class="card-body text-center p-2">
							<h5 class="mb-1">Kelas di mulai pada</h5>
							<h6><?= tgl_indo($row['start_date']); ?> - <?= tgl_indo($row['end_date']); ?></h6>
							<h5 class="mb-1">Jadwal ZOOM</h5>
							<div class="card bg-light" style="height: 175px; overflow-y: scroll;">
								<div class="card-body p-2">
									<?php
									$tno = 1;
									foreach ($schedule as $item) {
										if($item['class_id']==$row['id']){

											$date = date('Y-m-d', strtotime($item['schedule']));
											$time = date('H:i', strtotime($item['schedule']));
											?>
											<div class="row pl-2">
												<label class="col-sm-12 col-form-label py-0"><?= tgl_indo($date).' Pukul '.$time; ?></label>
											</div>
											<?php
										}
									}
									?>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-body text-center p-2">
							<a href="<?= base_url('classroom/room/chat/'.$row['id']); ?>" class="btn btn-primary btn-block" style="background: #048894;">
								Message 
								<?php
								$tno = 1;
								foreach ($chat as $item) {
									if($item['class_id']==$row['id']){
										?>
										<span class="badge badge-light"><?= $item['unread']; ?></span>
										<?php
									}
								}
								?>
							</a>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?= $this->endSection('content'); ?>