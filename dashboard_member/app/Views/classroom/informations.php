<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Informasi</h2>
<hr>
<div class="container mt-3">
	<?php
	foreach ($list as $data) { 
		$ext = pathinfo($data['banner_img'], PATHINFO_EXTENSION); 
		?>
		<div class="row no-gutters bg-light position-relative">
			<div class="col-md-4 mb-md-0 p-3">
				<?php
				if($ext=='mp4' || $ext =='flv'){
					?>
					<video controls width="100%">
						<source src="<?= base_url('component/image/informations/' . $data['banner_img']); ?>"></source>
					</video>
					<?php
				}else{
					?>
					<img src="<?= base_url('component/image/informations/' . $data['banner_img']); ?>" class="w-100 pull-right">
					<?php
				}
				?>
			</div>
			<div class="col-md-8 position-static p-3">
				<h5 class="mt-0"><?= $data['title']; ?></h5>
				<?php
				$date = date('Y-m-d', strtotime($data['created_at']));
				$time = date('H:i:s', strtotime($data['created_at']));
				?>
				<b>Tanggal Terbit : </b><?= tgl_indo($date); ?> <?= $time; ?>
				<p><?= readMoreHelper($data['content']); ?></p>
				<a href="<?= base_url('classroom/detail_information/' . $data['slug']); ?>" class="stretched-link">Read More</a>
			</div>
		</div>
		<br>
		<?php
	} ?>
</div>
<?= $this->endSection('content'); ?>