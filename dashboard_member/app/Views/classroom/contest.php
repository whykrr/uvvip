<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<style type="text/css">
    .dropdown-custom {
        position: relative;
        display: inline-block;
        width: 13%;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        padding: 12px;
        z-index: 1;
        text-align: left;
        min-width: 125px;
        width: auto;
        border-radius: 6px;
        cursor: pointer;
    }

    .dropdown-custom:hover .dropdown-content {
        display: block;
    }
</style>
<h2 class="m-2">Trading Contest</h2>
<div class="date-info m-2">
    <?= tgl_indo(date('Y-m-d')); ?>
</div>

<div class="col-lg-12">
    <?= $submenu; ?>
    <div id="accordianContest" class="container accordion mt-2 px-0">
        <div class="row mb-3">
            <div class="col-md-6 pr-1">
                <a class="btn btn-info btn-lg btn-block" data-toggle="collapse" data-parent="#accordianContest" href="#sectionContest" aria-expanded="true" aria-controls="sectionContest">All Contest</a>
            </div>
            <div class="col-md-6 pl-1">
                <a class="btn btn-info btn-lg btn-block" data-toggle="collapse" data-parent="#accordianContest" href="#sectionFollowed" aria-expanded="false" aria-controls="sectionFollowed">Following Contest</a>
            </div>
        </div>
        <?php
        $show_all = '';
        $show_followed = '';
        if (session()->get('contest')) {
            if (session()->get('contest') == 'all') {
                $show_all = 'show';
            } else {
                $show_followed = 'show';
            }
        } else {
            $show_all = 'show';
        } ?>
        <div id="sectionContest" class="collapse <?= $show_all ?>" data-parent="#accordianContest">
            <div class="table-responsive">
                <table class="table table-borderless" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <td>
                                <h3>All Contest</h3>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list as $data) {
                        ?>
                            <tr>
                                <td>
                                    <div class="row no-gutters bg-light position-relative">
                                        <div class="col-md-4 mb-md-0 p-3">
                                            <img src="<?= base_url('component/image/uploads_photo/' . $data['banner']); ?>" class="w-100 pull-right">
                                        </div>
                                        <div class="col-md-8 position-static p-3">
                                            <h5 class="mt-0"><?= $data['title']; ?></h5>
                                            <b>Tanggal Mulai Kontes : </b><?= tgl_indo($data['start_date']); ?>
                                            <p><?= readMoreHelper($data['desc']); ?></p>
                                            <a href="<?= base_url('classroom/dashboard/contest_detail/' . $data['slug']); ?>" class="stretched-link">Read More</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="sectionFollowed" class="collapse <?= $show_followed ?>" data-parent="#accordianContest">
            <div class="table-responsive">
                <table class="table table-borderless" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <td>
                                <h3>Following Contest</h3>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($list_followed as $data) {
                        ?>
                            <tr>
                                <td>
                                    <div class="row no-gutters bg-light position-relative">
                                        <div class="col-md-4 mb-md-0 p-3">
                                            <img src="<?= base_url('component/image/uploads_photo/' . $data['banner']); ?>" class="w-100 pull-right">
                                        </div>
                                        <div class="col-md-8 position-static p-3">
                                            <h5 class="mt-0"><?= $data['title']; ?></h5>
                                            <b>Tanggal Mulai Kontes : </b><?= tgl_indo($data['start_date']); ?>
                                            <br>
                                            <b>Status Kontes : </b><?= ($data['status_close'] == 't') ? 'Close' : 'Open'; ?>
                                            <p><?= readMoreHelper($data['desc']); ?></p>
                                            <a href="<?= base_url('classroom/dashboard/contest_detail/' . $data['slug']); ?>" class="stretched-link">Read More</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>