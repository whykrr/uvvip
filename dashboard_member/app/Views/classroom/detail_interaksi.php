<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>

<?php 
if($data['attachment'] != ''){
	$photo = base_url('component/image/uploads_photo/'.$data['attachment']);
}else{
	$photo = base_url('component/image/default-content.png');
}
?>
<div class="m-3">
	<a href="<?= base_url('classroom/dashboard/interaksi'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h2 class="m-2"><?= $data['title']; ?></h2>
<hr>
<div class="container mt-3">
	<div class="row position-relative">
		<div class="col-md-12 pt-3">
			<?php
			$date = date('Y-m-d', strtotime($data['created_at']));
			$time = date('H:i', strtotime($data['created_at']));
			?>
			<h3 class="mb-0"><?= $data['name']; ?> </h3>
			<?= tgl_indo($date); ?> <?= $time; ?> <br>
		</div>
		<div class="col-md-12 px-3 pt-1">
			<p class="mb-0"><?= $data['content']; ?></p>
		</div>
		<div class="col-md-12 p-3">
			<img src="<?= $photo; ?>" class="w-100 mx-auto d-block">
			<div class="row" style="margin-top: 15px;">
				<div class="col-lg-7">
					Disukai oleh <span id="total_like<?= $data['id']; ?>"><?= $data['total_like']; ?></span> orang 
				</div>
				<div class="col-lg-5">
					<span id="total_comment<?= $data['id']; ?>"><?= $data['total_comment']; ?></span> Komentar
				</div>
			</div>	
			<hr>
			<div class="d-flex justify-content-center"> 

				<?php 
				$users = session()->get('users');
				$likes = $komen->ForumLike($data['id'],$users['id']);

				if($likes > 0){
					$text = 'Unlike';
				}else{
					$text = 'Like';
				}
				?>

				<a href="#" class="btn-like btn-suka<?= $data['id']; ?> mr-1" key="<?= $data['id']; ?>"><?= $text; ?></a> | <a href="#" class="btn-koment ml-1" key="<?= $data['id']; ?>">Komentar</a>
			</div>
			<div class="row" id="komentar-panel<?= $data['id']; ?>" style="display: none;">
				<div class="col-lg-12">
					<form class="form-komentar">
						<div class="form-group">
							<label>Komentar</label>
							<textarea class="form-control input-lg" placeholder="Tulis Komentarmu" name="komentar"></textarea>
						</div>
						<input type="hidden" name="id_content" value="<?= $data['id']; ?>">
						<button type="submit" class="btn btn-primary">Kirim</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<h4>Komentar</h4>
	<div class="p-0" id="cont-comment<?= $data['id']; ?>">
		
		<?php 
		$komentar = $komen->ForumComment($data['id'])->getResultArray();

		foreach ($komentar as $kom) {
			?>
			<div class="row mb-0">
				<div class="col-lg-12">
					<div class="card card-comment">
						<div class="card-body p-1">
							<h6 class="mb-0 d-inline"><?= $kom['name']; ?></h6>
							<p class="mb-0 d-inline"><?= $kom['comment']; ?></p>
							<?php 
							$datepb = date('Y-m-d', strtotime($kom['created_at']));
							$timepb = date('H:i', strtotime($kom['created_at']));
							?>
							<p class="card-text"><small class="text-muted"><?= tgl_indo($datepb); ?> <?= $timepb; ?></small></p>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<script type="text/javascript">
	$('.btn-like').click(function(){
		var id_content = $(this).attr('key');

		$.ajax({
			type: 'post',
			url: '<?= base_url('classroom/dashboard/like_content'); ?>',
			data: {id_content:id_content},
			dataType: 'json',
			beforeSend:function(){
				$(".loading").show();
			},
			success: function(data)
			{
				var id_content = data.id_content;
				$('#total_like'+id_content).text(data.total_like);
				$('.btn-suka'+id_content).text(data.text);
				$(".loading").hide();
			},
			error : function() {
				$(".loading").hide();
				show_modal("Network error!");
			}
		});


	});

	$('.btn-koment').click(function(){
		var id_content = $(this).attr('key');

		$('#komentar-panel'+id_content).toggle();
	});


	$('.form-komentar').submit(function(event){
		event.preventDefault();
		form = $(this);
		$.ajax({
			type: 'post',
			url: '<?= base_url('classroom/dashboard/posting_komentar'); ?>',
			data: $(this).serialize(),
			dataType: 'json',
			beforeSend:function(){
				$(".loading").show();
			},
			success: function(data)
			{
				form.find('textarea').val('');
				$(".loading").hide();
				$('#total_comment'+data.id).text(data.total_comment);
				$('#cont-comment'+data.id).html(data.comment);
			},
			error : function() {
				$(".loading").hide();
				show_modal("Network error!");
			}
		});
	});
</script>
<?= $this->endSection('content'); ?>