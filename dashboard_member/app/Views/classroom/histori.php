<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<h2 class="m-2">Kelas</h2>
<div class="date-info m-2">
	<?= tgl_indo(date('Y-m-d')); ?>
</div>

<div class="row ml-2 mr-1 mt-2">
	
	<?= $submenu; ?>

	<div class="col-lg-12">
		<?php foreach ($kelas as $row) {

			if($row['banner'] != ''){
				$photo = base_url('component/image/uploads_photo/'.$row['banner']);
			}else{
				$photo = base_url('component/image/default-content.png');
			}

			$tgl_akhir =  strtotime($row['end_date']);
			 $final = date("Y-m-d", strtotime("+1 month", $tgl_akhir));
			 $now = date("Y-m-d");
			 
			?>

			<div class="row p-0 pt-3 pb-1 bg-light">
				<div class="col-lg-12">
					<h3 class="pb-1 text-capitalize"><?= $row['title']; ?></h3>
				</div>
			</div>
			<div class="row p-0 pb-3 mb-2 bg-light">
				<div class="col-lg-7">
					<img src="<?= $photo; ?>" class="img-fluid">
				</div>
				<div class="col-lg-5 pl-0">
					<div class="card">
						<div class="card-body text-center">
							<h5 class="mb-1">Kelas ini berakhir pada</h5>
							<h6><?= tgl_indo($row['end_date']); ?></h6><br>
							<?php if(strtotime($now) > strtotime($final)){ ?>
								<a href="#" class="btn btn-secondary">Download Review Materi</a>
							<?php }else{  ?>
							<a href="<?= base_url('component/videos/summary/'.$row['attachment']); ?>" class="btn clr-2" download>Download Review Materi</a>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
</div>
<?= $this->endSection('content'); ?>