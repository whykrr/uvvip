<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<style type="text/css">
	.dropdown-custom {
		position: relative;
		display: inline-block;
		width: 13%;
	}

	.dropdown-content {
		display: none;
		position: absolute;
		background-color: #f9f9f9;
		box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
		padding: 12px;
		z-index: 1;
		text-align: left;
		min-width: 125px;
		width: auto;
		border-radius: 6px;
		cursor: pointer;
	}

	.dropdown-custom:hover .dropdown-content {
		display: block;
	}
</style>
<div class="m-2">
	<a href="<?= base_url('classroom/dashboard/interaksi'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h2 class="m-2">Arsip Saya</h2>
<hr>

<div class="col-lg-12">

	<?php foreach ($interaksi as $row) { 

		if($row['attachment'] != ''){
			$photo = base_url('component/image/uploads_photo/'.$row['attachment']);
		}else{
			$photo = base_url('component/image/default-content.png');
		}
		?>

		<div class="row" style="margin-top: 15px;">
			<div class="col-lg-12 mb-2" style="text-align: right;">
				<h5 class="d-inline float-left mb-0"><?= $row['name']; ?></h5><br>
				<?php
				$users = session()->get('users');

				$date = date('Y-m-d', strtotime($row['created_at']));
				$time = date('H:i', strtotime($row['created_at']));
				?>
				<p class="d-inline float-left mb-0"><small class="text-muted"><?= tgl_indo($date); ?> <?= $time; ?></small></p>
			</div>
			<div class="col-lg-6">
				<img src="<?= $photo; ?>" class="img-fluid">
				<div class="row" style="margin-top: 15px;">
					<div class="col-lg-7">
						Disukai oleh <span id="total_like<?= $row['id']; ?>"><?= $row['total_like']; ?></span> orang 
					</div>
					<div class="col-lg-5">
						<span id="total_comment<?= $row['id']; ?>"><?= $row['total_comment']; ?></span> Komentar
					</div>
				</div>
			</div>
			<div class="col-lg-6 pl-0">
				<h4><?= $row['title']; ?></h4>
				<p><?= readMoreHelper($row['content']); ?></p>
				<a href="<?=  base_url('classroom/dashboard/detail_interaksi/'.$row['id']); ?>" style="color: #FCAF01;">Read More</a>
			</div>
			<div class="col-lg-12">
				<hr>
				<div class="d-flex justify-content-center"> 

					<?php 
					$likes = $komen->ForumLike($row['id'],$users['id']);

					if($likes > 0){
						$text = 'Unlike';
					}else{
						$text = 'Like';
					}
					?>

					<a href="#" class="btn-like btn-suka<?= $row['id']; ?> mr-1" key="<?= $row['id']; ?>"><?= $text; ?></a> | <a href="#" class="btn-koment ml-1" key="<?= $row['id']; ?>">Komentar</a>
				</div>
				<div class="row" id="komentar-panel<?= $row['id']; ?>" style="display: none;">
					<div class="col-lg-12">
						<form class="form-komentar">
							<div class="form-group">
								<label>Komentar</label>
								<textarea class="form-control input-lg" placeholder="Tulis Komentarmu" name="komentar"></textarea>
							</div>
							<input type="hidden" name="id_content" value="<?= $row['id']; ?>">
							<button type="submit" class="btn btn-primary">Kirim</button>
						</form>
					</div>
				</div>
				<hr>
				<h4>Komentar</h4>
				<div class="p-0" id="cont-comment<?= $row['id']; ?>" style="max-height: 110px;overflow-y: scroll;overflow-x: hidden;">
					<?php 
					$komentar = $komen->ForumComment($row['id'])->getResultArray();

					foreach ($komentar as $kom) {
						?>
						<div class="row mb-0">
							<div class="col-lg-12">
								<div class="card card-comment">
									<div class="card-body p-1">
										<h6 class="mb-0 d-inline"><?= $kom['name']; ?></h6>
										<p class="mb-0 d-inline"><?= $kom['comment']; ?></p>
										<?php 
										$datepb = date('Y-m-d', strtotime($kom['created_at']));
										$timepb = date('H:i', strtotime($kom['created_at']));
										?>
										<p class="card-text"><small class="text-muted"><?= tgl_indo($datepb); ?> <?= $timepb; ?></small></p>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<hr>
			</div>
		</div>

	<?php } ?>



</div>
<script type="text/javascript">
	$('.btn-like').click(function(){
		var id_content = $(this).attr('key');

		$.ajax({
			type: 'post',
			url: '<?= base_url('classroom/dashboard/like_content'); ?>',
			data: {id_content:id_content},
			dataType: 'json',
			beforeSend:function(){
				$(".loading").show();
			},
			success: function(data)
			{
				var id_content = data.id_content;
				$('#total_like'+id_content).text(data.total_like);
				$('.btn-suka'+id_content).text(data.text);
				$(".loading").hide();
			},
			error : function() {
				$(".loading").hide();
				show_modal("Network error!");
			}
		});


	});


	$('.dropdown-content').click(function(){
		var id_content = $(this).attr('key');

		$.ajax({
			type: 'post',
			url: '<?= base_url('classroom/dashboard/simpan_arsip'); ?>',
			data: {id_content:id_content},
			dataType: 'json',
			beforeSend:function(){
				$(".loading").show();
			},
			success: function(data)
			{
				$(".loading").hide();
				alert(data.msg);
			},
			error : function() {

				$(".loading").hide();
				show_modal("Network error!");
			}
		});
	});





	$('.btn-koment').click(function(){
		var id_content = $(this).attr('key');

		$('#komentar-panel'+id_content).toggle();
	});


	$('.form-komentar').submit(function(event){
		event.preventDefault();
		$.ajax({
			type: 'post',
			url: '<?= base_url('classroom/dashboard/posting_komentar'); ?>',
			data: $(this).serialize(),
			beforeSend:function(){
				$(".loading").show();
			},
			success: function(data)
			{
				$(".loading").hide();
				location.reload();
			},
			error : function() {
				$(".loading").hide();
				show_modal("Network error!");
			}
		});
	});
</script>
<?= $this->endSection('content'); ?>