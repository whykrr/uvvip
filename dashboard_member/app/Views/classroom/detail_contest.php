<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="m-3">
    <a href="<?= base_url('classroom/dashboard/contest'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h2 class="m-2"><?= $data['title']; ?></h2>
<hr>
<div class="container mt-3">
    <div class="row position-relative">
        <div class="col-md-12 pt-3">

            <?php
            if (!empty(session()->getFlashdata('message'))) { ?>
                <div class="alert alert-success">
                    <?php echo session()->getFlashdata('message'); ?>
                </div>
            <?php
            }
            ?>

            <b>Tanggal Mulai Kontes : </b><?= tgl_indo($data['start_date']); ?>
            <br>
            <b>Modal Awal : </b><?= format_rupiah($data['coin_capital']); ?>
        </div>
        <div class="col-md-12 p-3">
            <img src="<?= base_url('component/image/uploads_photo/' . $data['banner']); ?>" class="w-50 mx-auto d-block">
        </div>
        <div class="col-md-12 p-3">
            <p class="mb-1"><?= $data['desc']; ?></p>
        </div>
        <div class="col-md-12 p-3">
            <?php if (!$participant_check) : ?>
                <button type="button" class="btn btn-info btn-lg float-right" onclick="show_modal('contest', 'info', 'Join Contest', 'Apakah anda yakin akan mengikuti kontes ini ?', '<?= base_url('classroom/dashboard/join_contest/' . $data['id']) ?>');">Join Contest</button>
                <?php session()->set('contest', 'all') ?>
            <?php endif; ?>
            <?php if ($participant_check) : ?>
                <b>Link ZOOM : </b><a href="<?= $data['zoom_link'] ?>" target="_blank"><?= $data['zoom_link']; ?></a>
                <?php session()->set('contest', 'following') ?>
            <?php endif; ?>
        </div>
        <hr>
        <?php if ($data['status_close'] == 'f') : ?>
            <div class="col-md-12 p-3">
                <h3>Participant</h3>
                <table class="table table-borderless">
                    <tbody>
                        <?php foreach ($participant as $part) {
                        ?>
                            <tr>
                                <td width="70px">
                                    <?php
                                    if (!$part['user_photo']) {
                                    ?>
                                        <div class="rounded-avatar" style="width: 50px; height: 50px;">
                                            <img src="<?= base_url('component/image/profile_photo/avatar.png'); ?>">
                                        </div>
                                    <?php
                                    } else {
                                    ?>
                                        <div class="rounded-pp" style="width: 50px; height: 50px;">
                                            <img src="<?= base_url('component/image/profile_photo/' . $part['user_photo']); ?>">
                                        </div>
                                    <?php
                                    }
                                    ?>

                                </td>
                                <td class="align-middle">
                                    <h4 class="mb-0"><?= $part['user_name'] ?></h4>
                                </td>
                            </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>
        <?php else : ?>
            <div class="col-md-12 p-3">
                <h3>Participant</h3>
                <hr>
                <table class="table table-borderless">
                    <thead>
                        <tr>
                            <td colspan="2">
                                <h4 class="mb-0">Peserta</h4>
                            </td>
                            <td>
                                <h4 class="mb-0">Hasil</h4>
                            </td>
                            <td>
                                <h4 class="mb-0">Rangking</h4>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $rank = 1; ?>
                        <?php foreach ($participant as $part) {
                        ?>
                            <tr>
                                <td width="70px">
                                    <?php
                                    if (!$part['user_photo']) {
                                    ?>
                                        <div class="rounded-avatar" style="width: 50px; height: 50px;">
                                            <img src="<?= base_url('component/image/profile_photo/avatar.png'); ?>">
                                        </div>
                                    <?php
                                    } else {
                                    ?>
                                        <div class="rounded-pp" style="width: 50px; height: 50px;">
                                            <img src="<?= base_url('component/image/profile_photo/' . $part['user_photo']); ?>">
                                        </div>
                                    <?php
                                    }
                                    ?>

                                </td>
                                <td class="align-middle">
                                    <h5 class="mb-0"><?= $part['user_name'] ?></h5>
                                </td>
                                <td class="align-middle">
                                    <h5 class="mb-0"><?= format_rupiah($part['result']) . " ($part[result_percentage] %)" ?></h5>
                                </td>
                                <td class="align-middle">
                                    <h5 class="mb-0"><?= $rank++ ?></h5>
                                </td>
                            </tr>
                        <?php
                        } ?>
                    </tbody>
                </table>
            </div>
        <?php endif ?>
    </div>
</div>
<?= $this->endSection('content'); ?>