<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<div class="m-0">
	<a href="<?= base_url('classroom/room'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
</div>
<h4>ANALISA & REVIEW KELAS</h4>

<div class="col-lg-12 pt-3">
	<?php
	if($vt){
		?>
		<center>
			<video controls class="mb-2" width="75%" height="300px">
				<source src="<?= base_url('component/videos/video_trading/' . $vt['attachment']); ?>"></source>
			</video>
		</center>
		<?php
	}
	?>
	<div class="card">
		<div class="card-body">
			<div class="row">
				<div class="col-lg-6">
					<center>
						<p>Charater Score</p>
						<p style="margin: 0;font-size: 40px;font-weight: bold;"><?= $analisa['character_score']; ?></p>
					</center>
					<p><?= $analisa['character_review']; ?></p>
				</div>

				<div class="col-lg-6">
					<center>
						<p>Intunition Score</p>
						<p style="margin: 0;font-size: 40px;font-weight: bold;"><?= $analisa['intuition_score']; ?></p>
					</center>
					<p><?= $analisa['intiution_review']; ?></p>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">


</script>
<?= $this->endSection('content'); ?>