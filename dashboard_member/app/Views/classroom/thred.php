<?= $this->extend('layout/administrator') ?>

<?= $this->section('content') ?>
<a href="<?= base_url('classroom/room'); ?>" class="btn btn-danger rounded pull-right d-inline">X</a>
<h4>TREAT KELAS</h4>


<div class="accordion" id="materi" style="height: 75%;">
	<div class="col-lg-12">
		<div id="flex-wrapper" style="width: 100%;">
			<div id="flex-container">
				<div id="flex-inside">
					<?php $no = 1; ?>
					<?php foreach ($treats as $row) {
					?>
						<div class="flex-item">
							<a data-toggle="collapse" href="#materi<?= $no; ?>" role="button" class="text-reset">
								<img src="<?= base_url('component/image/asset/treat.png'); ?>" alt="" class="img-horizontal">
								<h6>Materi <?= $no++; ?></h6>
							</a>
						</div>
					<?php
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12" style="height: 100%;">
		<div class="row h-100">
			<?php $no = 1; ?>
			<?php foreach ($treats as $row) {
			?>
				<div class="col-lg-12 h-100 collapse" id="materi<?= $no++; ?>" data-parent="#materi">
					<embed style="height: 100%;" src="<?= base_url('component/pdf/treats/' . $row['attachment']); ?>" type="application/pdf" width="100%" height="700px" allowfullscreen></embed>
				</div>
			<?php
			}
			?>

		</div>
	</div>
</div>
<?= $this->endSection('content'); ?>