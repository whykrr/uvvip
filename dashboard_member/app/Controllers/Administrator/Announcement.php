<?php  
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\AnnouncementsModel;
class Announcement extends BaseController
{

	public function __construct()
	{
		$this->announMod = new AnnouncementsModel();
		helper('general');
	}
	public function index()
	{
		$data['page'] = "Pengumuman";
		$data['list'] = $this->announMod->orderBy('created_at', 'desc')->findAll();
		return view('administrator/announcement/list_announcement', $data);
	}
	public function add()
	{
		$data['page'] = "Pengumuman";
		$data['action'] = "add";
		$data['validation'] = \Config\Services::validation();
		return view('administrator/announcement/form_announcement', $data);
	}
	public function edit($id)
	{
		$data['page'] = "Pengumuman";
		$data['action'] = "edit";
		$data['validation'] = \Config\Services::validation();
		$data['data'] = $this->announMod->where('id', $id)->first();
		return view('administrator/announcement/form_announcement', $data);
	}
	public function detail($id)
	{
		$data['page'] = "Pengumuman";
		$data['data'] = $this->announMod->where('id', $id)->first();
		return view('administrator/announcement/detail_announcement', $data);
	}

	//--------------------------------------------------------------------

	public function act_add()
	{
		$validation = $this->validate([
			'content' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!',
				]
			],
		]);
		$post = $this->request->getPost();

		if(!$validation) {
			return redirect()->to(base_url('administrator/announcement/add'))->withInput();
		} else{
			$this->announMod->save($post);
			session()->setFlashdata('message', 'Pengumuman telah ditambahkan!');

			return redirect()->to(base_url('administrator/announcement'));
		}
	}	

	public function act_edit()
	{
		$post = $this->request->getPost();

		$validation = $this->validate([
			'content' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!',
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/announcement/add'))->withInput();
		} else{
			$this->announMod->save($post);
			session()->setFlashdata('message', 'Pengumuman telah diubah!');

			return redirect()->to(base_url('administrator/announcement'));
		}
	}	

	public function act_delete($id)
	{
		$this->announMod->delete($id);
		session()->setFlashdata('message', 'Pengumuman telah dihapus');

		return redirect()->to(base_url('administrator/announcement'));
	}	
}	
