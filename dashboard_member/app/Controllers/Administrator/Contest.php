<?php

namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\TradingContestModel;
use App\Models\TradingContestParticipantModel;
use App\Models\UsersModel;
use Cocur\Slugify\Slugify;


class Contest extends BaseController
{
	public function __construct()
	{
		$this->tcMod = new TradingContestModel();
		$this->tcpMod = new TradingContestParticipantModel();
		$this->uMod = new UsersModel();
		$this->slugify = new Slugify();
		helper('general');
	}
	public function index()
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "data";
		$data['list'] = $this->tcMod->where('start_date >', date('Y-m-d'))->orderBy('created_at', 'desc')->findAll();

		return view('administrator/contest/list_contest', $data);
	}
	public function add_contest()
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "data";
		$data['validation'] = \Config\Services::validation();

		return view('administrator/contest/form_contest', $data);
	}
	public function candidate($slug)
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "data";
		$data['contest'] = $this->tcMod->where('slug', $slug)->first();
		$data['list'] = $this->tcpMod->getParticipant($data['contest']['id'])->getResultArray();

		return view('administrator/contest/list_candidate', $data);
	}
	public function edit($slug)
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "data";
		$data['data'] = $this->tcMod->where('slug', $slug)->first();
		$data['validation'] = \Config\Services::validation();

		return view('administrator/contest/edit_contest', $data);
	}
	public function detail($slug)
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "data";
		$data['data'] = $this->tcMod->where('slug', $slug)->first();
		$data['validation'] = \Config\Services::validation();

		return view('administrator/contest/edit_contest', $data);
	}
	public function ongoing()
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "ongoing_contest";
		$data['list'] = $this->tcMod->where('start_date <=', date('Y-m-d'))->where('status_close', 'f')->orderBy('created_at', 'desc')->findAll();

		return view('administrator/contest/list_ongoing', $data);
	}
	public function participant($slug)
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "ongoing_contest";
		$data['contest'] = $this->tcMod->where('slug', $slug)->first();
		$data['list'] = $this->tcpMod->getParticipant($data['contest']['id'])->getResultArray();

		return view('administrator/contest/list_participant', $data);
	}
	public function history()
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "history";
		$data['list'] = $this->tcMod->where('start_date <=', date('Y-m-d'))->where('status_close', 't')->orderBy('updated_at', 'desc')->findAll();

		return view('administrator/contest/list_history', $data);
	}
	public function detail_history($slug)
	{
		$data['page'] = "Kontes Trading";
		$data['sub_page'] = "history";
		$data['contest'] = $this->tcMod->where('slug', $slug)->first();
		$data['list'] = $this->tcpMod->getParticipant($data['contest']['id'])->getResultArray();

		return view('administrator/contest/list_participant_history', $data);
	}

	// ------------------------------------------------------------------------------------------

	public function insert_contest()
	{
		$validation = $this->validate([
			'title' => [
				'rules'  => 'required|is_unique[trading_contest.title]',
				'errors' => [
					'required' => 'Judul Kontes harus diisi!',
					'is_unique' => 'Judul Kontes sudah digunakan!',
				]
			],
			'banner' => [
				'rules'  => 'uploaded[banner]|mime_in[banner,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Gambar Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'desc' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Deskripsi Kontes harus diisi!',
				]
			],

			'start_date' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Tanggal mulai kelas harus diisi!',
				]
			],
			'coin_capital' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Modal Awal Kontes harus diisi!',
				]
			],
		]);
		if (!$validation) {
			$validation = \Config\Services::validation();
			$json['res'] = 'fail';
			$json['validation'] = $validation->getErrors();
		} else {
			$insert = $this->request->getPost();
			$banner = $this->request->getFile('banner');

			$newName = 'contest' . $banner->getRandomName();
			$banner->move('component/image/uploads_photo', $newName);

			$insert['slug'] = $this->slugify->slugify($insert['title']);
			$insert['banner'] = $newName;
			$insert['status_close'] = false;

			$this->tcMod->save($insert);

			session()->setFlashdata('message', 'Kontes telah ditambahkan!');
			$json['res'] = 'success';
			$json['redirect'] = base_url('administrator/contest');
		}
		echo json_encode($json);
	}

	public function update_contest()
	{
		$insert = $this->request->getPost();

		$ruleeTitle = 'required|is_unique[trading_contest.title]';
		if ($insert['old_title'] == $insert['title']) {
			$ruleeTitle = 'required';
		}

		$validation = $this->validate([
			'title' => [
				'rules'  => $ruleeTitle,
				'errors' => [
					'required' => 'Judul Kontes harus diisi!',
					'is_unique' => 'Judul Kontes sudah digunakan!',
				]
			],
			'banner' => [
				'rules'  => 'mime_in[banner,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Gambar Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'desc' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Deskripsi Kontes harus diisi!',
				]
			],

			'start_date' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Tanggal mulai kelas harus diisi!',
				]
			],
			'coin_capital' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Modal Awal Kontes harus diisi!',
				]
			],
		]);
		if (!$validation) {
			$validation = \Config\Services::validation();
			$json['res'] = 'fail';
			$json['validation'] = $validation->getErrors();
		} else {
			$banner = $this->request->getFile('banner');

			if ($banner->getError() != 4) {
				@unlink('component/image/uploads_photo/' . $insert['old_banner']);
				$newName = 'contest' . $banner->getRandomName();
				$banner->move('component/image/uploads_photo', $newName);
				$insert['banner'] = $newName;
			}

			$insert['slug'] = $this->slugify->slugify($insert['title']);

			$update_class = $this->tcMod->save($insert);

			session()->setFlashdata('message', 'Kontes telah diubah!');
			$json['res'] = 'success';
			$json['redirect'] = base_url('administrator/contest');
		}
		echo json_encode($json);
	}
	public function refuse_candidate()
	{
		$post = $this->request->getPost();

		$contest = $this->tcMod->where('id', $post['id_trading'])->first();
		$member = $this->uMod->where('id', $post['id_user'])->first();

		$msg = "Halo $member[name], anda telah ditolak untuk mengikuti kontes $contest[title]. \n";
		$msg .= "Anda dapat mengikuti kontes di lain waktu. \n\n";
		$msg .= 'Salam Trader';
		$this->send_email($member['email'], 'Kontes Trading ' . $contest['title'], $msg);

		$this->tcpMod->delete($post);

		session()->setFlashdata('message', "Peserta dengan nama $member[name] berhasil ditolak !");
		return redirect()->to(base_url("administrator/contest/candidate/$contest[slug]"));
	}
	public function input_result()
	{
		$post = $this->request->getPost();

		$percentage = (($post['result'] / $post['capital']) * 100) - 100;
		$post['result_percentage'] = $percentage;
		$this->tcpMod->save($post);

		session()->setFlashdata('message', "Hasil berhasil di inputkan!");

		$json['res'] = 'success';
		$json['redirect'] = base_url("administrator/contest/participant/$post[slug]");

		echo json_encode($json);
	}
	public function change_zoom()
	{
		$post = $this->request->getPost();

		$this->tcMod->save($post);

		session()->setFlashdata('message', 'Link ZOOM telah diubah!');
		return redirect()->to(base_url('administrator/contest/ongoing'));
	}
	public function close_contest()
	{
		$inputResult = false;
		$post = $this->request->getPost();

		$contest = $this->tcMod->where('id', $post['id'])->first();
		$participant = $this->tcpMod->where('trading_contest_id', $post['id'])->find();

		foreach ($participant as $item) {
			if ($item['result'] == '') {
				$inputResult = true;
			}
		}

		if ($inputResult == false) {
			$update['id'] = $post['id'];
			$update['status_close'] = true;
			$this->tcMod->save($update);

			session()->setFlashdata('message', "Kontes $contest[title] telah ditutup!");
			return redirect()->to(base_url('administrator/contest/history'));
		} else {
			session()->setFlashdata('message_danger', "Kontes $contest[title] gagal ditutup, input semua hasil peserta terlebih dahulu!");
			return redirect()->to(base_url('administrator/contest/ongoing'));
		}
	}
}
