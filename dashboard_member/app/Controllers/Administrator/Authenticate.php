<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\AdministratorModel;

class Authenticate extends BaseController
{
	public function __construct()
	{
		$this->adminModel = new AdministratorModel();
	}

	public function index()
	{
		return view('administrator/authenticate/login');
	}

	//--------------------------------------------------------------------

	public function act_login()
	{
		$post = $this->request->getPost();
		$feedback = $this->adminModel->login($post);
		if($feedback['res']=='success'){
			session()->set('administrator', $feedback['session']);
			unset($feedback['session']);
			if(session()->get('last_page_adm')){
				return redirect()->to(base_url(session()->get('last_page_adm')));
			}else{
				return redirect()->to(base_url('administrator/dashboard'));
			}
		}else{
			session()->setFlashdata('message', $feedback['msg']);
			return redirect()->to(base_url('administrator/login'))->withInput();
		}

	}
	public function logout()
	{
		session()->destroy('administrator');
		return redirect()->to(base_url('administrator/login'));
	}
}
