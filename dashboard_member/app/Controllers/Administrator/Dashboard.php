<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;

class Dashboard extends BaseController
{
	public function index()
	{
		$data['page'] = "Dashboard";
		return view('administrator/dashboard/index', $data);
	}

	//--------------------------------------------------------------------

}
