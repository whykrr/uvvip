<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\TransactionModel;
use App\Models\ConfirmPaymentModel;
use App\Models\StatusPaymentsModel;
use App\Models\ClassMemberModel;
use App\Models\ClassBookedModel;
use App\Models\ClassModel;

class Transaction extends BaseController
{
	public function __construct()
	{
		$this->transMod = new TransactionModel();
		$this->cpMod = new ConfirmPaymentModel();
		$this->spMod = new StatusPaymentsModel();
		$this->cmMod = new ClassMemberModel();
		$this->cbMod = new ClassBookedModel();
		$this->cMod = new ClassModel();
		helper('general');
	}
	public function index()
	{
		$data['page'] = "Pendaftaran";
		$data['sub_page'] = "data";
		$data['list'] = $this->transMod->getTransactionList()->getResultArray();
		return view('administrator/transaction/list_transaction', $data);
	}
	public function detail($code)
	{
		$data['page'] = "Pendaftaran";
		$data['sub_page'] = $this->request->getGet('sub');
		$data['data'] = $this->transMod->getDetailTransaction($code)->getRowArray();
		$data['sp'] = $this->spMod->where('id', $data['data']['payment_status_id'])->first();
		return view('administrator/transaction/detail_transaction', $data);
	}
	public function history_detail($code)
	{
		$data['page'] = "Pendaftaran";
		$data['sub_page'] = "data";
		$data['data'] = $this->transMod->getDetailTransaction($code)->getRowArray();
		$data['sp'] = $this->spMod->where('id', $data['data']['payment_status_id'])->first();
		return view('administrator/transaction/detail_transaction', $data);
	}
	public function list_payment()
	{
		$data['page'] = "Pendaftaran";
		$data['sub_page'] = "list_payment";
		$data['list'] = $this->cpMod->getList()->getResultArray();
		return view('administrator/transaction/list_payment', $data);
	}
	public function confirm_payment()
	{
		$code = $this->request->getGet('code'); 

		$data['page'] = "Pendaftaran";
		$data['sub_page'] = "list_payment";
		$data['data'] = $this->cpMod->getDetail($code)->getRowArray();
		return view('administrator/transaction/detail_payment', $data);
	}
	public function list_history()
	{
		$data['page'] = "Pendaftaran";
		$data['sub_page'] = "history";
		$data['list'] = $this->transMod->getTransactionHistory()->getResultArray();
		return view('administrator/transaction/list_transaction', $data);
	}

	//--------------------------------------------------------------------

	public function repayment()
	{
		$transaction_code = $this->request->getPost('transaction_code');
		$class_id = $this->request->getPost('class_id');
		$user_id = $this->request->getPost('user_id');
		$data = $this->cMod->where('id', $class_id)->first();

		$get_booked = $this->cbMod->where('class_id', $class_id)->findAll();
		if(count($get_booked) >= $data['capacity']){
			session()->setFlashdata('message', 'Tidak bisa melakukan pembayaran ulang, jumlah peserta telah penuh!');
			return redirect()->to(base_url('administrator/transaction/detail/' . $transaction_code));
		}else{
			$data_booked['code'] = $class_id.'-'.$user_id;
			$data_booked['class_id'] = $class_id;
			$data_booked['user_id'] = $user_id;
			$this->cbMod->insertBooking($data_booked);

			// Ubah Paticipant 
			$dataClass['id'] = $class_id;
			$dataClass['participant'] = count($get_booked)+1;
			if($dataClass['participant'] == $data['capacity']){
				$dataClass['status_participant'] = 'Full Booked';
			}
			$this->cMod->save($dataClass);

			$update['id'] = $this->request->getPost('id');
			$update['payment_status_id'] = '1';
			$update['pay_before'] = date('Y-m-d H:i:s', strtotime('+2 Hours'));

			$this->transMod->save($update);

			$this->cpMod->where('transaction_code', $transaction_code)->delete();

			session()->setFlashdata('message', 'Status pembayaran telah diubah!');
			return redirect()->to(base_url('administrator/transaction'));
		}
	}
	public function accept_payment()
	{
		$transaction_code = $this->request->getPost('transaction_code');

		$updateCp['id'] = $this->request->getPost('id');
		$updateCp['status'] = 'Accept';

		$this->cpMod->save($updateCp);
		$this->transMod->updatePS($transaction_code, '3');

		$get_trans = $this->transMod->getDetailTransaction($transaction_code)->getRowArray();

		$classMember['transaction_id'] = $get_trans['id'];
		$classMember['class_id'] = $this->request->getPost('class_id');
		$classMember['user_id'] = $this->request->getPost('user_id');
		$classMember['code'] = $classMember['class_id']. '-' .$classMember['user_id'];

		$this->cmMod->insertMember($classMember);

		session()->setFlashdata('message', 'Konfirmasi pembayaran ' . $transaction_code . ' telah diterima!');
		return redirect()->to(base_url('administrator/transaction/list_payment/'));
	}
	public function reject_payment()
	{
		$transaction_code = $this->request->getPost('transaction_code');

		$updateCp['id'] = $this->request->getPost('id');
		$updateCp['status'] = 'Reject';

		$this->cpMod->save($updateCp);

		$this->transMod->updatePS($transaction_code, '5');

		$code = $this->request->getPost('class_id'). '-' .$this->request->getPost('user_id');
		$this->cbMod->where('code', $code)->delete();

		$get_booked = $this->cbMod->where('class_id', $this->request->getPost('class_id'))->findAll();

		$data = $this->cMod->where('id', $this->request->getPost('class_id'))->first();

		$dataClass['id'] = $this->request->getPost('class_id');
		$dataClass['participant'] = count($get_booked);
		if($dataClass['participant'] < $data['capacity']){
			$dataClass['status_participant'] = 'Open';
		}
		$this->cMod->save($dataClass);

		session()->setFlashdata('message', 'Konfirmasi pembayaran ' . $transaction_code . ' telah ditolak!');
		return redirect()->to(base_url('administrator/transaction/list_payment/'));
	}
}
