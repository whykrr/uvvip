<?php  
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\UsersModel;
use App\Models\TransactionModel;

class Member extends BaseController
{

	public function __construct()
	{
		$this->userMod = new UsersModel();
		$this->transMod = new TransactionModel();
		helper('general');
	}
	public function index()
	{
		$data['page'] = "Member";
		$data['member'] = $this->userMod->orderBy('created_at', 'desc')->findAll();
		return view('administrator/member/list_data', $data);
	}
	public function detail($username)
	{
		$data['page'] = "Member";
		$data['detail'] = $this->userMod->where('username', $username)->first();
		$data['transaction'] = $this->transMod->getTransactionList($data['detail']['id'])->getResultArray();
		return view('administrator/member/detail_data', $data);
	}

	//--------------------------------------------------------------------

	public function suspend()
	{
		$post = $this->request->getPost();
		$this->userMod->save($post);

		session()->setFlashdata('message', 'Akun member telah ditangguhkan!');

		return redirect()->to(base_url('administrator/member/' . $post['username']));
	}	

	public function activate()
	{
		$post = $this->request->getPost();
		$this->userMod->save($post);

		session()->setFlashdata('message', 'Akun member telah diaktifkan kembali!');

		return redirect()->to(base_url('administrator/member/' . $post['username']));
	}	
}	
