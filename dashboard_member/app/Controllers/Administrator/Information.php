<?php  
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\InformationsModel;
use Cocur\Slugify\Slugify;

class Information extends BaseController
{

	public function __construct()
	{
		$this->infoMod = new InformationsModel();
		$this->slugify = new Slugify();
		helper('general');
	}
	public function index()
	{
		$data['page'] = "Informasi";
		$data['list'] = $this->infoMod->orderBy('created_at', 'desc')->findAll();
		return view('administrator/information/list_info', $data);
	}
	public function add()
	{
		$data['page'] = "Informasi";
		$data['action'] = "add";
		$data['validation'] = \Config\Services::validation();
		return view('administrator/information/form_info', $data);
	}
	public function edit($slug)
	{
		$data['page'] = "Informasi";
		$data['action'] = "edit";
		$data['validation'] = \Config\Services::validation();
		$data['data'] = $this->infoMod->where('slug', $slug)->first();
		return view('administrator/information/form_info', $data);
	}
	public function detail($slug)
	{
		$data['page'] = "Informasi";
		$data['data'] = $this->infoMod->where('slug', $slug)->first();
		return view('administrator/information/detail_info', $data);
	}

	//--------------------------------------------------------------------

	public function act_add()
	{
		$validation = $this->validate([
			'title' => [
				'rules'  => 'required|is_unique[offers.title]',
				'errors' => [
					'required' => 'Judul harus diisi!',
					'is_unique' => 'Judul sudah digunakan!',
				]
			],
			'banner_img' => [
				'rules'  => 'uploaded[banner_img]|mime_in[banner_img,image/jpg,image/jpeg,image/png,video/mp4,video/x-flv]',
				'errors' => [
					'uploaded' => 'Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar atau video mp4 dan flv!',
				]
			],
			'content_wysiwig' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);
		$post = $this->request->getPost();
		$banner = $this->request->getFile('banner_img');

		if(!$validation) {
			return redirect()->to(base_url('administrator/information/add'))->withInput();
		} else{
			$newName = $banner->getRandomName();
			$banner->move('component/image/informations', $newName);

			$post['slug'] = $this->slugify->slugify($post['title']);
			$post['banner_img'] = $newName;
			$post['content'] = strip_tags($post['content_wysiwig']);

			$this->infoMod->save($post);
			session()->setFlashdata('message', 'Informasi dengan judul '  . $post['title'] . ' telah ditambahkan!');

			return redirect()->to(base_url('administrator/information'));
		}
	}	

	public function act_edit()
	{
		$post = $this->request->getPost();

		if($post['id']){
			$rules_title = 'required';
		}else{
			$rules_title = 'required|is_unique[offers.title]';
		}

		$validation = $this->validate([
			'title' => [
				'rules'  => $rules_title,
				'errors' => [
					'required' => 'Judul harus diisi!',
					'is_unique' => 'Judul sudah digunakan!',
				]
			],
			'banner_img' => [
				'rules'  => 'mime_in[banner_img,image/jpg,image/jpeg,image/png,video/mp4,video/x-flv]',
				'errors' => [
					'mime_in' => 'Banner harus berupa gambar atau video mp4 dan flv!',
				]
			],
			'content_wysiwig' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/information/add'))->withInput();
		} else{
			$banner = $this->request->getFile('banner_img');
			if($banner->getError() != 4){
				$newName = $banner->getRandomName();
				$banner->move('component/image/informations', $newName);
				$post['banner_img'] = $newName;
			}

			$post['slug'] = $this->slugify->slugify($post['title']);
			$post['content'] = strip_tags($post['content_wysiwig']);

			$this->infoMod->save($post);
			session()->setFlashdata('message', 'Informasi dengan judul '  . $post['title'] . ' telah diubah!');

			return redirect()->to(base_url('administrator/information'));
		}
	}	

	public function act_delete($id)
	{

		$this->infoMod->delete($id);
		session()->setFlashdata('message', 'Informasi telah dihapus');

		return redirect()->to(base_url('administrator/information'));
	}	
}	
