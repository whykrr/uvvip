<?php 
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\ClassMemberModel;
use App\Models\ForumFeedsModel;

class Interaction extends BaseController
{
	public function __construct()
	{
		$this->ClassMemMod = new ClassMemberModel();
		$this->ForumFeedsMod = new ForumFeedsModel();
		helper('general');

	}
	public function index()
	{
		$data['page'] = "Interaksi";
		$data['users'] = session()->get('administrator');
		$data['interaksi'] = $this->ForumFeedsMod->FeedsUsers()->getResultArray();
		$data['komen'] = $this->ForumFeedsMod;
		return view('administrator/interaction/index', $data);
	}
	public function delete_feed($id){
		$this->ForumFeedsMod->delete($id);
		session()->setFlashdata('message', 'Feed Interaksi telah dihapus');

		return redirect()->to(base_url('administrator/interaction'));
	}

	//--------------------------------------------------------------------

}
