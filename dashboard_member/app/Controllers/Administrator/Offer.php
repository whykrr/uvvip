<?php  
namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\OffersModel;
use Cocur\Slugify\Slugify;

class Offer extends BaseController
{

	public function __construct()
	{
		$this->offerMod = new OffersModel();
		$this->slugify = new Slugify();
		helper('general');
	}
	public function index()
	{
		$data['page'] = "Penawaran";
		$data['list'] = $this->offerMod->orderBy('created_at', 'desc')->findAll();
		return view('administrator/offer/list_offer', $data);
	}
	public function add()
	{
		$data['page'] = "Penawaran";
		$data['action'] = "add";
		$data['validation'] = \Config\Services::validation();
		return view('administrator/offer/form_offer', $data);
	}
	public function edit($slug)
	{
		$data['page'] = "Penawaran";
		$data['action'] = "edit";
		$data['validation'] = \Config\Services::validation();
		$data['data'] = $this->offerMod->where('slug', $slug)->first();
		return view('administrator/offer/form_offer', $data);
	}
	public function detail($slug)
	{
		$data['page'] = "Penawaran";
		$data['data'] = $this->offerMod->where('slug', $slug)->first();
		return view('administrator/offer/detail_offer', $data);
	}

	//--------------------------------------------------------------------

	public function act_add()
	{
		$validation = $this->validate([
			'title' => [
				'rules'  => 'required|is_unique[offers.title]',
				'errors' => [
					'required' => 'Judul harus diisi!',
					'is_unique' => 'Judul sudah digunakan!',
				]
			],
			'banner_img' => [
				'rules'  => 'uploaded[banner_img]|mime_in[banner_img,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Gambar Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'content_wysiwig' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);
		$post = $this->request->getPost();
		$banner = $this->request->getFile('banner_img');

		if(!$validation) {
			return redirect()->to(base_url('administrator/offer/add'))->withInput();
		} else{
			$newName = $banner->getRandomName();
			$banner->move('component/image/offers', $newName);

			$post['slug'] = $this->slugify->slugify($post['title']);
			$post['banner_img'] = $newName;
			$post['content'] = strip_tags($post['content_wysiwig']);

			$this->offerMod->save($post);
			session()->setFlashdata('message', 'Penawaran dengan judul '  . $post['title'] . ' telah ditambahkan!');

			return redirect()->to(base_url('administrator/offer'));
		}
	}	

	public function act_edit()
	{
		$post = $this->request->getPost();

		if($post['id']){
			$rules_title = 'required';
		}else{
			$rules_title = 'required|is_unique[offers.title]';
		}

		$validation = $this->validate([
			'title' => [
				'rules'  => $rules_title,
				'errors' => [
					'required' => 'Judul harus diisi!',
					'is_unique' => 'Judul sudah digunakan!',
				]
			],
			'banner_img' => [
				'rules'  => 'mime_in[banner_img,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'content_wysiwig' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!'
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('administrator/offer/add'))->withInput();
		} else{
			$banner = $this->request->getFile('banner_img');
			if($banner->getError() != 4){
				$newName = $banner->getRandomName();
				$banner->move('component/image/offers', $newName);
				$post['banner_img'] = $newName;
			}

			$post['slug'] = $this->slugify->slugify($post['title']);
			$post['content'] = strip_tags($post['content_wysiwig']);

			$this->offerMod->save($post);
			session()->setFlashdata('message', 'Penawaran dengan judul '  . $post['title'] . ' telah diubah!');

			return redirect()->to(base_url('administrator/offer'));
		}
	}	

	public function act_delete($id)
	{
		$this->offerMod->delete($id);
		session()->setFlashdata('message', 'Informasi telah dihapus');

		return redirect()->to(base_url('administrator/information'));
	}	
}	
