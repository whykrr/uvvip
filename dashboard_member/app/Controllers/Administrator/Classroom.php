<?php

namespace App\Controllers\Administrator;

use App\Controllers\BaseController;
use App\Models\ClassModel;
use App\Models\PackageModel;
use App\Models\ClassTreatsModels;
use App\Models\ClassVideoModel;
use App\Models\ClassSummaryModels;
use App\Models\ClassQuizModel;
use App\Models\ClassMemberModel;
use App\Models\ClassAnaliticsModels;
use App\Models\ZoomScheduleModel;
use App\Models\ChattingsModel;

class Classroom extends BaseController
{
	public function __construct()
	{
		$this->cMod = new ClassModel();
		$this->packMod = new PackageModel();
		$this->ctMod = new ClassTreatsModels();
		$this->cvMod = new ClassVideoModel();
		$this->csMod = new ClassSummaryModels();
		$this->cqMod = new ClassQuizModel();
		$this->cmMod = new ClassMemberModel();
		$this->caMod = new ClassAnaliticsModels();
		$this->zsMod = new ZoomScheduleModel();
		$this->ChattingsModel = new ChattingsModel();

		helper('general');
	}
	public function index()
	{
		$users = session()->get('administrator');
		$data['page'] = "Kelas";
		$data['sub_page'] = "room";
		$data['chat'] = $this->ChattingsModel->countUnread('A' . $users['id'])->getResultArray();
		$data['list'] = $this->cMod->getList()->getResultArray();
		return view('administrator/classroom/list_room', $data);
	}
	public function message($class_id)
	{
		$user = session()->get('administrator');
		$user_id = $user['id'];
		$data['users'] = $user;
		$data['read_chat'] = $this->ChattingsModel->getRead('A' . $user_id, $class_id)->getResultArray();
		$data['unread_chat'] = $this->ChattingsModel->getUnread('A' . $user_id, $class_id)->getResultArray();

		$json['chat'] = view('administrator/classroom/message', $data);
		$json['unread'] = count($data['unread_chat']);

		echo json_encode($json);
	}
	public function students($class_id)
	{
		$data['page'] = "Kelas";
		$data['sub_page'] = "room";
		$data['class'] = $this->cMod->where('id', $class_id)->first();
		$data['list'] = $this->cmMod->getListStudents($class_id)->getResultArray();
		return view('administrator/classroom/list_students', $data);
	}

	public function schedule()
	{
		$data['page'] = "Kelas";
		$data['sub_page'] = "schedule";
		$data['list'] = $this->cMod->getListSchedule()->getResultArray();
		return view('administrator/classroom/list_schedule', $data);
	}
	public function add_class()
	{
		$data['page'] = "Kelas";
		$data['sub_page'] = "schedule";
		$data['package'] = $this->packMod->findAll();
		$data['validation'] = \Config\Services::validation();
		return view('administrator/classroom/form_class', $data);
	}
	public function quiz($id)
	{
		$data['page'] = "Kelas";
		$data['sub_page'] = "schedule";
		$data['validation'] = \Config\Services::validation();
		$data['class_id'] = $id;
		return view('administrator/classroom/form_quiz', $data);
	}
	public function edit_class($id)
	{
		$data['page'] = "Kelas";
		$data['sub_page'] = "schedule";
		$data['package'] = $this->packMod->findAll();
		$data['class'] = $this->cMod->where('id', $id)->first();
		$data['zclass'] = $this->zsMod->where('class_id', $id)->orderBy('schedule', 'asc')->findAll();
		$data['tclass'] = $this->ctMod->where('class_id', $id)->findAll();
		$data['vclass'] = $this->cvMod->where('class_id', $id)->findAll();
		$data['sumclass'] = $this->csMod->where('class_id', $id)->first();;
		$data['validation'] = \Config\Services::validation();
		return view('administrator/classroom/edit_class', $data);
	}
	public function detail_class($id)
	{
		$data['page'] = "Kelas";
		$data['sub_page'] = "schedule";
		$data['package'] = $this->packMod->findAll();
		$data['class'] = $this->cMod->where('id', $id)->first();
		$data['zclass'] = $this->zsMod->where('class_id', $id)->orderBy('schedule', 'asc')->findAll();
		$data['tclass'] = $this->ctMod->where('class_id', $id)->findAll();
		$data['vclass'] = $this->cvMod->where('class_id', $id)->findAll();
		$data['sumclass'] = $this->csMod->where('class_id', $id)->first();
		$data['validation'] = \Config\Services::validation();
		return view('administrator/classroom/detail_class', $data);
	}

	//--------------------------------------------------------------------

	public function insert_class()
	{
		$validation = $this->validate([
			'title' => [
				'rules'  => 'required|is_unique[class.title]',
				'errors' => [
					'required' => 'Nama kelas harus diisi!',
					'is_unique' => 'Nama Kelas sudah digunakan!',
				]
			],
			'banner' => [
				'rules'  => 'uploaded[banner]|mime_in[banner,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Gambar Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'desc' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Deskripsi kelas harus diisi!',
				]
			],
			'package_id' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Jenis kelas harus diisi!',
				]
			],
			'capacity' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Kapasitas kelas harus diisi!',
				]
			],
			'start_date' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Tanggal mulai kelas harus diisi!',
				]
			],
			'end_date' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Tanggal akhir kelas harus diisi!',
				]
			],
			'market_link' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Link Market harus diisi!',
				]
			],
			'review_materi' => [
				'rules'  => 'uploaded[review_materi]|mime_in[review_materi,video/mp4,video/x-flv]',
				'errors' => [
					'uploaded' => 'Review materi harus diisi!',
					'mime_in' => 'Review materi harus berupa mp4 atau flv!',
				]
			],
			'zoom' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Jadwal harus diisi!',
				]
			],
			'treat' => [
				'rules'  => 'uploaded[treat]|mime_in[treat,application/pdf]',
				'errors' => [
					'uploaded' => 'Treat harus diisi!',
					'mime_in' => 'Treat harus berupa document pdf!',
				]
			],
			'video' => [
				'rules'  => 'uploaded[video]|mime_in[video,video/mp4,video/x-flv]',
				'errors' => [
					'uploaded' => 'Video harus diisi!',
					'mime_in' => 'Video harus berupa mp4 atau flv!',
				]
			],
		]);
		if (!$validation) {
			$validation = \Config\Services::validation();
			$json['res'] = 'fail';
			$json['validation'] = $validation->getErrors();
		} else {
			$insert = $this->request->getPost();
			$banner = $this->request->getFile('banner');
			$review_materi = $this->request->getFile('review_materi');
			$treat = $this->request->getFileMultiple('treat');
			$video = $this->request->getFileMultiple('video');

			$newName = $banner->getRandomName();
			$banner->move('component/image/uploads_photo', $newName);

			$insert['banner'] = $newName;
			$insert['participant'] = '0';
			$insert['status'] = 'Scheduled';
			$insert['status_participant'] = 'Open';

			$insert_class = $this->cMod->save($insert);

			if ($insert_class) {
				$newNameRM = $review_materi->getRandomName();
				$review_materi->move('component/videos/summary', $newNameRM);

				$class_id = $this->cMod->getInsertID();

				$insertSummary['class_id'] = $class_id;
				$insertSummary['attachment'] = $newNameRM;

				$this->csMod->save($insertSummary);
				foreach ($insert['zoom'] as $zoom) {
					$insertZoom['class_id'] = $class_id;
					$insertZoom['schedule'] = $zoom;
					$this->zsMod->save($insertZoom);
				}
				foreach ($treat as $tdata) {
					$tname = $tdata->getRandomName();

					$insertTreat['class_id'] = $class_id;
					$insertTreat['attachment'] = $tname;
					$this->ctMod->insertFile($insertTreat);

					$tdata->move('component/pdf/treats', $tname);
				}
				foreach ($video as $vdata) {
					$vname = $vdata->getRandomName();

					$insertVideo['class_id'] = $class_id;
					$insertVideo['attachment'] = $vname;
					$this->cvMod->insertFile($insertVideo);

					$vdata->move('component/videos/class_video', $vname);
				}
			}

			session()->setFlashdata('message', 'Kelas telah ditambahkan!');
			$json['res'] = 'success';
			$json['redirect'] = base_url('administrator/classroom/schedule');
		}
		echo json_encode($json);
	}
	public function update_class()
	{
		$insert = $this->request->getPost();

		$titleRules = 'required|is_unique[class.title]';
		$zoomRules = 'required';
		$treatRules = 'uploaded[treat]|mime_in[treat,application/pdf]';
		$videoRules = 'uploaded[video]|mime_in[video,video/mp4,video/x-flv]';

		if ($insert['old_title'] == $insert['title']) {
			$titleRules = 'required';
		}
		if ($insert['zoom_count'] > 0) {
			$zoomRules = 'min_length[0]';
		}
		if ($insert['treat_count'] > 0) {
			$treatRules = 'mime_in[treat,application/pdf]';
		}
		if ($insert['video_count'] > 0) {
			$videoRules = 'mime_in[video,video/mp4,video/x-flv]';
		}

		$validation = $this->validate([
			'title' => [
				'rules'  => $titleRules,
				'errors' => [
					'required' => 'Nama kelas harus diisi!',
					'is_unique' => 'Nama Kelas sudah digunakan!',
				]
			],
			'banner' => [
				'rules'  => 'mime_in[banner,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Gambar Banner harus diisi!',
					'mime_in' => 'Banner harus berupa gambar!',
				]
			],
			'desc' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Deskripsi kelas harus diisi!',
				]
			],
			'package_id' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Jenis kelas harus diisi!',
				]
			],
			'capacity' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Kapasitas kelas harus diisi!',
				]
			],
			'start_date' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Tanggal mulai kelas harus diisi!',
				]
			],
			'end_date' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Tanggal akhir kelas harus diisi!',
				]
			],
			'market_link' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Link Market harus diisi!',
				]
			],
			'review_materi' => [
				'rules'  => 'mime_in[review_materi,video/mp4,video/x-flv]',
				'errors' => [
					'uploaded' => 'Review materi harus diisi!',
					'mime_in' => 'Review materi harus berupa mp4 atau flv!',
				]
			],
			'zoom' => [
				'rules'  => $zoomRules,
				'errors' => [
					'required' => 'Jadwal harus diisi!',
				]
			],
			'treat' => [
				'rules'  => $treatRules,
				'errors' => [
					'uploaded' => 'Treat harus diisi!',
					'mime_in' => 'Treat harus berupa document pdf!',
				]
			],
			'video' => [
				'rules'  => $videoRules,
				'errors' => [
					'uploaded' => 'Video harus diisi!',
					'mime_in' => 'Video harus berupa mp4 atau flv!',
				]
			],
		]);
		if (!$validation) {
			$validation = \Config\Services::validation();
			$json['res'] = 'fail';
			$json['validation'] = $validation->getErrors();
		} else {
			$banner = $this->request->getFile('banner');
			$review_materi = $this->request->getFile('review_materi');
			$treat = $this->request->getFileMultiple('treat');
			$video = $this->request->getFileMultiple('video');

			if ($banner->getError() != 4) {
				$newName = $banner->getRandomName();
				$banner->move('component/image/uploads_photo', $newName);

				$insert['banner'] = $newName;
			}
			$insert['participant'] = '0';
			$insert['status'] = 'Scheduled';
			$insert['status_participant'] = 'Open';

			$insert_class = $this->cMod->save($insert);

			if ($insert_class) {
				$class_id = $insert['id'];

				if ($review_materi->getError() != 4) {
					$this->csMod->where('class_id', $class_id)->delete();

					$newNameRM = $review_materi->getRandomName();
					$review_materi->move('component/videos/summary', $newNameRM);

					$insertSummary['class_id'] = $class_id;
					$insertSummary['attachment'] = $newNameRM;

					$this->csMod->save($insertSummary);
				}

				if (@$insert['zoom']) {
					foreach ($insert['zoom'] as $zoom) {
						if ($zoom != '') {
							$insertZoom['class_id'] = $class_id;
							$insertZoom['schedule'] = $zoom;
							$this->zsMod->save($insertZoom);
						}
					}
				}

				foreach ($treat as $tdata) {
					if ($tdata->getError() != 4) {
						$tname = $tdata->getRandomName();

						$insertTreat['class_id'] = $class_id;
						$insertTreat['attachment'] = $tname;
						$this->ctMod->insertFile($insertTreat);

						$tdata->move('component/pdf/treats', $tname);
					}
				}
				foreach ($video as $vdata) {
					if ($vdata->getError() != 4) {
						$vname = $vdata->getRandomName();

						$insertVideo['class_id'] = $class_id;
						$insertVideo['attachment'] = $vname;
						$this->cvMod->insertFile($insertVideo);

						$vdata->move('component/videos/class_video', $vname);
					}
				}
			}

			session()->setFlashdata('message', 'Kelas telah diubah!');
			$json['res'] = 'success';
			$json['redirect'] = base_url('administrator/classroom/schedule');
		}
		echo json_encode($json);
	}
	public function remove_schedule()
	{
		$this->zsMod->delete($this->request->getPost('id'));

		$json['res'] = 'success';
		echo json_encode($json);
	}
	public function remove_treat()
	{
		$this->ctMod->delete($this->request->getPost('id'));
		unlink('component/pdf/treats/' . $this->request->getPost('file'));

		$json['res'] = 'success';
		echo json_encode($json);
	}
	public function remove_video()
	{
		$this->cvMod->delete($this->request->getPost('id'));
		unlink('component/videos/class_video/' . $this->request->getPost('file'));

		$json['res'] = 'success';
		echo json_encode($json);
	}
	public function data_temp($class_id)
	{
		$json = $this->cqMod->where('class_id', $class_id)->orderBy('id', 'asc')->find();

		echo json_encode($json);
	}
	public function save_temp_quiz()
	{
		$req = $this->request->getPost();
		$this->cqMod->save($req);

		$json['res'] = 'success';
		echo json_encode($json);
	}
	public function delete_temp_quiz()
	{
		$id = $this->request->getPost('id');
		$this->cqMod->delete($id);

		$json['res'] = 'success';
		echo json_encode($json);
	}
	public function save_msg()
	{
		$users = session()->get('administrator');
		$post = $this->request->getPost();

		$data_chat = [
			'id' => $users['id'] . date('YmdHis'),
			'message' => $post['message'],
			'user_id'  => 'A' . $users['id'],
			'name'  => $users['name'],
			'read'  => 'A' . $users['id'] . '|',
			'class_id'  => $post['class_id'],
			'created_at'  => date('Y-m-d H:i:s')
		];

		$this->ChattingsModel->saveChat($data_chat);

		$json['res'] = 'success_callback';
		$json['uri'] = base_url('administrator/classroom/message/' . $post['class_id']);

		echo json_encode($json);
	}
	public function change_zoom()
	{
		$post = $this->request->getPost();

		$this->cMod->save($post);

		session()->setFlashdata('message', 'Link ZOOM telah diubah!');
		return redirect()->to(base_url('administrator/classroom'));
	}
	public function open_class()
	{
		$class_id = $this->request->getPost('class_id');

		$class = $this->cMod->where('id', $class_id)->first();

		$quiz = $this->cqMod->where('class_id', $class_id)->find();

		if (count($quiz) == 0) {
			session()->setFlashdata('message_danger', 'Kelas tidak bisa dibuka, mohon untuk menambahkan soal kuis terlebih dahulu');
			return redirect()->to(base_url('administrator/classroom/schedule'));
		} else {
			if (date('Y-m-d') < $class['start_date']) {
				session()->setFlashdata('message_danger', 'Kelas belum bisa dibuka, buka kelas pada tanggal ' . tgl_indo($class['start_date']) . ' atau lebih!');
				return redirect()->to(base_url('administrator/classroom/schedule'));
			} else {

				$update_class['id'] = $class_id;
				$update_class['status'] = 'Open';
				$this->cMod->save($update_class);

				session()->setFlashdata('message', 'Kelas dengan judul ' . $class['title'] . ' telah dibuka!');
				return redirect()->to(base_url('administrator/classroom'));
			}
		}
	}
	public function close_class()
	{
		$class_id = $this->request->getPost('class_id');

		$class = $this->cMod->where('id', $class_id)->first();

		if (date('Y-m-d') < $class['end_date']) {
			session()->setFlashdata('message_danger', 'Kelas belum bisa ditutup, tutup kelas pada tanggal ' . tgl_indo($class['end_date']) . ' atau lebih!');
			return redirect()->to(base_url('administrator/classroom'));
		} else {
			$update_class['id'] = $class_id;
			$update_class['status'] = 'Close';
			$this->cMod->save($update_class);

			session()->setFlashdata('message', 'Kelas dengan judul ' . $class['title'] . ' telah ditutup!');
			return redirect()->to(base_url('administrator/classroom'));
		}
	}
	public function save_analitics()
	{

		$validation = $this->validate([
			'character_score' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Karakter Skor harus diisi!',
				]
			],
			'intuition_score' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Karakter Skor kelas harus diisi!',
				]
			],
			'character_review' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Karakter Review kelas harus diisi!',
				]
			],
			'intiution_review' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Karakter Review harus diisi!',
				]
			],
		]);
		if (!$validation) {
			$validation = \Config\Services::validation();
			$json['res'] = 'fail';
			$json['validation'] = $validation->getErrors();
		} else {

			$insert = $this->request->getPost();
			$insert_ca = $this->caMod->save($insert);

			session()->setFlashdata('message', 'Analisa telah disimpan!');
			$json['res'] = 'success';
			$json['redirect'] = base_url('administrator/classroom/students/' . $insert['class_id']);
		}
		echo json_encode($json);
	}
}
