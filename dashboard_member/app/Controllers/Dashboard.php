<?php 
namespace App\Controllers;

use App\Models\AnnouncementsModel;
use App\Models\InformationsModel;
use App\Models\TransactionModel;

class Dashboard extends BaseController
{
	public function __construct()
	{
		$this->annMod = new AnnouncementsModel();
		$this->infoMod = new InformationsModel();
		$this->trMod = new TransactionModel();
		helper('general');
	}
	public function index()
	{
		$user = session()->get('users');
		$get_ann = $this->annMod->findAll();
		$get_nmClass = $this->trMod->getAnnNMC(date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'). '-1 day')))->getResultArray();

		$announcement = [];
		foreach ($get_nmClass as $nmc) {
			array_push($announcement, 'Selamat datang dan selamat bergabung '.$nmc['name'].' di kelas '.$nmc['title'].' ');
		}
		foreach ($get_ann as $data) {
			array_push($announcement, $data['content']);
		}
		$data['announcement'] = implode(' | ', $announcement);
		$data['information'] = $this->infoMod->orderBy('created_at', 'desc')->findAll();
		return view('layout/dashboard', $data);
	}

	//--------------------------------------------------------------------

}
