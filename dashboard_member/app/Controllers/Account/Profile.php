<?php 
namespace App\Controllers\Account;

use App\Controllers\BaseController;
use App\Models\UsersModel;

class Profile extends BaseController
{
	public function __construct()
	{
		$this->usersModel = new UsersModel();
	}
	public function index()
	{
		helper('general');

		$data['validation'] = \Config\Services::validation();
		$data['menu'] = 'account';
		$data['menu_active'] = 'profile';
		$data['sf'] = 'disabled';
		$data['edit'] = 'none';
		return view('account/profile', $data);
	}

	public function edit_photo()
	{
		helper('general');

		$data['validation'] = \Config\Services::validation();
		$data['menu_active'] = 'profile';
		$data['menu'] = 'account';
		$data['user'] = session()->get('users');;
		return view('account/edit_photo', $data);
	}

	public function edit_personal()
	{
		helper('general');

		$data['validation'] = \Config\Services::validation();
		$data['menu'] = 'account';
		$data['menu_active'] = 'profile';
		$data['sf'] = '';
		$data['edit'] = 'personal';
		return view('account/profile', $data);
	}

	public function edit_username()
	{
		$data['validation'] = \Config\Services::validation();
		$data['menu'] = 'account';
		$data['menu_active'] = 'profile';
		$data['sf'] = '';
		$data['edit'] = 'security';
		return view('account/profile', $data);
	}
	public function edit_password()
	{
		helper('general');

		$data['validation'] = \Config\Services::validation();
		$data['menu_active'] = 'profile';
		$data['menu'] = 'account';
		return view('account/edit_password', $data);
	}

	//--------------------------------------------------------------------

	public function act_photo()
	{
		$validation = $this->validate([
			'photo' => [
				'rules'  => 'uploaded[photo]|mime_in[photo,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Foto Profil harus diisi!',
					'mime_in' => 'Foto Profil harus berupa gambar!',
				]
			],
		]);
		$photo = $this->request->getFile('photo');

		if(!$validation) {
			return redirect()->to(base_url('account/profile/edit_photo'))->withInput();
		} else{
			$newName = session()->get('users')['username'].'.'.$photo->guessExtension();
			@unlink('component/image/profile_photo/' . $newName);
			$photo->move('component/image/profile_photo', $newName);

			$post['photo'] = $newName;
			$post['id'] = session()->get('users')['id'];

			$this->usersModel->save($post);

			$users_sess = $this->usersModel->find($post['id']);
			unset($users_sess['password']);
			session()->set('users', $users_sess);

			return redirect()->to(base_url('account/profile'));
		}
	}
	public function act_personal()
	{
		$input = $this->request->getPost();
		$users = session()->get('users');

		if($users['email'] == $input['email']){
			$rules_email = 'required';
		}else{
			$rules_email = 'required|is_unique[users.email]';
		}

		if($users['phone'] == $input['phone']){
			$rules_phone = 'required';
		}else{
			$rules_phone = 'required|is_unique[users.phone]';
		}

		$validation = $this->validate([
			'name'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Nama harus diisi!'
				]
			],
			'address'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Alamat harus diisi!'
				]
			],
			'phone'    => [
				'rules'  => $rules_phone,
				'errors' => [
					'required' => 'Nomor telepon harus diisi!',
					'is_unique' => 'Nomor Telepon sudah terdaftar!'
				]
			],
			'email' => [
				'rules'  => $rules_email,
				'errors' => [
					'required' => 'E-Mail harus diisi!',
					'is_unique' => 'E-Mail sudah terpakai',
				]
			],
			'identity_card' => [
				'rules'  => 'mime_in[identity_card,image/jpg,image/jpeg,image/png], max_size[identity_card, 2048]',
				'errors' => [
					'max_size' => 'Kartu Indentitas maksimal 2 Mb!',
					'mime_in' => 'Kartu Indentitas harus berupa gambar!',
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('account/profile/edit_personal'))->withInput()->with('validation', $validation);
		} else {
			$idc = $this->request->getFile('identity_card');
			if($idc->getError() != 4){	
				$newName = session()->get('users')['username'].'.'.$idc->guessExtension();
				@unlink('component/image/identity/' . $newName);
				$idc->move('component/image/identity', $newName);
				$input['identity_card'] = $newName;
			}

			$this->usersModel->update($users['id'], $input);

			$users_sess = $this->usersModel->find($users['id']);
			unset($users_sess['password']);
			session()->set('users', $users_sess);

			return redirect()->to(base_url('account/profile'));
		}	
	}

	public function act_username()
	{
		$input = $this->request->getPost();
		$users = session()->get('users');

		if($users['username'] == $input['username']){
			$rules_uname = 'required|alpha_dash';
		}else{
			$rules_uname = 'required|alpha_dash|is_unique[users.username]';
		}

		$validation = $this->validate([
			'username' => [
				'rules'  => $rules_uname,
				'errors' => [
					'required' => 'Nama Pengguna harus diisi!',
					'is_unique' => 'Nama Pengguna sudah terpakai',
					'alpha_dash' => 'Username hanya boleh terisi huruf, angka, underscore dan strip!',
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('account/profile/edit_username'))->withInput()->with('validation', $validation);
		} else {
			$this->usersModel->update($users['id'], $input);

			$users_sess = $this->usersModel->find($users['id']);
			unset($users_sess['password']);
			session()->set('users', $users_sess);

			return redirect()->to(base_url('account/profile'));
		}	
	}

	public function act_password()
	{
		$input = $this->request->getPost();
		$users = session()->get('users');

		$validation = $this->validate([
			'old_password' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Password lama harus diisi!',
				]
			],
			'password' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Password baru harus diisi!',
				]
			],
			'pass_confirm' => [
				'rules'  => 'required|matches[password]',
				'errors' => [
					'required' => 'Konfirmasi password harus diisi!',
					'matches' => 'Konfirmasi password baru tidak sama!',
				]
			],
		]);


		$get_pass = $this->usersModel->find($users['id']);
		$hash = $get_pass['password'];

		if(password_verify($input['old_password'], $hash)){
			if(!$validation) {
				return redirect()->to(base_url('account/profile/edit_password'))->withInput()->with('validation', $validation);
			}else{
				$input['password'] = password_hash($input['password'], PASSWORD_BCRYPT);
				$this->usersModel->update($users['id'], $input);

				return redirect()->to(base_url('account/profile'));
			}
		}else{
			session()->setFlashdata('message', 'Password lama salah!');
			return redirect()->to(base_url('account/profile/edit_password'))->withInput()->with('validation', $validation);
		}
	}
}
