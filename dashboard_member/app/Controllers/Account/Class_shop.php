<?php

namespace App\Controllers\Account;

use App\Controllers\BaseController;
use App\Models\PackageModel;
use App\Models\ClassModel;
use App\Models\ClassBookedModel;
use App\Models\TransactionModel;
use Iwouldrathercode\SimpleCoupons\Coupon;

class Class_shop extends BaseController
{
	public function __construct()
	{
		$this->packModel = new PackageModel();
		$this->cModel = new ClassModel();
		$this->cbModel = new ClassBookedModel();
		$this->trModel = new TransactionModel();
		helper('general');
	}
	public function index()
	{
		$data['menu'] = 'account';
		$data['list'] = $this->packModel->orderBy('id')->findAll();
		$data['menu_active'] = 'class_shop';
		return view('account/package_list', $data);
	}
	public function class_list()
	{
		$package_id = $this->request->getGet('package');

		$data['menu'] = 'account';
		$data['menu_active'] = 'class_shop';
		$data['list'] = $this->cModel->orderBy('id', 'desc')->where('package_id', $package_id)->where('start_date >', date('Y-m-d'))->where('status', 'Scheduled')->findAll();
		if ($data['list']) {
			$data['empty_class'] = false;
		} else {
			$data['empty_class'] = true;
		}
		return view('account/class_list', $data);
	}
	public function class_detail()
	{
		$package_id = $this->request->getGet('package');
		$class_id = $this->request->getGet('class');

		$data['menu'] = 'account';
		$data['menu_active'] = 'class_shop';
		$data['data'] = $this->cModel->where('id', $class_id)->first();
		$data['package_id'] = $package_id;
		return view('account/class_detail', $data);
	}
	public function purchase_review()
	{
		$getUri = $this->request->getGet();

		$code = new Coupon();

		$data['users'] = session()->get('users');
		$data['validation'] = \Config\Services::validation();
		$data['transaction_code'] = 'REG' . $code->allow(['0'])->deny(['I', 'O'])->limit(16)->generate();
		$data['menu'] = 'account';
		$data['menu_active'] = 'class_shop';
		$data['package'] = $this->packModel->where('id', $getUri['package'])->first();
		$data['class'] = $this->cModel->where('id', $getUri['class'])->first();
		$data['from'] = $getUri['from'];
		return view('account/purchase_review', $data);
	}

	public function act_purchase()
	{
		$data = $this->request->getPost();

		$code_book = $data['class_id'] . '-' . $data['user_id'];
		$check_participant = $this->cbModel->find($code_book);

		if ($check_participant) {
			session()->setFlashdata('message', 'Anda telah terdaftar di kelas ini!');
			return redirect()->to(base_url("account/class_shop/purchase_review?package=$data[package_id]&class=$data[class_id]"));
		} else {
			$get_booked = $this->cbModel->where('class_id', $data['class_id'])->findAll();
			if (count($get_booked) >= $data['capacity']) {
				session()->setFlashdata('message', 'Kelas telah mencapai batas maksimal peserta!');
				return redirect()->to(base_url("account/class_shop/purchase_review?package=$data[package_id]&class=$data[class_id]"));
			} else {

				$data['payment_status_id'] = '1';
				$data['confirm_payment_status_id'] = '1';
				$data['price'] = $data['price'] + random_int(100, 999);
				$data['pay_before'] = date('Y-m-d H:i:s', strtotime('+2 Hours'));
				$this->trModel->insert($data);

				$data_booked['class_id'] = $data['class_id'];
				$data_booked['user_id'] = $data['user_id'];
				$data_booked['code'] = $code_book;
				$this->cbModel->insert($data_booked);

				// Ubah Paticipant 
				$dataClass['id'] = $data['class_id'];
				$dataClass['participant'] = count($get_booked) + 1;
				if ($dataClass['participant'] == $data['capacity']) {
					$dataClass['status_participant'] = 'Full Booked';
				}
				$this->cModel->save($dataClass);

				return redirect()->to(base_url('account/transaction_detail/' . $data['transaction_code']));
			}
		}
	}
}
