<?php 
namespace App\Controllers\Account;

use App\Controllers\BaseController;
use App\Models\OffersModel;

class Offers extends BaseController
{
	public function __construct()
	{
		$this->oModel = new OffersModel();
	}
	public function index()
	{
		helper('general');
		$data['menu'] = 'account';
		$data['list'] = $this->oModel->orderBy('created_at', 'desc')->findAll();
		$data['menu_active'] = 'offers';
		return view('account/offers', $data);
	}
	public function detail($slug)
	{
		helper('general');
		$data['menu'] = 'account';
		$data['data'] = $this->oModel->where('slug', $slug)->first();
		$data['menu_active'] = 'offers';
		if($data['data']){
			return view('account/detail_offer', $data);
		}else{
			return redirect()->to(base_url('account/offers'));
		}
	}
}