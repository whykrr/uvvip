<?php 
namespace App\Controllers\Account;

use App\Controllers\BaseController;
use App\Models\TransactionModel;
use App\Models\ConfirmPaymentModel;
use App\Models\BankModel;
use App\Models\PackageModel;
use App\Models\ClassModel;
use App\Models\ClassBookedModel;
use App\Models\StatusPaymentsModel;

class Transaction extends BaseController
{
	public function __construct()
	{
		$this->transModel = new TransactionModel();
		$this->cpMod = new ConfirmPaymentModel();
		$this->bankMod = new BankModel();
		$this->packageMod = new PackageModel();
		$this->cMod = new ClassModel();
		$this->cbMod = new ClassBookedModel();
		$this->spMod = new StatusPaymentsModel();
	}
	public function index()
	{
		helper('general');
		$users = session()->get('users');
		$this->transModel->update_transTO($users['id']);

		$data['validation'] = \Config\Services::validation();
		$data['menu'] = 'account';
		$data['menu_active'] = 'transaction';
		$data['list'] = $this->transModel->getTransactionList($users['id'])->getResultArray();
		if($data['list']){
			$data['empty_trans'] = false;
		}else{
			$data['empty_trans'] = true;
		}
		return view('account/transaction_list', $data);
	}
	public function history()
	{
		helper('general');
		$users = session()->get('users');
		$this->transModel->update_transTO($users['id']);

		$data['menu'] = 'account';
		$data['menu_active'] = 'transaction';
		$data['list'] = $this->transModel->getTransactionHistory($users['id'])->getResultArray();
		if($data['list']){
			$data['empty_trans'] = false;
		}else{
			$data['empty_trans'] = true;
		}
		return view('account/history_transaction', $data);
	}
	public function detail($code)
	{
		helper('general');

		$users = session()->get('users');
		$this->transModel->update_transTO($users['id']);
		if($this->check_transTO($code) == false){
			return redirect()->to(base_url('account/transaction'));
			die;
		}

		$data['menu'] = 'account';
		$data['menu_active'] = 'transaction';
		$data['data'] = $this->transModel->getDetailTransaction($code)->getRowArray();
		$data['sp'] = $this->spMod->where('id', $data['data']['payment_status_id'])->first();
		return view('account/transaction_detail', $data);
	}
	public function payment_method()
	{
		helper('general');

		$code = $this->request->getGet('code');

		$users = session()->get('users');
		$this->transModel->update_transTO($users['id']);
		if($this->check_transTO($code) == false){
			return redirect()->to(base_url('account/transaction'));
			die;
		}

		$data['menu'] = 'account';
		$data['trans'] = $this->transModel->getDetailTransaction($code)->getRowArray();
		$data['data_bank'] = $this->bankMod->findAll();
		$data['menu_active'] = 'transaction';
		return view('account/payment_method', $data);
	}
	public function payment()
	{
		helper('general');

		$code = $this->request->getGet('code');
		$bank = $this->request->getGet('bank');

		$users = session()->get('users');
		$this->transModel->update_transTO($users['id']);
		if($this->check_transTO($code) == false){
			return redirect()->to(base_url('account/transaction'));
			die;
		}

		$data['menu'] = 'account';
		$data['trans'] = $this->transModel->getDetailTransaction($code)->getRowArray();
		$data['data_bank'] = $this->bankMod->where('id', $bank)->first();
		$data['menu_active'] = 'transaction';
		return view('account/payment', $data);
	}
	public function confirm_payment()
	{
		helper('general');
		$req = $this->request->getGet();

		$data['validation'] = \Config\Services::validation();
		$data['request'] = $req;
		$data['menu'] = 'account';
		$data['data_bank'] = $this->bankMod->findAll();
		$data['data_package'] = $this->packageMod->findAll();
		$data['data_class'] = $this->cMod->select('id, title as name')->findAll();

		if($req){
			$data['trans'] = $this->transModel->getDetailTransaction($req['code'])->getRowArray();
		}
		
		$data['menu_active'] = 'transaction';
		return view('account/confirm_payment', $data);
	}

	public function cancel_transaction()
	{
		$code = $this->request->getPost('transaction_code');
		$user = session()->get('users')['id'];


		$get_tr = $this->transModel->getDetailTransaction($code)->getRowArray();

		$code_book = $get_tr['class_id']. '-' .$user;
		$this->cbMod->where('code', $code_book)->delete();

		$get_booked = $this->cbMod->where('class_id', $get_tr['class_id'])->findAll();

		$data_book = $this->cMod->where('id', $get_tr['class_id'])->first();

		$dataClass['id'] = $get_tr['class_id'];
		$dataClass['participant'] = count($get_booked);
		if($dataClass['participant'] < $data_book['capacity']){
			$dataClass['status_participant'] = 'Open';
		}
		$this->cMod->save($dataClass);

		$this->transModel->updatePS($code, '6');
		
		session()->setFlashdata('message', 'Pendaftaran dengan kode ' . $code . ' telah dibatalakan!');
		return redirect()->to(base_url('account/transaction'));
	}

	public function save_confirm_payment()
	{
		$input = $this->request->getPost();
		$attachment = $this->request->getFile('attachment');
		$users = session()->get('users');

		$validation = $this->validate([
			'transaction_code' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!',
				]
			],
			'bank_id' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!',
				]
			],
			'bank_acc_name' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!',
				]
			],
			'bank_acc' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!',
				]
			],
			'nominal' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!',
				]
			],
			'package_id' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!',
				]
			],
			'class_id' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Form ini harus diisi!',
				]
			],
			'attachment' => [
				'rules'  => 'uploaded[attachment]|mime_in[attachment,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'Bukti Transfer harus diisi!',
					'mime_in' => 'Bukti Transfer harus berupa gambar!',
				]
			],
		]);

		if(!$validation) {
			return redirect()->to(base_url('account/confirm_payment?code='.$input['transaction_code'].'&bank='.$input['bank_id']))->withInput()->with('validation', $validation);
		}else{
			$newName = $input['transaction_code'].'.'.$attachment->guessExtension();
			@unlink('component/image/payment/' . $newName);
			$attachment->move('component/image/payment', $newName);

			$input['user_id'] = $users['id'];
			$input['attachment'] = $newName;
			$input['status'] = 'Waiting';
			$this->cpMod->insert($input);

			$uTrans['id'] = $input['transaction_id'];
			$uTrans['payment_status_id'] = '2';
			$this->transModel->save($uTrans);

			session()->setFlashdata('message', 'Konfirmasi pembayaran dengan kode ' . $input['transaction_code'] . ' telah dikirim!');
			return redirect()->to(base_url('account/transaction'));
		}	
	}
	public function check_transTO($code){
		$check = $this->transModel->getDetailTransaction($code)->getRowArray();
		$feedback = true;
		if($check['payment_status_id'] == '4'){
			session()->setFlashdata('message', 'Pendaftaran dengan kode ' . $code . ' telah dibatalkan otomatis oleh sistem!');
			$feedback = false;
		}
		return $feedback;
	}
}