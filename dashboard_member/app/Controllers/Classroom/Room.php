<?php

namespace App\Controllers\Classroom;

use App\Controllers\BaseController;
use App\Models\UsersModel;
use App\Models\ClassMemberModel;
use App\Models\ForumFeedsModel;
use App\Models\ClassModel;
use App\Models\ClassVideoTradingModel;
use App\Models\ClassQuizModel;
use App\Models\UserClassQuizModel;
use App\Models\ClassVideoModel;
use App\Models\ClassTreatsModels;
use App\Models\ClassAnaliticsModels;
use App\Models\ChattingsModel;
use App\Models\ZoomScheduleModel;

class Room extends BaseController
{
	public function __construct()
	{
		$this->ClassMemMod = new ClassMemberModel();
		$this->ForumFeedsMod = new ForumFeedsModel();
		$this->ClassMod = new ClassModel();
		$this->VidTrading = new ClassVideoTradingModel();
		$this->QuizModel = new ClassQuizModel();
		$this->UserQuiz = new UserClassQuizModel();
		$this->ClassVideo = new ClassVideoModel();
		$this->ClassTreats = new ClassTreatsModels();
		$this->ClassAnalitics = new ClassAnaliticsModels();
		$this->ChattingsModel = new ChattingsModel();
		$this->zsMod = new ZoomScheduleModel();

		helper('general');
	}

	public function index()
	{
		$users = session()->get('users');
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['chat'] = $this->ChattingsModel->countUnread('M' . $users['id'])->getResultArray();
		$data['schedule'] = $this->zsMod->getScheduleCLass($users['id'])->getResultArray();
		$data['kelas'] = $this->ClassMod->ClassUser($users['id'])->getResultArray();

		$data_sm['submenu_active'] = 'room';
		$data['submenu'] = view('classroom/menu_room', $data_sm);

		return view('classroom/room', $data);
	}

	public function histori()
	{

		$users = session()->get('users');
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['kelas'] = $this->ClassMod->ClassUserHistori($users['id'])->getResultArray();

		$data_sm['submenu_active'] = 'history';
		$data['submenu'] = view('classroom/menu_room', $data_sm);

		return view('classroom/histori', $data);
	}

	public function upload_video($id_class = '')
	{
		$users = session()->get('users');
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['id_class'] = $id_class;
		$data['vt'] = $this->VidTrading->where('user_id', $users['id'])->where('class_id', $id_class)->first();

		return view('classroom/upload_video', $data);
	}

	public function quiz($id_class = '')
	{
		$users = session()->get('users');
		$score = $this->UserQuiz->where('class_id', $id_class)->where('user_id', $users['id']);
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['id_class'] = $id_class;
		$data['quiz'] = $this->QuizModel->where('class_id', $id_class)->findAll();
		$data['score'] = $score->first();
		$data['scorenya'] = count($this->UserQuiz->where('class_id', $id_class)->where('user_id', $users['id'])->findAll());


		return view('classroom/quiz', $data);
	}

	public function treat($id_class = '')
	{
		$users = session()->get('users');
		$data['treats'] = $this->ClassTreats->where('class_id', $id_class)->findAll();
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['id_class'] = $id_class;

		return view('classroom/thred', $data);
	}

	public function analisa($id_class = '')
	{
		$users = session()->get('users');
		$data['vt'] = $this->VidTrading->where('user_id', $users['id'])->where('class_id', $id_class)->first();
		$data['analisa'] = $this->ClassAnalitics->where('class_id', $id_class)->where('user_id', $users['id'])->first();
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['id_class'] = $id_class;


		return view('classroom/analisa', $data);
	}


	public function materi($id_class = '')
	{
		$users = session()->get('users');
		$data['treats'] = $this->ClassTreats->where('class_id', $id_class)->findAll();
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['id_class'] = $id_class;

		return view('classroom/materi', $data);
	}

	public function video($id_class = '')
	{
		$users = session()->get('users');
		$data['video'] = $this->ClassVideo->where('class_id', $id_class)->findAll();
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['id_class'] = $id_class;

		return view('classroom/video', $data);
	}

	public function chat($class_id)
	{
		$users = session()->get('users');
		$user_id = $users['id'];
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'room';
		$data['users'] = $users;

		$data['class'] = $this->ClassMod->where('id', $class_id)->first();
		$data['read_chat'] = $this->ChattingsModel->getRead('M' . $user_id, $class_id)->getResultArray();
		$data['unread_chat'] = $this->ChattingsModel->getUnread('M' . $user_id, $class_id)->getResultArray();

		$data_sm['submenu_active'] = 'message';
		$data['submenu'] = view('classroom/menu_room', $data_sm);

		return view('classroom/chat', $data);
	}
	public function save_chat()
	{
		if ($post = $this->request->getVar()) {
			$users = session()->get('users');
			$data_chat = [
				'id' => $users['id'] . date('YmdHis'),
				'message' => $post['message'],
				'user_id'  => 'M' . $users['id'],
				'name'  => $users['name'],
				'read'  => 'M' . $users['id'] . '|',
				'class_id'  => $post['class_id'],
				'created_at'  => date('Y-m-d H:i:s')
			];

			$db = \Config\Database::connect();
			$db->table('chattings')->insert($data_chat);
		}
	}

	public function send_answer()
	{
		$post = $this->request->getVar();
		$users = session()->get('users');
		$quiz = $this->QuizModel->where('class_id', $post['id_class'])->findAll();

		$answer = array();
		$total = 0;
		$true = 0;
		foreach ($quiz as $row) {
			$id = $row['id'];
			$answer[] =  $post['answer' . $id];

			if ($post['answer' . $id] == $row['key']) {
				$true += 1;
			}

			$total++;
		}

		$hasil = (100 / (int)$total) * (int)$true;


		$data['score'] =  ceil((int)$hasil);
		$data['answers'] =  implode("|", $answer);
		$data['user_id'] = $users['id'];
		$data['class_id'] = $post['id_class'];

		$this->UserQuiz->save($data);

		return redirect()->to('/classroom/room/quiz/' . $post['id_class']);
	}

	public function create_video()
	{
		$users = session()->get('users');

		$photo = $this->request->getFile('video');
		$nama_photo = $photo->getRandomName();
		$photo->move('component/videos/video_trading', $nama_photo);

		$post = $this->request->getPost();
		$this->VidTrading->save([
			'class_id' => $post['id_class'],
			'user_id' => $users['id'],
			'attachment' => $nama_photo,
		]);
	}
}
