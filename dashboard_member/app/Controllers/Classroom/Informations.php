<?php 
namespace App\Controllers\Classroom;

use App\Controllers\BaseController;
use App\Models\InformationsModel;

class Informations extends BaseController
{
	public function __construct()
	{
		$this->oModel = new InformationsModel();
	}
	public function index()
	{
		helper('general');
		$data['menu'] = 'classroom';
		$data['list'] = $this->oModel->orderBy('created_at', 'desc')->findAll();
		$data['menu_active'] = 'informations';
		return view('classroom/informations', $data);
	}
	public function detail($slug)
	{
		helper('general');
		$data['menu'] = 'classroom';
		$data['data'] = $this->oModel->where('slug', $slug)->first();
		$data['menu_active'] = 'informations';
		if($data['data']){
			return view('classroom/detail_information', $data);
		}else{
			return redirect()->to(base_url('account/informations'));
		}
	}
}