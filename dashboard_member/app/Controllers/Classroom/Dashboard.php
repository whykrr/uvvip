<?php

namespace App\Controllers\Classroom;

use App\Controllers\BaseController;
use App\Models\UsersModel;
use App\Models\ClassMemberModel;
use App\Models\ForumFeedsModel;
use App\Models\TradingContestModel;
use App\Models\TradingContestParticipantModel;

class Dashboard extends BaseController
{
	public function __construct()
	{
		$this->ClassMemMod = new ClassMemberModel();
		$this->ForumFeedsMod = new ForumFeedsModel();
		$this->tcMod = new TradingContestModel();
		$this->tcpMod = new TradingContestParticipantModel();
		helper('general');
	}
	public function index()
	{
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'dashboard';
		$data['member'] = $this->ClassMemMod->ClassMemberUser()->getResultArray();

		$data_sm['submenu_active'] = 'new_participant';
		$data['submenu'] = view('classroom/menu_dashboard', $data_sm);

		return view('classroom/dashboard', $data);
	}
	public function interaksi()
	{
		$data['users'] = session()->get('users');
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'dashboard';
		$data['interaksi'] = $this->ForumFeedsMod->FeedsUsers()->getResultArray();
		$data['komen'] = $this->ForumFeedsMod;
		$data['validation'] = \Config\Services::validation();

		$data_sm['submenu_active'] = 'interaction';
		$data['submenu'] = view('classroom/menu_dashboard', $data_sm);

		return view('classroom/interaksi', $data);
	}
	public function cari_interaksi()
	{
		$member = '';
		if ($this->request->getVar('member')) {
			$member =  $this->request->getVar('member');
		}

		$data['menu'] = 'classroom';
		$data['menu_active'] = 'dashboard';
		$data['interaksi'] = $this->ForumFeedsMod->carifeed($member)->getResultArray();
		$data['komen'] = $this->ForumFeedsMod;
		$data['validation'] = \Config\Services::validation();

		return view('classroom/cari_interaksi', $data);
	}
	public function myarsip()
	{
		$users = session()->get('users');
		$user_id = $users['id'];

		$data['menu'] = 'classroom';
		$data['menu_active'] = 'dashboard';
		$data['interaksi'] = $this->ForumFeedsMod->myarsip($user_id)->getResultArray();
		$data['komen'] = $this->ForumFeedsMod;
		$data['validation'] = \Config\Services::validation();

		return view('classroom/myarsip', $data);
	}
	public function contest()
	{
		$data['users'] = session()->get('users');
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'dashboard';
		$data['list'] = $this->tcMod->where('status_close', 'f')->orderBy('start_date', 'asc')->findAll();
		$data['list_followed'] = $this->tcMod->getFollowed($data['users']['id'])->getResultArray();

		$data_sm['submenu_active'] = 'trading_contest';
		$data['submenu'] = view('classroom/menu_dashboard', $data_sm);

		return view('classroom/contest', $data);
	}
	public function contest_detail($slug)
	{
		$data['users'] = session()->get('users');
		$data['menu'] = 'classroom';
		$data['menu_active'] = 'dashboard';
		$data['data'] = $this->tcMod->where('slug', $slug)->first();
		$data['participant'] = $this->tcpMod->getParticipant($data['data']['id'])->getResultArray();
		$data['participant_check'] = $this->tcpMod->where('id', $data['data']['id'] . $data['users']['id'])->first();

		$data_sm['submenu_active'] = 'trading_contest';
		$data['submenu'] = view('classroom/menu_dashboard', $data_sm);

		return view('classroom/detail_contest', $data);
	}

	public function create_interaksi()
	{

		$validation = $this->validate([
			'title' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Judul harus diisi!',
				]
			],
			'content' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Konten harus diisi!',
				]
			],
			'photo' => [
				'rules'  => 'uploaded[photo]|mime_in[photo,image/jpg,image/jpeg,image/png]',
				'errors' => [
					'uploaded' => 'File harus diisi!',
					'mime_in' => 'File harus berupa gambar!',
				]
			],
		]);

		if (!$validation) {
			return redirect()->to(base_url('classroom/dashboard/interaksi'))->withInput();
			die;
		}

		$users = session()->get('users');
		$newName = '';
		$photo = $this->request->getFile('photo');
		if ($photo->getError() != 4) {

			$photo->move('component/image/uploads_photo');
			$newName = $photo->getName();
		}



		$post = $this->request->getVar();
		$this->ForumFeedsMod->save([
			'title' => $post['title'],
			'content' => $post['content'],
			'user_id' => $users['id'],
			'total_like' => 0,
			'total_comment' => 0,
			'attachment' => $newName
		]);

		return redirect()->to('/classroom/dashboard/interaksi');
	}

	public function like_content()
	{

		$users = session()->get('users');
		$id_content = $this->request->getVar('id_content');
		$likes = $this->ForumFeedsMod->ForumLike($id_content, $users['id']);

		if ($likes > 0) {
			if ($post = $this->request->getVar()) {
				$users = session()->get('users');

				$db = \Config\Database::connect();
				$db->table('forum_likes')->delete(['forum_feed_id' => $post['id_content'], 'user_id' => $users['id']]);

				$id_content = $post['id_content'];

				$feed = $this->ForumFeedsMod->where(['id' => $id_content])->first();
				$upp['total_like'] =  $feed['total_like'] - 1;

				$builder = $db->table('forum_feeds');
				$builder->where('id', $id_content);
				$builder->update($upp);

				$json['total_like'] = $feed['total_like'] - 1;
				$json['id_content'] = $post['id_content'];
				$json['text'] = 'Like';

				return json_encode($json);
			}
		} else {
			if ($post = $this->request->getVar()) {
				$users = session()->get('users');
				$data = [
					'forum_feed_id' => $post['id_content'],
					'user_id'  => $users['id']
				];

				$db = \Config\Database::connect();
				$db->table('forum_likes')->insert($data);

				$id_content = $post['id_content'];

				$feed = $this->ForumFeedsMod->where(['id' => $id_content])->first();
				$upp['total_like'] =  $feed['total_like'] + 1;

				$builder = $db->table('forum_feeds');
				$builder->where('id', $id_content);
				$builder->update($upp);

				$json['total_like'] = $feed['total_like'] + 1;
				$json['id_content'] = $post['id_content'];
				$json['text'] = 'Unlike';

				return json_encode($json);
			}
		}
	}

	public function simpan_arsip()
	{
		if ($post = $this->request->getVar()) {
			$users = session()->get('users');
			$id_content = $post['id_content'];
			$user_id = $users['id'];

			$db = \Config\Database::connect();
			$feed = $db->table('forum_archives')->where(['forum_feed_id' => $id_content, 'user_id' => $user_id])->countAllResults();

			if ($feed > 0) {
				$json['msg'] = "Feed sudah ada";
				$json['status'] = 2;
				echo json_encode($json);
			} else {
				$data = [
					'id' => $post['id_content'] . $users['id'],
					'forum_feed_id' => $post['id_content'],
					'user_id'  => $users['id'],
					'created_at' => date('Y-m-d H:i:s'),
					'updated_at' => date('Y-m-d H:i:s')
				];

				$db = \Config\Database::connect();
				$db->table('forum_archives')->insert($data);

				$json['msg'] = "Feed sudah tersimpan di arsip";
				$json['status'] = 1;
				echo json_encode($json);
			}
		}
	}

	public function detail_interaksi($id = '')
	{
		helper('general');
		$data['menu'] = 'classroom';
		$data['data'] = $this->ForumFeedsMod->FeedsUsersWhere($id)->getRowArray();
		$data['komen'] = $this->ForumFeedsMod;
		$data['menu_active'] = 'Interaksi';
		if ($data['data']) {
			return view('classroom/detail_interaksi', $data);
		} else {
			return redirect()->to(base_url('classroom/dashboard/interaksi'));
		}
	}


	public function hapus_feed()
	{
		if ($post = $this->request->getVar()) {
			$id_content = $post['id_content'];
			$users = session()->get('users');
			$user_id = $users['id'];
			$this->ForumFeedsMod->where(['id' => $id_content, 'user_id' => $user_id])->delete();
		}
	}


	public function posting_komentar()
	{

		if ($post = $this->request->getVar()) {
			$users = session()->get('users');
			$data = [
				'comment' => $post['komentar'],
				'forum_feed_id' => $post['id_content'],
				'user_id'  => $users['id'],
				'created_at'  => date('Y-m-d H:i:s')
			];

			$db = \Config\Database::connect();
			$db->table('forum_comments')->insert($data);

			$id_content = $post['id_content'];

			$feed = $this->ForumFeedsMod->where(['id' => $id_content])->first();
			$upp['total_comment'] =  $feed['total_comment'] + 1;

			$builder = $db->table('forum_feeds');
			$builder->where('id', $id_content);
			$builder->update($upp);

			$d_comment['id'] = $post['id_content'];
			$d_comment['komen'] = $this->ForumFeedsMod;
			$json['comment'] = view('classroom/comment', $d_comment);
			$json['total_comment'] = $upp['total_comment'];
			$json['id'] = $post['id_content'];

			echo json_encode($json);
		}
	}

	public function join_contest($contest_id)
	{
		$user = session()->get('users');

		$insert['id'] = $contest_id . $user['id'];
		$insert['trading_contest_id'] = $contest_id;
		$insert['user_id'] = $user['id'];
		$insert['submision_acc'] = 't';

		$this->tcpMod->insertParticipant($insert);

		$contest = $this->tcMod->where('id', $contest_id)->first();

		session()->setFlashdata('message', 'Selamat, anda telah terdaftar di kontes ini!');
		return redirect()->to(base_url('classroom/dashboard/contest_detail/' . $contest['slug']));
	}

	//--------------------------------------------------------------------

}
