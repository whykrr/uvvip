<?php

namespace App\Controllers;

use App\Models\UsersModel;
use Iwouldrathercode\SimpleCoupons\Coupon;
use CodeIgniter\RESTful\ResourceController;

class Authenticate extends BaseController
{
	protected $format = 'json';

	public function __construct()
	{
		$this->usersModel = new UsersModel();
	}
	public function index()
	{
		return redirect()->to(base_url('authenticate/login'));
	}
	public function login()
	{
		return view('authenticate/login');
	}
	public function forget_password()
	{
		return view('authenticate/forget_password');
	}
	public function register()
	{
		return view('authenticate/register');
	}

	public function act_login()
	{
		$post = $this->request->getPost();
		$feedback = $this->usersModel->login($post);
		if ($feedback['res'] == 'success') {
			session()->set('users', $feedback['session']);
			unset($feedback['session']);
		}
		echo json_encode($feedback);
	}
	public function act_register()
	{

		$validation = $this->validate([
			'email' => [
				'rules'  => 'required|is_unique[users.email]|valid_email',
				'errors' => [
					'required' => 'Email harus diisi!',
					'is_unique' => 'Email sudah terdaftar!',
					'valid_email' => 'Email tidak valid!',
				]
			],
			'phone' => [
				'rules'  => 'required|is_unique[users.phone]',
				'errors' => [
					'required' => 'Nomor Telepon harus diisi!',
					'is_unique' => 'Nomor Telepon sudah terdaftar!',
				]
			],
			'name'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Nama Lengkap harus diisi!'
				]
			],
			'username' => [
				'rules'  => 'required|alpha_dash|is_unique[users.username]',
				'errors' => [
					'required' => 'Username harus diisi!',
					'is_unique' => 'Username sudah terpakai!',
					'alpha_dash' => 'Username hanya boleh terisi huruf, angka, underscore dan strip!',
				]
			],
			'password'    => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Password harus diisi!'
				]
			],
		]);

		$post = $this->request->getPost();
		$feedback = [];

		if (!$validation) {
			$data_validation = \Config\Services::validation();

			$errors = $data_validation->getErrors();
			$feedback['res'] = 'fail';
			$feedback['msg'] = implode('<br>', $errors);
		} else {
			$login = [];
			$login['username'] = $post['username'];
			$login['password'] = $post['password'];

			$code = new Coupon();

			$msg = 'Halo ' . $post['name'] . ', Selamat datang di United VIP Vision.' . "\n\n";
			$msg .= 'Gunakan akun baru anda untuk masuk ke dashboard http://dashboard.unitedvipvision.id/login' . "\n";
			$msg .= 'Lengkapi data pribadi anda terlebih dahulu di http://dashboard.unitedvipvision.id/account/profile sebelum melakukan pendaftaran kelas' . "\n";
			$msg .= 'Anda dapat mendaftarkan kelas kami di http://dashboard.unitedvipvision.id/account/class_shop' . "\n\n";
			$msg .= 'Salam Trader';
			$this->send_email($post['email'], 'Pendaftaran Akun', $msg);

			$post['referal_code'] = $code->allow(['0'])->deny(['I', 'O'])->limit(12)->generate();
			$post['password'] = password_hash($post['password'], PASSWORD_BCRYPT);
			$post['status'] = 'Active';
			$this->usersModel->save($post);

			$feedback = $this->usersModel->login($login);
			if ($feedback['res'] == 'success') {
				session()->set('users', $feedback['session']);
				unset($feedback['session']);
			}
		}
		echo json_encode($feedback);
	}
	public function act_fpassword()
	{
		$validation = $this->validate([
			'email' => [
				'rules'  => 'required|valid_email',
				'errors' => [
					'required' => 'Email harus diisi!',
					'valid_email' => 'Email tidak valid!',
				]
			],
			'phone' => [
				'rules'  => 'required',
				'errors' => [
					'required' => 'Nomor Telepon harus diisi!',
				]
			],
		]);
		if (!$validation) {
			$data_validation = \Config\Services::validation();

			$errors = $data_validation->getErrors();
			$feedback['res'] = 'fail';
			$feedback['msg'] = implode('<br>', $errors);
		} else {
			$post = $this->request->getPost();

			$cek = $this->usersModel->where('email', $post['email'])->where('phone', $post['phone'])->first();
			if (!$cek) {
				$feedback['res'] = 'fail';
				$feedback['msg'] = 'Kombinasi email dan nomor telepon yang anda masukkan tidak terdaftar!';
			} else {
				$code = new Coupon();
				$newPassword = $code->allow(['0'])->deny(['I', 'O'])->limit(6)->generate();

				$update_password['id'] = $cek['id'];
				$update_password['password'] = password_hash($newPassword, PASSWORD_BCRYPT);

				$this->usersModel->save($update_password);


				$msg = 'Halo ' . $cek['name'] . ', password anda telah diperbaharui.' . "\n\n";
				$msg .= 'Password baru anda : ' . $newPassword . "\n";
				$msg .= 'Gunakan password baru anda untuk masuk ke http://dashboard.unitedvipvision.id/login' . "\n\n";
				$msg .= 'Note : Disarankan untuk langsung mengganti password pada saat berhasil mendapatkan akun anda kembali!';
				$this->send_email($cek['email'], 'Permintaan Reset Password', $msg);

				$feedback['res'] = 'success_msg';
				$feedback['msg'] = 'Password baru anda telah kami kirim silahkan cek kotak masuk atau spam email anda.';
			}
		}
		echo json_encode($feedback);
	}
	public function logout()
	{
		session()->destroy('users');
		return redirect()->to(base_url('login'));
	}

	//--------------------------------------------------------------------

}
