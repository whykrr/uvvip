<?php

namespace App\Models;

use CodeIgniter\Model;

class ChattingsModel extends Model
{
	protected $table      = 'chattings';
	protected $primaryKey = 'id';

	protected $allowedFields = ['user_id', 'name', 'message', 'read', 'class_id', 'created_at'];

	public function saveChat($dataa)
	{

		$db = \Config\Database::connect();
		$db->table('chattings')->insert($dataa);
	}

	public function getRead($user, $class_id)
	{
		$query = $this->db->table($this->table);

		$query->select('*');
		$query->like('read', $user . '|', 'both');
		$query->orderBy('created_at', 'asc');
		$query->where('class_id', $class_id);

		return $query->get();
	}
	public function getUnread($user, $class_id)
	{
		$query = $this->db->table($this->table);

		$query->select('*');
		$query->notLike('read', $user . '|');
		$query->orderBy('created_at', 'asc');
		$query->where('class_id', $class_id);
		$unread = $query->get();
		foreach ($unread->getResultArray() as $item) {
			$savemsg['id'] = $item['id'];
			$savemsg['read'] = $item['read'] . $user . '|';
			$this->save($savemsg);
		}

		return $unread;
	}
	public function countUnread($user)
	{
		$query = $this->db->table($this->table);

		$query->select('COUNT(id) as unread');
		$query->select('class_id');
		$query->notLike('read', $user . '|');
		$query->groupBy('class_id');

		return $query->get();
	}
}
