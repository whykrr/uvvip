<?php namespace App\Models;

use CodeIgniter\Model;

class ClassQuizModel extends Model
{
	protected $table      = 'class_quiz';
    protected $primaryKey = 'id';
    protected $allowedFields = ['class_id', 'question', 'option_a', 'option_b', 'option_c', 'option_d', 'key'];
  
}