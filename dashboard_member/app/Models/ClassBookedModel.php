<?php namespace App\Models;

use CodeIgniter\Model;

class ClassBookedModel extends Model
{
	protected $table      = 'class_booked';
    protected $primaryKey = 'code';

    protected $allowedFields = [
    	'class_id',
    	'user_id',
    	'code',
    ];

    public function insertBooking($data){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->insert($data);
    }
}