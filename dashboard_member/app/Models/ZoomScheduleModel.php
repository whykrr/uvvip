<?php namespace App\Models;

use CodeIgniter\Model;

class ZoomScheduleModel extends Model
{
	protected $table      = 'class_zoom_schedule';
	protected $primaryKey = 'id';
	protected $allowedFields = ['class_id', 'schedule'];

	public function getScheduleCLass($user){
		$query = $this->db->table($this->table.' czs');

		$query->select('czs.class_id');
		$query->select('czs.schedule');
		$query->join('class cl', 'cl.id = czs.class_id');
		$query->join('class_member cm', 'cm.class_id = czs.class_id');
		$query->where('cl.status !=', 'Close');
		$query->where('cm.user_id', $user);
		$query->where('czs.schedule >=', date('Y-m-d'));
		$query->orderBy('czs.class_id', 'asc');
		$query->orderBy('czs.schedule', 'asc');

		return $query->get();
	}

}