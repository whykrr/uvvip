<?php namespace App\Models;

use CodeIgniter\Model;
use App\Models\ClassBookedModel;
use App\Models\ClassModel;

class TransactionModel extends Model
{
	protected $table      = 'transactions';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'transaction_code',
    	'user_id',
    	'package_id',
    	'class_id',
    	'price',
        'payment_status_id',
        'pay_before',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';


    public function __construct(){
        $this->cbMod = new ClassBookedModel();
        $this->cMod = new ClassModel();
    }

    public function getTransactionList($id = ''){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->select('transactions.id');
        $builder->select('transactions.transaction_code');
        $builder->select('date(transactions.created_at) as created_at');
        $builder->select('users.name as name');
        $builder->select('packages.name as package_name');
        $builder->select('class.title as class_name');
        $builder->select('status_payments.status as payment_status');
        $builder->select('status_payments.style as ps_style');
        $builder->join('users', 'users.id = transactions.user_id');
        $builder->join('packages', 'packages.id = transactions.package_id');
        $builder->join('class', 'class.id = transactions.class_id');
        $builder->join('status_payments', 'status_payments.id = transactions.payment_status_id');
        $builder->where("(transactions.payment_status_id = 1 OR transactions.payment_status_id = 2)");
        if($id != ''){
            $builder->where('transactions.user_id', $id);
        }
        return $builder->get();
    }
    public function getTransactionHistory($id = ''){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->select('transactions.id');
        $builder->select('transactions.transaction_code');
        $builder->select('date(transactions.created_at) as created_at');
        $builder->select('users.name as name');
        $builder->select('packages.name as package_name');
        $builder->select('class.title as class_name');
        $builder->select('status_payments.status as payment_status');
        $builder->select('status_payments.style as ps_style');
        $builder->join('users', 'users.id = transactions.user_id');
        $builder->join('packages', 'packages.id = transactions.package_id');
        $builder->join('class', 'class.id = transactions.class_id');
        $builder->join('status_payments', 'status_payments.id = transactions.payment_status_id');
        $builder->where("(transactions.payment_status_id != 1 AND transactions.payment_status_id != 2)");
        $builder->orderBy('transactions.created_at', 'desc');
        if($id != ''){
            $builder->where('transactions.user_id', $id);
        }
        return $builder->get();
    }
    public function getDetailTransaction($code){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->select('transactions.id');
        $builder->select('transactions.transaction_code');
        $builder->select('transactions.user_id');
        $builder->select('transactions.created_at as created_at');
        $builder->select('users.name as name');
        $builder->select('packages.name as package_name');
        $builder->select('class.title as class_name');
        $builder->select('transactions.payment_status_id');
        $builder->select('transactions.price');
        $builder->select('transactions.package_id');
        $builder->select('transactions.class_id');
        $builder->select('transactions.pay_before');
        $builder->join('users', 'users.id = transactions.user_id');
        $builder->join('packages', 'packages.id = transactions.package_id');
        $builder->join('class', 'class.id = transactions.class_id');
        $builder->where('transactions.transaction_code', $code);
        $builder->orderBy('transactions.created_at', 'desc');
        return $builder->get();
    }
    public function updatePS($code, $ps){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->set('payment_status_id', $ps);
        $builder->where('transaction_code', $code);
        $builder->update();
    }
    public function getAnnNMC($date){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table.' tr');

        $builder->select('u.name');
        $builder->select('cl.title');
        $builder->join('class cl', 'cl.id = tr.class_id');
        $builder->join('users u', 'u.id = tr.user_id');
        $builder->where('tr.payment_status_id', '3');
        $builder->where('tr.updated_at >=', $date);
        $builder->where('cl.status !=', 'Close');
        return $builder->get();
    }
    public function update_transTO($user){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->set('payment_status_id', '4');
        $builder->where('payment_status_id', '1');
        $builder->where('user_id', $user);
        $builder->where('pay_before <', date('Y-m-d H:i:s'));
        $builder->update();

        $get_TO = $builder->where('user_id', $user)->where('payment_status_id', '4')->get()->getResultArray();

        foreach ($get_TO as $data) {
            $code = $data['class_id']. '-' .$user;
            $this->cbMod->where('code', $code)->delete();

            $get_booked = $this->cbMod->where('class_id', $data['class_id'])->findAll();

            $data_book = $this->cMod->where('id', $data['class_id'])->first();

            $dataClass['id'] = $data['class_id'];
            $dataClass['participant'] = count($get_booked);
            if($dataClass['participant'] < $data_book['capacity']){
                $dataClass['status_participant'] = 'Open';
            }
            $this->cMod->save($dataClass);            
        }
    }
}

?>