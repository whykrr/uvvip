<?php namespace App\Models;

use CodeIgniter\Model;

class ClassTreatsModels extends Model
{
	protected $table      = 'class_treats';
    protected $primaryKey = 'id';
    protected $allowedFields = ['class_id', 'attachment'];
	protected $useTimestamps = true;
	
	public function insertFile($data)
    {
        return $this->db->table($this->table)->insert($data);
    }
}