<?php namespace App\Models;

use CodeIgniter\Model;

class AnnouncementsModel extends Model
{
    protected $table      = 'announcements';
    protected $primaryKey = 'id';

    protected $allowedFields = [
        'content',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}