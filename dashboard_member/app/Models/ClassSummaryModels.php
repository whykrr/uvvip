<?php namespace App\Models;

use CodeIgniter\Model;

class ClassSummaryModels extends Model
{
	protected $table      = 'class_summary';
	protected $primaryKey = 'id';
	protected $allowedFields = ['class_id', 'attachment'];
	protected $useTimestamps = true;
}