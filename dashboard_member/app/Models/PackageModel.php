<?php namespace App\Models;

use CodeIgniter\Model;

class PackageModel extends Model
{
	protected $table      = 'packages';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'name',
    	'desc',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
}