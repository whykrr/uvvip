<?php namespace App\Models;

use CodeIgniter\Model;

class ClassVideoTradingModel extends Model
{
	protected $table      = 'class_video_trading';
	protected $primaryKey = 'id';

	protected $allowedFields = [
		'user_id',
		'class_id',
		'attachment',
	];
	protected $useTimestamps = 'datetime';
	
}