<?php namespace App\Models;

use CodeIgniter\Model;

class ConfirmPaymentModel extends Model
{
	protected $table      = 'payment_confirmations';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'transaction_code',
    	'user_id',
        'bank_id',
        'bank_acc_name',
        'bank_acc',
        'nominal',
        'package_id',
        'class_id',
        'status',
        'attachment',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';

    public function getList(){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table.' pc');

        $builder->select('pc.id');
        $builder->select('pc.transaction_code');
        $builder->select('us.name as user');
        $builder->select('ba.name as bank');
        $builder->select('pack.name as package');
        $builder->select('cl.title as class');
        $builder->select('pc.created_at');
        $builder->join('users us', 'us.id = pc.user_id');
        $builder->join('data_bank ba', 'ba.id = pc.bank_id');
        $builder->join('packages pack', 'pack.id = pc.package_id');
        $builder->join('class cl', 'cl.id = pc.class_id');
        $builder->where('pc.status', 'Waiting');

        return $builder->get();
    }
    public function getDetail($code){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table.' pc');

        $builder->select('pc.id');
        $builder->select('pc.transaction_code');
        $builder->select('us.name as user');
        $builder->select('ba.name as bank');
        $builder->select('pc.bank_acc_name');
        $builder->select('pc.bank_acc');
        $builder->select('pack.name as package');
        $builder->select('cl.title as class');
        $builder->select('pack.id as package_id');
        $builder->select('cl.id as class_id');
        $builder->select('pc.created_at');
        $builder->select('pc.user_id');
        $builder->select('pc.attachment');
        $builder->join('users us', 'us.id = pc.user_id');
        $builder->join('data_bank ba', 'ba.id = pc.bank_id');
        $builder->join('packages pack', 'pack.id = pc.package_id');
        $builder->join('class cl', 'cl.id = pc.class_id');
        $builder->where('pc.transaction_code', $code);

        return $builder->get();
    }
}