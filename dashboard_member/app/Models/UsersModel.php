<?php namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
	protected $table      = 'users';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'referal_code',
    	'name',
    	'address',
    	'phone',
    	'email',
        'facebook',
        'instagram',
        'twitter',
        'username',
        'password',
        'photo',
        'identity_card',
        'status',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    public function login($data){
        $users = $this->where('username', $data['username'])->first();
        $password = $data['password'];
        $feedback = [];

        if($users){
            $hash = $users['password'];
            if ($users['status'] == 'Active') {
                if (password_verify($password, $hash)) {
                    unset($users['password']);
                    $feedback['session'] = $users;
                    $feedback['res'] = 'success';
                    $feedback['msg'] = '';
                } else {
                    $feedback['res'] = 'fail';
                    $feedback['msg'] = 'Username dan password salah!';
                }
            }else{
                $feedback['res'] = 'fail';
                $feedback['msg'] = 'Akun ditanguhkan!';
            }
        }else{
            $feedback['res'] = 'fail';
            $feedback['msg'] = 'Username tidak terdaftar!';
        }
        return $feedback;
    }
}

?>