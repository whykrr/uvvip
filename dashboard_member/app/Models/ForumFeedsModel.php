<?php

namespace App\Models;

use CodeIgniter\Model;

class ForumFeedsModel extends Model
{
    protected $table      = 'forum_feeds';
    protected $primaryKey = 'id';

    protected $allowedFields = ['title', 'content', 'user_id', 'total_like', 'total_comment', 'attachment'];
    protected $useTimestamps = true;


    public function FeedsUsers()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('forum_feeds');

        $builder->select('forum_feeds.*, users.name');
        $builder->join('users', 'forum_feeds.user_id = users.id', 'LEFT');
        $builder->orderBy('created_at', 'desc');
        return $builder->get();
    }

    public function FeedsUsersWhere($id)
    {
        $db = \Config\Database::connect();
        $builder = $db->table('forum_feeds');

        $builder->select('forum_feeds.*, users.name');
        $builder->join('users', 'forum_feeds.user_id = users.id', 'LEFT');
        $builder->where('forum_feeds.id', $id);
        return $builder->get();
    }

    public function ForumComment($id)
    {
        $db = \Config\Database::connect();
        $builder = $db->table('forum_comments');

        $builder->select('forum_comments.*, users.name');
        $builder->join('users', 'forum_comments.user_id = users.id', 'LEFT');
        $builder->where('forum_feed_id', $id);
        $builder->orderBy('created_at', 'desc');
        return $builder->get();
    }


    public function myarsip($id)
    {
        $db = \Config\Database::connect();
        $builder = $db->table('forum_feeds');

        $builder->select('forum_feeds.*, users.name');
        $builder->join('forum_archives', 'forum_feeds.id = forum_archives.forum_feed_id', 'LEFT');
        $builder->join('users', 'forum_feeds.user_id = users.id', 'LEFT');
        $builder->where('forum_archives.user_id', $id);
        return $builder->get();
    }

    public function carifeed($nama)
    {
        $db = \Config\Database::connect();
        $builder = $db->table('forum_feeds');

        $builder->select('forum_feeds.*, users.name');
        $builder->join('users', 'forum_feeds.user_id = users.id', 'LEFT');
        $builder->like('users.name', strtolower($nama), 'both');
        $builder->orLike('users.name', ucfirst($nama), 'both');
        $builder->orLike('users.name', strtoupper($nama), 'both');
        return $builder->get();
    }

    public function ForumLike($id, $user_id)
    {
        $db = \Config\Database::connect();
        $builder = $db->table('forum_likes');

        $builder->select('*');
        $builder->where('forum_feed_id', $id);
        $builder->where('user_id', $user_id);
        return $builder->countAllResults();
    }
}
