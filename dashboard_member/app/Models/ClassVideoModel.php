<?php namespace App\Models;

use CodeIgniter\Model;

class ClassVideoModel extends Model
{
	protected $table      = 'class_video';
	protected $primaryKey = 'id';
	protected $allowedFields = ['class_id', 'attachment'];
	protected $useTimestamps = true;

	public function insertFile($data)
    {
        return $this->db->table($this->table)->insert($data);
    }
}