<?php namespace App\Models;

use CodeIgniter\Model;

class UserClassQuizModel extends Model
{
	protected $table      = 'user_class_quiz';
    protected $primaryKey = 'id';


     protected $allowedFields = [
        'user_id',
        'class_id',
        'answers',
        'score',
    ];	

    

  
}