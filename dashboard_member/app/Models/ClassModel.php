<?php

namespace App\Models;

use CodeIgniter\Model;

class ClassModel extends Model
{
    protected $table      = 'class';
    protected $primaryKey = 'id';

    protected $allowedFields = [
        'title',
        'banner',
        'desc',
        'package_id',
        'capacity',
        'participant',
        'start_date',
        'end_date',
        'market_link',
        'zoom_link',
        'status',
        'status_participant',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    public function getList()
    {
        $db = \Config\Database::connect();
        $builder = $db->table($this->table . ' cl');

        $builder->select('cl.*');
        $builder->select('pack.name as package_name');
        $builder->join('packages pack', 'pack.id = cl.package_id', 'LEFT');
        $builder->where('cl.status', 'Open');
        $builder->orderBy('cl.created_at', 'desc');
        return $builder->get();
    }

    public function getListSchedule()
    {
        $db = \Config\Database::connect();
        $builder = $db->table($this->table . ' cl');

        $builder->select('cl.*');
        $builder->select('pack.name as package_name');
        $builder->join('packages pack', 'pack.id = cl.package_id', 'LEFT');
        $builder->where('cl.status', 'Scheduled');
        $builder->orderBy('cl.created_at', 'desc');
        return $builder->get();
    }

    public function ClassUser($id_user)
    {
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->select('class.*, packages.name as paket');
        $builder->join('class_member', 'class.id = class_member.class_id', 'LEFT');
        $builder->join('packages', 'class.package_id = packages.id', 'LEFT');
        $builder->where('class_member.user_id', $id_user);
        $builder->where('class.status !=', 'Close');
        $builder->orderBy('class.start_date', 'asc');
        return $builder->get();
    }

    public function ClassUserHistori($id_user)
    {
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->select('class.*, packages.name as paket, class_summary.attachment');
        $builder->join('class_member', 'class.id = class_member.class_id', 'LEFT');
        $builder->join('packages', 'class.package_id = packages.id', 'LEFT');
        $builder->join('class_summary', 'class_summary.class_id = class.id', 'LEFT');
        $builder->where('class_member.user_id', $id_user);
        $builder->where('class.status', 'Close');
        $builder->orderBy('class.end_date', 'desc');
        return $builder->get();
    }
}
