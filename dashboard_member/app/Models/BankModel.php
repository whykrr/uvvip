<?php namespace App\Models;

use CodeIgniter\Model;

class BankModel extends Model
{
	protected $table      = 'data_bank';
    protected $primaryKey = 'id';

    protected $allowedFields = [
    	'id',
    	'name',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
}