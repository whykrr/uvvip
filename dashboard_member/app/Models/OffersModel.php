<?php namespace App\Models;

use CodeIgniter\Model;

class OffersModel extends Model
{
    protected $table      = 'offers';
    protected $primaryKey = 'id';

    protected $allowedFields = [
        'title',
        'slug',
        'content',
        'content_wysiwig',
        'banner_img',
    ];

    protected $useTimestamps = 'datetime';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}