<?php namespace App\Models;

use CodeIgniter\Model;

class ClassMemberModel extends Model
{
	protected $table      = 'class_member';
    protected $primaryKey = 'code';
    protected $allowedFields = [
        'code',
        'class_id',
        'user_id',
        'transactions_id',
    ];


    public function ClassMemberUser(){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->select('users.photo, users.name, users.address, packages.name as package, transactions.created_at');
        $builder->join('users', 'users.id = class_member.user_id', 'LEFT');
        $builder->join('transactions', 'transactions.id = class_member.transaction_id', 'LEFT');
        $builder->join('packages', 'packages.id = transactions.package_id', 'LEFT');
        $builder->orderBy('transactions.created_at', 'DESC');
        return $builder->get();
    }
    public function getListStudents($class_id)
    {
        $builder = $this->db->table($this->table . ' cm');

        $builder->select('u.id as user_id');
        $builder->select('u.name');
        $builder->select('u.username');
        $builder->select('u.phone');
        $builder->select('u.email');
        $builder->select('ucq.score');
        $builder->select('cvt.attachment');
        $builder->select('ca.id as id_analitics');
        $builder->select('ca.character_score');
        $builder->select('ca.character_review');
        $builder->select('ca.intuition_score');
        $builder->select('ca.intiution_review');
        $builder->join('class cl', 'cl.id = cm.class_id', 'LEFT');
        $builder->join('users u', 'u.id = cm.user_id', 'LEFT');
        $builder->join('user_class_quiz ucq', 'ucq.user_id = cm.user_id AND ucq.class_id = cm.class_id', 'LEFT');
        $builder->join('class_video_trading cvt', 'cvt.user_id = cm.user_id AND cvt.class_id = cm.class_id', 'LEFT');
        $builder->join('class_analitics ca', 'ca.user_id = cm.user_id AND ca.class_id = cm.class_id', 'LEFT');
        $builder->orderBy('u.name', 'asc');
        $builder->where('cm.class_id', $class_id);

        return $builder->get();
    }
    public function insertMember($data){
        $db = \Config\Database::connect();
        $builder = $db->table($this->table);

        $builder->insert($data);
    }

}