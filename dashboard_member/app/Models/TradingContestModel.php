<?php

namespace App\Models;

use CodeIgniter\Model;

class TradingContestModel extends Model
{
    protected $table      = 'trading_contest';
    protected $primaryKey = 'id';

    protected $allowedFields = [
        'title',
        'slug',
        'desc',
        'banner',
        'start_date',
        'coin_capital',
        'zoom_link',
        'status_close',
    ];

    protected $useTimestamps = 'datetime';

    public function getFollowed($user_id)
    {
        $builder = $this->db->table($this->table . ' tc');

        $builder->select('tc.*');
        $builder->join('trading_contest_participant tcp', 'tc.id = tcp.trading_contest_id');
        $builder->where('tcp.user_id', $user_id);
        $builder->orderBy('tcp.created_at', 'desc');

        return $builder->get();
    }
}
