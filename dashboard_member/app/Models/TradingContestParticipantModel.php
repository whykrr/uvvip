<?php

namespace App\Models;

use CodeIgniter\Model;

class TradingContestParticipantModel extends Model
{
    protected $table      = 'trading_contest_participant';
    protected $primaryKey = 'id';

    protected $allowedFields = [
        'id',
        'trading_contest_id',
        'user_id',
        'submision_acc',
        'result',
        'result_percentage'
    ];

    protected $useTimestamps = 'datetime';

    public function insertParticipant($data)
    {
        $builder = $this->db->table($this->table);
        $data['created_at'] = date('Y-m-d H:i:s');
        $data['updated_at'] = date('Y-m-d H:i:s');
        $builder->insert($data);
    }

    public function getParticipant($contest_id)
    {
        $builder = $this->db->table($this->table . ' tcp');

        $builder->select('tcp.*');
        $builder->select('u.photo as user_photo');
        $builder->select('u.name as user_name');
        $builder->select('u.phone as user_phone');
        $builder->select('u.email as user_email');
        $builder->select('u.username as user_username');
        $builder->join('users u', 'u.id = tcp.user_id', 'left');
        $builder->where('trading_contest_id', $contest_id);
        $builder->orderBy('tcp.result_percentage', 'desc');

        return $builder->get();
    }
}
