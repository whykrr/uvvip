<?php namespace App\Models;

use CodeIgniter\Model;

class ClassAnaliticsModels extends Model
{
	protected $table      = 'class_analitics';
    protected $primaryKey = 'id';
    protected $allowedFields = [
        'class_id',
        'user_id',
        'character_score',
        'character_review',
        'intuition_score',
        'intiution_review',
    ];
    protected $useTimestamps = 'datetime';
  
}