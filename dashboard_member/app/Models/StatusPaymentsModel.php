<?php namespace App\Models;

use CodeIgniter\Model;

class StatusPaymentsModel extends Model
{
	protected $table      = 'status_payments';
    protected $primaryKey = 'id';
}