$('.button-action').buttonAction({
	component: "detail,edit,delete",
	keyAttr: "value",
});
$('.modal-item').createModal();

$('.modal-dismiss').click(function(){
	$('#modal-notif').hide();
})
$('.coming-soon').click(function(){
	show_modal('basic', '', 'notification', 'Dalam Perbaikan Sistem!', '');
})
$('.logout-btn').click(function(){
	link = $(this).data('link');
	show_modal('logout', 'warning', 'Logout', 'Apakah anda yakin akan keluar dari akun ini ?', link);
})

setTimeout(function(){
	$('.alert').fadeOut();
}, 3500);

setInterval(function(){
	get_time();
	get_date();
}, 1000);
function f_digit(val){
	if(val < 10){
		return '0'+val;
	}else{
		return val;
	}
}
function bulan_indo(val){
	if(val == 0){ return 'Januari'; }
	else if(val == 1){ return 'Februari'; }
	else if(val == 2){ return 'Maret'; }
	else if(val == 3){ return 'April'; }
	else if(val == 4){ return 'Mei'; }
	else if(val == 5){ return 'Juni'; }
	else if(val == 6){ return 'Juli'; }
	else if(val == 7){ return 'Agustus'; }
	else if(val == 8){ return 'September'; }
	else if(val == 9){ return 'Oktober'; }
	else if(val == 10){ return 'November'; }
	else if(val == 11){ return 'Desember'; }
}
function get_time(){
	var today = new Date();
	var time = f_digit(today.getHours())+' : '+f_digit(today.getMinutes())+' : '+f_digit(today.getSeconds());
	$('.running-time').text(time);
}
function get_date(){
	var today = new Date();
	var time = today.getDate()+' '+bulan_indo(today.getMonth())+' '+today.getFullYear();
	$('.running-date').text(time);
}
function show_modal(type, header_type, header, isi, link) {
	$('#modal-notif').find('.modal-notif-header').text(header.toUpperCase());
	$('#modal-notif').find('.modal-notif-content').html(isi);

	if(header_type != ''){
		header_type = 'header-'+header_type;
		$('#modal-notif').find('.modal-notif-header').addClass(header_type);
	}
	if(type=='basic'){
		$('#modal-notif').find('.btn-info').text('OK');
		$('#modal-notif').find('.btn-info').show();
		$('#modal-notif').show();
	}
	if(type=='basic_link'){
		$('#modal-notif').find('.btn-info').text('OK');
		$('#modal-notif').find('.btn-info').attr('href', link);
		$('#modal-notif').find('.btn-info').show();
		$('#modal-notif').show();
	}
	if(type=='contest'){
		$('#modal-notif').find('.btn-info').text('Ya');
		$('#modal-notif').find('.btn-info').attr('href', link);
		$('#modal-notif').find('.btn-info').show();
		$('#modal-notif').find('.btn-danger').text('Tidak');
		$('#modal-notif').find('.btn-danger').show();

		$('#modal-notif').show();
	}
	if(type=='logout'){
		$('#modal-notif').find('.btn-info').text('Batal');
		$('#modal-notif').find('.btn-danger').text('OK');
		$('#modal-notif').find('.btn-danger').attr('href', link);
		$('#modal-notif').find('.btn-info').show();
		$('#modal-notif').find('.btn-danger').show();
		$('#modal-notif').show();
	}
}

$('.form-ajax').submit(function(){
	uri_redirect = $(this).data('redirect');
	$.ajax({
		type: 'post',
		url: $(this).data('uri'),
		data: $(this).serialize(),
		dataType: 'json',
		beforeSend:function(){
			$(".loading").show();
		},
		success: function(data)
		{
			if(data.res == 'success'){
				window.location = uri_redirect;
			}else if(data.res == 'success_msg'){
				show_modal('basic_link', 'success', 'notification', data.msg, uri_redirect);
			}else{
				$(".loading").hide();
				show_modal('basic', 'fail', 'notification', data.msg, '');
			}
		},
		error : function() {
			$(".loading").hide();
			show_modal("Network error!");
		}
	});
	return false;
});
$('input[type=file]').change(function (){
	if (this.files && this.files[0]) {
		var reader = new FileReader();
		input = this;

		reader.onload = function(e) {
			$(input).parent().find('.img-preview').attr('src', e.target.result);
		}

		reader.readAsDataURL(this.files[0]);
	}
})